//
//  SharedEnums.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-14.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class IndicatorView: UIView {
    var color = UIColor.clear {
        didSet { setNeedsDisplay() }
    }
    
    override func draw(_ rect: CGRect) {
        color.set()
        UIBezierPath(ovalIn: rect).fill()
    }
}

enum ActionDescriptor {
    case Price,  Timers
    
    func title(forDisplayMode displayMode: ButtonDisplayMode) -> String? {
        guard displayMode != .imageOnly else { return nil }
        
        switch self {
        case .Price: return "Price"
        case .Timers: return "Timer"
        }
    }
    
    func image(forStyle style: ButtonStyle, displayMode: ButtonDisplayMode) -> UIImage? {
        guard displayMode != .titleOnly else { return nil }
        
        let name: String
        switch self {
        case .Price: name = "doted_arrow_right"
        case .Timers: name = "Timer_watch"
        }
//        return circularIcon(with: color(forStyle: .backgroundColor), size: CGSize.init(width: 40, height: 40), icon: UIImage.init(named: name))
        return UIImage(named: name)
    }
    
    func color(forStyle style: ButtonStyle) -> UIColor {
        switch self {
        case .Price: return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .Timers: return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    func circularIcon(with color: UIColor, size: CGSize, icon: UIImage? = nil) -> UIImage? {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)

        UIBezierPath(ovalIn: rect).addClip()

        color.setFill()
        UIRectFill(rect)

        if let icon = icon {
            let iconRect = CGRect(x: (rect.size.width - icon.size.width) / 2,
                                  y: (rect.size.height - icon.size.height) / 2,
                                  width: icon.size.width,
                                  height: icon.size.height)
            icon.draw(in: iconRect, blendMode: .normal, alpha: 1.0)
        }

        defer { UIGraphicsEndImageContext() }

        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
enum ButtonDisplayMode {
    case titleAndImage, titleOnly, imageOnly
}

enum ButtonStyle {
    case backgroundColor, circular
}
