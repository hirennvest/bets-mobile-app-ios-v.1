//
//  ParamStruct.swift
//  Lead
//
//  Created by Hiren Joshi on 22/01/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import Foundation

// MARK:- Login And Register Parameters
struct LoginParamDict: Encodable {
    var ids: String
    var password: String
    
    var description: [String : Any] {
        get {
            return ["email" : ids.removeWhiteSpace(), "password": password.removeWhiteSpace()]
        }
    }
}

struct SignupParamDict: Encodable {
    var username: String
    var email: String
    var password: String
    
    var description: [String : Any] {
        get {
            return [
                "username": self.username,
                "email": self.email,
                "password": self.password,
                "ref_affiliate": "1",   // reference/upline affiliate-id
                "account_type": "Personal",
                "signedup_app": "GX"   // The user signing up from app
            ]
        }
    }
}

struct GXUsernameCheckDict: Encodable {
    var email: String
    var username: String
    var check: Bool = true
    
    var description: [String : Any] {
        get {
            return [
                "username": self.username,
                "email": self.email,
                "username_check": self.check]
        }
    }
}

struct GXUsernameDict: Encodable {
    var email: String
    var username: String
    var image: String
    
    var description: [String : Any] {
        get {
            return [
                "username": self.username,
                "email": self.email,
                "image_url": self.image]
        }
    }
}

struct ConfirmParamDict: Encodable {
    var email: String
    var code: String
    
    var description: [String : Any] {
        get {
            return [
                "email": self.email,
                "code": self.code
            ]
        }
    }
}

struct AuthLoginParamDict: Encodable {
    var ids: String
    var password: String
    var totp_code: String
    
    var description: [String : Any] {
        get {
            return ["email" : ids.removeWhiteSpace(), "password": password.removeWhiteSpace(), "totp_code": self.totp_code]
        }
    }
}

struct RefreshTokenParamDict: Encodable {
    var refreshToken: String
    var devicekey: String
    
    var description: [String : Any] {
        get {
            return ["refreshToken" : refreshToken, "device_key": devicekey]
        }
    }
}

struct UserdataParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

struct RegisterBetsParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

// MARK:- Comman Body Parameters
struct CommanHeader: Encodable {
    var email: String
    var token: String
    
    var description: [String : String] {
        get {
            return ["email" : email.removeWhiteSpace().count == 0 ? HeaderSetter().email : email, "token": token.count == 0 ? HeaderSetter().token : token]
        }
    }
}

struct VaultTransactionParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

struct FundVaultParamDict: Encodable {
    var arcade_userid: String
    
    var description: [String : Any] {
        get {
            return ["arcade_userid" : self.arcade_userid.isEmpty ? HeaderSetter().ArcadeUserid : self.arcade_userid, "arcade_id": HeaderSetter().Arcadeid]
        }
    }
}

struct BetVaultTopupParam: Encodable {
    var email: String
    var source_coin: String
    var dest_coin: String
    var amount: Double
    var arcade_userid: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace(), "source_coin": source_coin, "dest_coin": dest_coin, "amount": amount, "arcade_userid" : self.arcade_userid.isEmpty ? HeaderSetter().ArcadeUserid : self.arcade_userid]
        }
    }
}

struct BetVaultWithdrawParam: Encodable {
    var email: String
    var source_coin: String
    var dest_coin: String
    var amount: Double
    var arcade_userid: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace(), "source_coin": source_coin, "dest_coin": dest_coin, "amount": amount, "arcade_userid" : self.arcade_userid.isEmpty ? HeaderSetter().ArcadeUserid : self.arcade_userid]
        }
    }
}

struct ForexConvertParam: Encodable {
    var buy: String
    var from: String
    
    var description: [String : String] {
        get {
            return ["buy" : buy, "from": from]
        }
    }
}

struct LotteryCoinParam: Encodable {
    var coin: String
    
    var description: [String : String] {
        get {
            return ["coin" : coin]
        }
    }
}

struct VaultAddressParamDict: Encodable {
    var arcade_userid: String
    
    var description: [String : Any] {
        get {
            return ["arcade_userid" : self.arcade_userid.isEmpty ? HeaderSetter().ArcadeUserid : self.arcade_userid, "arcade_id": HeaderSetter().Arcadeid]
        }
    }
}

struct PurchaseTicketsParamDict: Encodable {
    var email: String
    var tickets: [String]
    var lotteryid: String
    var sourcevault: String
    var arcade_userid: String
    
    var description: [String : Any] {
        get {
            return ["tickets" : tickets, "lottery_id": lotteryid, "email" : email.removeWhiteSpace(), "source_vault": sourcevault, "arcade_userid" : self.arcade_userid.isEmpty ? HeaderSetter().ArcadeUserid : self.arcade_userid, "arcade_id": HeaderSetter().Arcadeid]
        }
    }
}

struct VaultCoinlistParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

