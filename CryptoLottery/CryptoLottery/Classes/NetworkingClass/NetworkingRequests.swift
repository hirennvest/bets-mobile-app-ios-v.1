//
//  NetworkingRequests.swift
//  Lead
//
//  Created by Hiren Joshi on 03/01/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
//import RxAlamofire
//import RxCocoa
//import RxSwift
import SwiftyJSON
import CRNotifications


enum APIError: Error {
    case InternetConnection
    case invalidInput
    case badResposne
    case invalidURL
    
    func toNSError() -> NSError {
        let domain = "Domain url"
        switch self {
        case .InternetConnection:
            return NSError(domain: domain, code: 0, userInfo: [NSLocalizedDescriptionKey: "Internet connection not available please, check Internet and try agian!"])
        case .invalidURL:
            return NSError(domain: domain, code: -1, userInfo: [NSLocalizedDescriptionKey: "Input should be a valid URL"])
        case .invalidInput:
            return NSError(domain: domain, code: -2, userInfo: [NSLocalizedDescriptionKey: "Input should be a valid number"])
        case .badResposne:
            return NSError(domain: domain, code: -3, userInfo: [NSLocalizedDescriptionKey: "Bad response"])
        }
    }
}

@objc public class NetworkingRequests: NSObject {
    
    static let shared = NetworkingRequests()
    
    var successcomplitionHandler: ((_ data: Any)->Void)?
    var onError: ((_ msg: String, _ ErrCode: NSInteger?)->Void)?
    var rechability: Reachability!
    
    @objc class var swiftSharedInstance: NetworkingRequests {
        struct Singleton {
            static let instance = NetworkingRequests()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> NetworkingRequests {
        return NetworkingRequests.swiftSharedInstance
    }
    
    private override init() {
        self.rechability = Reachability.init(hostname: "www.google.com")
        self.rechability.startNotifier()
    }
    
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func getCurrentViewController() -> UIViewController? {
        if #available(iOS 13, *) {
            let window = UIApplication.shared.windows
            if let rootController = window[0].rootViewController {
                var currentController: UIViewController! = rootController
                while( currentController.presentedViewController != nil ) {
                    currentController = currentController.presentedViewController
                }
                return currentController
            }
            return nil
        } else {
            
            if let rootController = UIApplication.shared.keyWindow?.rootViewController {
                var currentController: UIViewController! = rootController
                while( currentController.presentedViewController != nil ) {
                    currentController = currentController.presentedViewController
                }
                return currentController
            }
            return nil
        }
    }
    
    class func displayError(_ error: NSError?) {
        if let e = error {
            let alertController = UIAlertController(title: "Error", message: e.localizedDescription, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                // do nothing...
            }
            alertController.addAction(okAction)
            self.getCurrentViewController()!.present(alertController, animated: true, completion: nil)
        }
    }
    
}

//MARK:- JsonCOnverter
extension NetworkingRequests {
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

//MARK:- Request Methods With String URL
extension NetworkingRequests {
    
    func uploadImage(_ endpoint:String, param: [String: String]?, image:UIImage, filename:String, imageName:String,completion: @escaping (_ result:NSDictionary?, _ isSuccess:Bool, _ errorStr:String?) -> Void) {
        
        if self.rechability.isReachable() || NetworkingRequests.isInternetAvailable() {
            let requestUrl = endpoint
            upload(multipartFormData: { multipartFormData in
                guard let imageData = image.jpeg(.lowest) else {
                    print("Image size too large")
                    return
                }
                multipartFormData.append(imageData, withName: filename, fileName: imageName, mimeType: "image/jpeg")
                print("image is :\(imageName)")
                if param != nil {
                    for (key,value) in param! {
                        multipartFormData.append((value).data(using: .utf8)!, withName: key)
                    }
                }
            }, to: requestUrl,
               method:.post,
               headers: [
                "Content-Type":"multipart/form-data",
                "Access-Control-Allow-Origin": "*"
                ],
               encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response)
                        if response.response?.statusCode == 200 {
                            if let jsonData = response.result.value as? NSDictionary {
                                completion(jsonData,true,nil)
                            }else {
                                completion(nil,false,NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)
                            }
                        }else if  response.response?.statusCode == 401{
                            if let jsonData = response.result.value as? NSDictionary {
                                completion(jsonData,true,nil)
                            }else {
                                completion(nil,false,NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)
                            }
                            main {
                                UserInfoData.shared.RefreshToken { (Tokenstatus) in
                                    let  jsonData = response.result.value as? NSDictionary
                                    if Tokenstatus {
                                        guard let message = jsonData!["payload"] as? String else {
                                            completion(nil,false,NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)
                                            return
                                        }
                                        completion(nil,false,message)
                                    }
                                    else {
                                        UserInfoData.shared.UserLogout()
                                    }
                                }
                            }
                        }else {
                            if let jsonData = response.result.value as? NSDictionary {
                                if let err = jsonData["message"] as? String {
                                    completion(nil,false,err)
                                }else {
                                    completion(nil,false,NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)
                                }
                            }else {
                                completion(nil,false,NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) as String?)
                            }
                        }
                    }
                case .failure(let error):
                    completion(nil,false,error.localizedDescription)
                }
                
            })
        }
        else {
            completion(nil,false,APIError.InternetConnection.toNSError().localizedDescription)
        }
    }
    
    func RequestMultiPart(url:URL, imagename: String, pictures: UIImage, finish: @escaping((message:String, list:[[String: Any]],isSuccess:Bool)) -> Void) {
        
//        var result:(message:String, list:[[String: Any]],isSuccess:Bool) = (message: "Fail", list:[],isSuccess : false)
//        result.isSuccess = true
//        result.message = msg
//        if let jsonArray = parsedData["data"] as? [[String: Any]] {
//            result.list = jsonArray
//        }
//            finish(result)
    }
    
    func requestPOST(_ key: String, Parameters: [String: Any]? = nil, Headers: [String: String]? = nil, onSuccess: ((_ data: [String: Any], _ status: Bool)->Void)?, onError: ((_ msg: String, _ ErrCode: NSInteger?)->Void)?) {
        
        if self.rechability.isReachable() || NetworkingRequests.isInternetAvailable() {
            if key.count != 0 {
                Alamofire.request(key, method: .post, parameters: Parameters, encoding: JSONEncoding.default, headers: Headers).responseJSON(completionHandler:
                    { (DataResponse) in
                        switch DataResponse.result {
                        case .success(let JSON):
                            print("Success with JSON: \(JSON)")
                            if DataResponse.response?.statusCode == 200 {
                                guard let body = DataResponse.result.value as? [String: Any] else {
                                    onError!("mapping issue", DataResponse.response?.statusCode)
                                    return
                                }
                                let statuscode = self.CheckResponseStatus(results: body)
                                print(body)
                                AppUtillity.shared.NetworkIndicator(status: false)
                                if statuscode {
                                    onSuccess!(body, statuscode)
                                }
                                else {
                                    onSuccess!(body, statuscode)
                                }
                            }
                            else if DataResponse.response?.statusCode == 401 {
                                guard let body = DataResponse.result.value as? [String: Any] else {
                                    onError!("mapping issue", DataResponse.response?.statusCode)
                                    return
                                }
                                main {
                                    print("URL: \(key) and body:\(body)")
                                    UserInfoData.shared.RefreshToken { (Tokenstatus) in
                                        if Tokenstatus {
                                            guard let message = body["payload"] as? String else {
                                                onError!("", 401)
                                                return
                                            }
                                            onError!(message, 200)
                                        }
                                        else {
                                            UserInfoData.shared.UserLogout()
                                        }
                                    }
                                }
                            }
                            else {
                                guard let body = DataResponse.result.value as? [String: Any] else {
                                    onError!("mapping issue", DataResponse.response?.statusCode)
                                    return
                                }
                                guard let message = body["payload"] as? String else {
                                    onError!("", DataResponse.response?.statusCode)
                                    return
                                }
                                AppUtillity.shared.NetworkIndicator(status: false)
                                onError!(message, DataResponse.response?.statusCode)
                            }
                            
                        case .failure(_):
                            main {
                                UserInfoData.shared.RefreshToken { (Tokenstatus) in
                                    if Tokenstatus {
                                        onError!("Refresh Token", 200)
                                    }
                                    else {
                                        UserInfoData.shared.UserLogout()
                                    }
                                }
                            }
                        }
                })
            }
            else {
                AppUtillity.shared.NetworkIndicator(status: false)
                onError!(APIError.invalidURL.toNSError().localizedDescription, 401)
            }
        }
        else {
            AppUtillity.shared.NetworkIndicator(status: false)
            onError!(APIError.InternetConnection.toNSError().localizedDescription, 1000)
        }
        
    }
    
    func requestsGET(_ key: String, Parameters: [String: Any]? = nil, Headers: [String: String]? = nil, onSuccess: ((_ data: [String: AnyObject])->Void)?, onError: ((_ msg: String, _ ErrCode: NSInteger?)->Void)?) {
        
        if self.rechability.isReachable() {
            var StrURL: String = ""
            if Parameters != nil {
                StrURL = String.init(format: "%@?%@", key, (Parameters! as NSDictionary).DictToqueryString)
            }
            else {
                StrURL = String.init(format: "%@", key)
            }
            
            if key.count != 0 {
                
                Alamofire.request(key,
                method: .get,
                parameters: Parameters,
                encoding: URLEncoding.queryString,
                headers: Headers).responseJSON(completionHandler:
                  { (DataResponse) in
                      debugPrint(request)
                      switch DataResponse.result {
                      case .success(let JSON):
                              print("Success with JSON: \(JSON)")
                          if DataResponse.response?.statusCode == 200 {
                              guard let body = DataResponse.result.value as? [String: AnyObject] else {
                                  onError!("mapping issue", DataResponse.response?.statusCode)
                                  return
                              }
                              let statuscode = self.CheckResponseStatus(results: body)
                              print(body)
                              AppUtillity.shared.NetworkIndicator(status: false)
                              if statuscode {
                                  onSuccess!(body)
                              }
                              else {
                                  onSuccess!(body)
                              }
                          }
                          else if DataResponse.response?.statusCode == 401 {
                              guard let body = DataResponse.result.value as? [String: Any] else {
                                  onError!("mapping issue", DataResponse.response?.statusCode)
                                  return
                              }
                              main {
                                  print("URL: \(key) and body:\(body)")
                                  UserInfoData.shared.RefreshToken { (Tokenstatus) in
                                      if Tokenstatus {
                                          guard let message = body["payload"] as? String else {
                                              onError!("", 401)
                                              return
                                          }
                                          onError!(message, 200)
                                      }
                                      else {
                                          UserInfoData.shared.UserLogout()
                                      }
                                  }
                              }
                          }
                          else {
                              guard let body = DataResponse.result.value as? [String: Any] else {
                                  onError!("mapping issue", DataResponse.response?.statusCode)
                                  return
                              }
                              guard let message = body["payload"] as? String else {
                                  onError!("", DataResponse.response?.statusCode)
                                  return
                              }
                              AppUtillity.shared.NetworkIndicator(status: false)
                              onError!(message, DataResponse.response?.statusCode)
                          }
                          
                      case .failure(_):
                          main {
                              UserInfoData.shared.RefreshToken { (Tokenstatus) in
                                  if Tokenstatus {
                                      onError!("Refresh Token", 200)
                                  }
                                  else {
                                      UserInfoData.shared.UserLogout()
                                  }
                              }
                          }
                      }
                })
            }
            else {
                AppUtillity.shared.NetworkIndicator(status: false)
                onError!(APIError.invalidURL.toNSError().localizedDescription, 401)
            }
        }
        else {
            AppUtillity.shared.NetworkIndicator(status: false)
            onError!(APIError.InternetConnection.toNSError().localizedDescription, 1000)
        }
        
    }
    
}

//MARK:- Request Methods With PlistKey
extension NetworkingRequests {
    
    func CheckResponseStatus(results: [String: Any]) -> Bool {
        let status = results["status"] as AnyObject
        let statuscode: Bool
        switch status {
        case is String:
            print("\(status) is a string!")
            if (status as! String) == "1" {
                statuscode = true
            }
            else {
                statuscode = false
            }
        case is Int:
            print("\(status) is an integer!")
            if (status as! Int) == 1 {
                statuscode = true
            }
            else {
                statuscode = false
            }
        case is Bool:
            statuscode = status as! Bool
            
        default:
            print("I don't know this value!")
            statuscode = status as! Bool
        }
        return statuscode
    }
    
}
