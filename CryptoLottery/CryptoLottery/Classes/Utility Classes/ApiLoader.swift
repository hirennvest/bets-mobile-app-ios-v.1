//
//  ApiLoader.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-05.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import Foundation
import Lottie
import UIKit

public enum LottieHUDMaskType {
    case solid
    case transparent
}

public final class ApiLoader {
    
    static let shared = ApiLoader("Loading")
    
    class var swiftSharedInstance: ApiLoader {
        struct Singleton {
            static let instance = ApiLoader("Loading")
        }
        return Singleton.instance
    }
    // the sharedInstance class method can be reached from ObjC
    class func sharedInstance() -> ApiLoader {
        return ApiLoader.swiftSharedInstance
    }
    
    public struct LottieHUDConfig {
        
        static var shadow: CGFloat = 0.4
        static var animationDuration: TimeInterval = 0.3
    }
    
    var maskView: UIView!
    // Not implemeted yet :)
//        public var blurMaskType: UIBlurEffect = UIBlurEffect(style: .dark)
    
    private var _lottie: AnimationView!
    
    public var contentMode: UIView.ContentMode = .scaleAspectFit {
        didSet {
            self._lottie.contentMode = contentMode
            self._lottie.clipsToBounds = true
            self._lottie.layer.cornerRadius = 10
        }
    }
    
    public var maskType: LottieHUDMaskType = .solid

    public var size: CGSize = CGSize(width: 200, height: 200)
    
    init(_ name: String, loop: Bool = true) {
        self._lottie = AnimationView(name: name)
        self._lottie.loopMode = .loop
        main {
            self.maskView = UIView()
            self.maskView.translatesAutoresizingMaskIntoConstraints = false
            self.maskView.isUserInteractionEnabled = false
            self.maskView.clipsToBounds = true
            self.maskView.layer.cornerRadius = 0
            self.maskView.alpha = 0.0
        }
    }
    
    init(_ lottie: AnimationView) {
        self._lottie = lottie
        self.maskView.translatesAutoresizingMaskIntoConstraints = false
        self.maskView.isUserInteractionEnabled = false
        self.maskView.clipsToBounds = true
        self.maskView.layer.cornerRadius = 0
        self.maskView.alpha = 0.0
    }
    
    public func showHUD(with delay: TimeInterval = 0.0, loop: Bool = true, Ispopup: Bool = false) {
        self._lottie.loopMode = loop ? .loop : .playOnce
        createHUD(delay: delay, Ispopup: Ispopup)
    }
    
    public func stopHUD() {
        clearHUD()
    }
    
    private func createHUD(delay: TimeInterval = 0.0, Ispopup: Bool = false) {
        main {
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
            self.configureMask()
            if Ispopup {
                self.configurePopupConstraints()
            }
            else {
                self.configureConstraints()
            }
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: delay, options: .curveEaseIn, animations: {
                
                self.maskView.alpha = 1.0
            }, completion: nil)
            
            self._lottie.play(completion: { _ in
                self.clearHUD()
            })
        }
    }
    
    private func configureMask() {
        if maskType == .transparent {
            maskView.backgroundColor = UIColor.black.withAlphaComponent(LottieHUDConfig.shadow)
        } else {
            maskView.backgroundColor = UIColor.white
        }
    }
    
    private func configurePopupConstraints() {
        // Configure Backround View Constraints
        main {
            self.popUpview.view.addSubview(self.maskView)
            
            guard let keyWindowMargins = self.popUpview.view else {return}
            
            self.maskView.leadingAnchor.constraint(equalTo: keyWindowMargins.leadingAnchor, constant: 0).isActive = true
            self.maskView.trailingAnchor.constraint(equalTo: keyWindowMargins.trailingAnchor, constant: 0).isActive = true
            self.maskView.topAnchor.constraint(equalTo: keyWindowMargins.topAnchor).isActive = true
            self.maskView.bottomAnchor.constraint(equalTo: keyWindowMargins.bottomAnchor).isActive = true
            
            self.maskView.addSubview(self._lottie)
            
            // Configure Lottie Constraints
            self._lottie.translatesAutoresizingMaskIntoConstraints = false
            self._lottie.centerXAnchor.constraint(equalTo: self.maskView.centerXAnchor, constant: 0).isActive = true
            self._lottie.centerYAnchor.constraint(equalTo: self.maskView.centerYAnchor, constant: 0).isActive = true
            self._lottie.heightAnchor.constraint(equalToConstant: self.size.height).isActive = true
            self._lottie.widthAnchor.constraint(equalToConstant: self.size.width).isActive = true
        }
    }
    
    private func configureConstraints() {
        // Configure Backround View Constraints
        main {
            self.keyWindow.view.addSubview(self.maskView)
            
            guard let keyWindowMargins = self.keyWindow.view else {return}
            
            self.maskView.leadingAnchor.constraint(equalTo: keyWindowMargins.leadingAnchor, constant: 0).isActive = true
            self.maskView.trailingAnchor.constraint(equalTo: keyWindowMargins.trailingAnchor, constant: 0).isActive = true
            self.maskView.topAnchor.constraint(equalTo: keyWindowMargins.topAnchor).isActive = true
            self.maskView.bottomAnchor.constraint(equalTo: keyWindowMargins.bottomAnchor).isActive = true
            
            self.maskView.addSubview(self._lottie)
            
            // Configure Lottie Constraints
            self._lottie.translatesAutoresizingMaskIntoConstraints = false
            self._lottie.centerXAnchor.constraint(equalTo: self.maskView.centerXAnchor, constant: 0).isActive = true
            self._lottie.centerYAnchor.constraint(equalTo: self.maskView.centerYAnchor, constant: 0).isActive = true
            self._lottie.heightAnchor.constraint(equalToConstant: self.size.height).isActive = true
            self._lottie.widthAnchor.constraint(equalToConstant: self.size.width).isActive = true
        }
    }
    
    private func clearHUD() {
        main {
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: 0, options: .curveEaseIn, animations: {
                self.maskView.alpha = 0.0
            }) { finished in
                UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
                self.maskView.removeFromSuperview()
                self._lottie.stop()
            }
        }
    }
    
    private var keyWindow: UIViewController {
        return UIApplication.topViewController()!
    }
    
    private var popUpview: UIViewController {
        return (App?.window.rootViewController)!
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

