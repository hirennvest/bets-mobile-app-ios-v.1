//
//  Font.swift
//
//  Created by Hiren Joshi on 05/08/19.
//  Copyright © 2019 Hiren Joshi. All rights reserved.
//

import UIKit
import Foundation

/**
 Font category classes
 */
class Font: UIFont {
    
    func MontserratBlackFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Black", size: size)
        return font!
    }
    
    func MontserratBlackItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-BlackItalic", size: size)
        return font!
    }
    
    func MontserratBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Bold", size: size)
        return font!
    }
    
    func MontserratBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-BoldItalic", size: size)
        return font!
    }
    
    func MontserratExtraBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraBold", size: size)
        return font!
    }
    
    func MontserratExtraBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraBoldItalic", size: size)
        return font!
    }
    
    func MontserratExtraLightFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraLight", size: size)
        return font!
    }
    
    func MontserratExtraLightItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraLightItalic", size: size)
        return font!
    }
    
    func MontserratItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Italic", size: size)
        return font!
    }
    
    func MontserratLightFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Light", size: size)
        return font!
    }
    
    func MontserratLightItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-LightItalic", size: size)
        return font!
    }
    
    func MontserratMediumFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Medium", size: size)
        return font!
    }
    
    func MontserratMediumItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-MediumItalic", size: size)
        return font!
    }
    
    func MontserratRegularFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Regular", size: size)
        return font!
    }
    
    func MontserratSemiBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-SemiBold", size: size)
        return font!
    }
    
    func MontserratSemiBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-SemiBoldItalic", size: size)
        return font!
    }
    
    func MontserratThinFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Thin", size: size)
        return font!
    }
    
    func MontserratThinItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ThinItalic", size: size)
        return font!
    }
    
}
