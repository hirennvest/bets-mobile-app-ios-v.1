//
//  ViewLoader.swift
//  CryptoLottery
//
//  Created by Hiren on 14/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import Foundation
import Lottie
import UIKit

public final class ViewLoader {
    
    static let shared = ViewLoader("Loading")
    
    class var swiftSharedInstance: ViewLoader {
        struct Singleton {
            static let instance = ViewLoader("Loading")
        }
        return Singleton.instance
    }
    // the sharedInstance class method can be reached from ObjC
    class func sharedInstance() -> ViewLoader {
        return ViewLoader.swiftSharedInstance
    }
    
    public struct LottieHUDConfig {
        
        static var shadow: CGFloat = 0.4
        static var animationDuration: TimeInterval = 0.3
    }
    
    var maskView: UIView!
    // Not implemeted yet :)
//        public var blurMaskType: UIBlurEffect = UIBlurEffect(style: .dark)
    
    private var _lottie: AnimationView!
    
    public var contentMode: UIView.ContentMode = .scaleAspectFit {
        didSet {
            self._lottie.contentMode = contentMode
            self._lottie.clipsToBounds = true
            self._lottie.layer.cornerRadius = 10
        }
    }
    
    public var maskType: LottieHUDMaskType = .solid

    public var size: CGSize = CGSize(width: 200, height: 200)
    
    init(_ name: String, view: UIViewController? = nil) {
        self._lottie = AnimationView(name: name)
        self._lottie.loopMode = .loop
        main {
            self.maskView = UIView()
            self.maskView.translatesAutoresizingMaskIntoConstraints = false
            self.maskView.isUserInteractionEnabled = false
            self.maskView.clipsToBounds = true
            self.maskView.layer.cornerRadius = 0
            self.maskView.alpha = 0.0
        }
    }
    
    init(_ lottie: AnimationView) {
        self._lottie = lottie
        self.maskView.translatesAutoresizingMaskIntoConstraints = false
        self.maskView.isUserInteractionEnabled = false
        self.maskView.clipsToBounds = true
        self.maskView.layer.cornerRadius = 0
        self.maskView.alpha = 0.0
    }
    
    public func showHUD(with delay: TimeInterval = 0.0, loop: Bool = true, view: UIViewController) {
        self._lottie.loopMode = loop ? .loop : .playOnce
        createHUD(delay: delay, view: view)
    }
    
    public func stopHUD() {
        clearHUD()
    }
    
    private func createHUD(delay: TimeInterval = 0.0, view: UIViewController) {
        main {
            UIApplication.shared.keyWindow!.isUserInteractionEnabled = false
            self.configureMask()
            self.configurePopupConstraints(view: view)
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: delay, options: .curveEaseIn, animations: {
                
                self.maskView.alpha = 1.0
            }, completion: nil)
            
            self._lottie.play(completion: { _ in
                self.clearHUD()
            })
        }
    }
    
    private func configureMask() {
        if maskType == .transparent {
            maskView.backgroundColor = UIColor.black.withAlphaComponent(LottieHUDConfig.shadow)
        } else {
            maskView.backgroundColor = UIColor.white
        }
    }
    
    private func configurePopupConstraints(view: UIViewController) {
        // Configure Backround View Constraints
        main {
            view.view.addSubview(self.maskView)
            
            self.maskView.leadingAnchor.constraint(equalTo: view.view.leadingAnchor, constant: 0).isActive = true
            self.maskView.trailingAnchor.constraint(equalTo: view.view.trailingAnchor, constant: 0).isActive = true
            self.maskView.topAnchor.constraint(equalTo: view.view.topAnchor).isActive = true
            self.maskView.bottomAnchor.constraint(equalTo: view.view.bottomAnchor).isActive = true
            
            self.maskView.addSubview(self._lottie)
            
            // Configure Lottie Constraints
            self._lottie.translatesAutoresizingMaskIntoConstraints = false
            self._lottie.centerXAnchor.constraint(equalTo: self.maskView.centerXAnchor, constant: 0).isActive = true
            self._lottie.centerYAnchor.constraint(equalTo: self.maskView.centerYAnchor, constant: 0).isActive = true
            self._lottie.heightAnchor.constraint(equalToConstant: self.size.height).isActive = true
            self._lottie.widthAnchor.constraint(equalToConstant: self.size.width).isActive = true
        }
    }
    
    private func clearHUD() {
        main {
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: 0, options: .curveEaseIn, animations: {
                self.maskView.alpha = 0.0
            }) { finished in
                UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
                self.maskView.removeFromSuperview()
                self._lottie.stop()
            }
        }
    }
    
}
