//
//  VideoListCell.m
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-05.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

#import "VideoListCell.h"

@implementation VideoListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.videolistview.backgroundColor = [UIColor clearColor];
    self.videolistview.clipsToBounds = true;
    self.videolistview.layer.borderWidth = 1.0;
    self.videolistview.layer.borderColor = [UIColor colorWithRed: 0.00 green: 0.16 blue: 0.32 alpha: 0.2].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
