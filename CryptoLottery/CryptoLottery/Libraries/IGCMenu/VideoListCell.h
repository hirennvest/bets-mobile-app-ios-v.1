//
//  VideoListCell.h
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-05.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *videolistview;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

NS_ASSUME_NONNULL_END
