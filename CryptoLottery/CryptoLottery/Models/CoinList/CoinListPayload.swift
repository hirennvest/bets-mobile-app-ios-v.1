//
//	CoinListPayload.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CoinListPayload : NSObject, NSCoding{

	var coin : String!
	var comingSoon : Bool!
	var imageUrl : String!
	var name : String!
	var type : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
        coin = (dictionary["coin"] as? String)!.uppercased()
		comingSoon = dictionary["coming_soon"] as? Bool
		imageUrl = dictionary["image_url"] as? String
		name = dictionary["name"] as? String
		type = dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if coin != nil{
			dictionary["coin"] = coin.uppercased()
		}
		if comingSoon != nil{
			dictionary["coming_soon"] = comingSoon
		}
		if imageUrl != nil{
			dictionary["image_url"] = imageUrl
		}
		if name != nil{
			dictionary["name"] = name
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        coin = (aDecoder.decodeObject(forKey: "coin") as? String)!.uppercased()
         comingSoon = aDecoder.decodeObject(forKey: "coming_soon") as? Bool
         imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if coin != nil{
			aCoder.encode(coin.uppercased(), forKey: "coin")
		}
		if comingSoon != nil{
			aCoder.encode(comingSoon, forKey: "coming_soon")
		}
		if imageUrl != nil{
			aCoder.encode(imageUrl, forKey: "image_url")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
