//
//	CoinListRootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CoinListRootClass : NSObject, NSCoding{

	var payload : [CoinListPayload]!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		payload = [CoinListPayload]()
		if let payloadArray = dictionary["payload"] as? [[String:Any]]{
			for dic in payloadArray{
				let value = CoinListPayload(fromDictionary: dic)
				payload.append(value)
			}
		}
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if payload != nil{
			var dictionaryElements = [[String:Any]]()
			for payloadElement in payload {
				dictionaryElements.append(payloadElement.toDictionary())
			}
			dictionary["payload"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         payload = aDecoder.decodeObject(forKey :"payload") as? [CoinListPayload]
         status = aDecoder.decodeObject(forKey: "status") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if payload != nil{
			aCoder.encode(payload, forKey: "payload")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}