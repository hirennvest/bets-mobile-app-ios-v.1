//
//    ChatsMessage.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ChatsMessage {

    var chatid : String!
    var message : String!
    var sender : String!
    var identifier : String!
    var date : Double!
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        chatid = dictionary["id"] as? String
        message = dictionary["message"] as? String
        sender = dictionary["sender"] as? String
        identifier = dictionary["thread_id"] as? String
        date = dictionary["timestamp"] as? Double
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if chatid != nil{
            dictionary["id"] = chatid
        }
        if message != nil{
            dictionary["message"] = message
        }
        if sender != nil{
            dictionary["sender"] = sender
        }
        if identifier != nil{
            dictionary["thread_id"] = identifier
        }
        if date != nil{
            dictionary["timestamp"] = date
        }
        return dictionary
    }

}
