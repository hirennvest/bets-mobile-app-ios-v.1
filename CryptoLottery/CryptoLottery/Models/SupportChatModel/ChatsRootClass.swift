//
//    ChatsRootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ChatsRootClass{

    var message : [ChatsMessage]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        message = [ChatsMessage]()
        if let messageArray = dictionary["message"] as? [[String:Any]]{
            for dic in messageArray{
                let value = ChatsMessage(fromDictionary: dic)
                message.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            var dictionaryElements = [[String:Any]]()
            for messageElement in message {
                dictionaryElements.append(messageElement.toDictionary())
            }
            dictionary["message"] = dictionaryElements
        }
        return dictionary
    }

}
