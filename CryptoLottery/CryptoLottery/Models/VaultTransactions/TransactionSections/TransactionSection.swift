//
//  TransactionSection.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-18.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import Foundation

class TransactionSection : NSObject, NSCoding{

    var sectionArray : [SectionArray]!
    var sectionkey : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        sectionArray = [SectionArray]()
        if let sectionArrayArray = dictionary["SectionArray"] as? [[String:Any]]{
            for dic in sectionArrayArray{
                let value = SectionArray(fromDictionary: dic)
                sectionArray.append(value)
            }
        }
        sectionkey = dictionary["sectionkey"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sectionArray != nil{
            var dictionaryElements = [[String:Any]]()
            for sectionArrayElement in sectionArray {
                dictionaryElements.append(sectionArrayElement.toDictionary())
            }
            dictionary["SectionArray"] = dictionaryElements
        }
        if sectionkey != nil{
            dictionary["sectionkey"] = sectionkey
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         sectionArray = aDecoder.decodeObject(forKey :"SectionArray") as? [SectionArray]
         sectionkey = aDecoder.decodeObject(forKey: "sectionkey") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if sectionArray != nil{
            aCoder.encode(sectionArray, forKey: "SectionArray")
        }
        if sectionkey != nil{
            aCoder.encode(sectionkey, forKey: "sectionkey")
        }

    }

}

class SectionArray : NSObject, NSCoding{

    var sectionkey : String!
    var transactionList : [VaultTransaction_TransactionList]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        sectionkey = dictionary["sectionkey"] as? String
        transactionList = [VaultTransaction_TransactionList]()
        if let transactionListArray = dictionary["transaction_list"] as? [[String:Any]]{
            for dic in transactionListArray{
                let value = VaultTransaction_TransactionList(fromDictionary: dic)
                transactionList.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sectionkey != nil{
            dictionary["sectionkey"] = sectionkey
        }
        if transactionList != nil{
            var dictionaryElements = [[String:Any]]()
            for transactionListElement in transactionList {
                dictionaryElements.append(transactionListElement.toDictionary())
            }
            dictionary["transaction_list"] = dictionaryElements
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         sectionkey = aDecoder.decodeObject(forKey: "sectionkey") as? String
         transactionList = aDecoder.decodeObject(forKey :"transaction_list") as? [VaultTransaction_TransactionList]

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if sectionkey != nil{
            aCoder.encode(sectionkey, forKey: "sectionkey")
        }
        if transactionList != nil{
            aCoder.encode(transactionList, forKey: "transaction_list")
        }

    }

}
