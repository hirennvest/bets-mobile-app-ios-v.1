//
//    VaultTransaction_TransactionList.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class VaultTransaction_TransactionList : NSObject, NSCoding{

    var id : String!
    var arcadeCode : String!
    var arcadeId : String!
    var buy : Bool!
    var clientApp : String!
    var credit : Bool!
    var debit : Bool!
    var deductedSourceAmount : Double!
    var destCoin : String!
    var destination : String!
    var directTransaction : Bool!
    var email : String!
    var indirectTransaction : Bool!
    var internalTrade : Bool!
    var lottery : String!
    var lotteryId : String!
    var lotteryTopup : Bool!
    var participantJackpotReward : Bool!
    var previousBalance : Double!
    var reclaim : Bool!
    var source : String!
    var sourceCoin : String!
    var ticketPrice : Double!
    var tickets : [String]!
    var timestamp : Double!
    var txnId : String!
    var txnType : String!
    var txnValue : Double!
    var updatedBalance : Double!
    var vaultCoin : String!
    var xyz : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["_id"] as? String
        arcadeCode = dictionary["arcade_code"] as? String
        arcadeId = dictionary["arcade_id"] as? String
        buy = dictionary["buy"] as? Bool
        clientApp = dictionary["client_app"] as? String
        credit = dictionary["credit"] as? Bool
        debit = dictionary["debit"] as? Bool
        deductedSourceAmount = dictionary["deducted_source_amount"] as? Double
        destCoin = dictionary["dest_coin"] as? String
        destination = dictionary["destination"] as? String
        directTransaction = dictionary["direct_transaction"] as? Bool
        email = dictionary["email"] as? String
        indirectTransaction = dictionary["indirect_transaction"] as? Bool
        internalTrade = dictionary["internal_trade"] as? Bool
        lottery = dictionary["lottery"] as? String
        lotteryId = dictionary["lottery_id"] as? String
        lotteryTopup = dictionary["lottery_topup"] as? Bool
        participantJackpotReward = dictionary["participant_jackpot_reward"] as? Bool
        previousBalance = dictionary["previous_balance"] as? Double
        reclaim = dictionary["reclaim"] as? Bool
        source = dictionary["source"] as? String
        sourceCoin = dictionary["source_coin"] as? String
        ticketPrice = dictionary["ticket_price"] as? Double
        tickets = dictionary["tickets"] as? [String]
        timestamp = dictionary["timestamp"] as? Double
        txnId = dictionary["txn_id"] as? String
        txnType = dictionary["txn_type"] as? String
        txnValue = dictionary["txn_value"] as? Double
        updatedBalance = dictionary["updated_balance"] as? Double
        vaultCoin = dictionary["vault_coin"] as? String
        xyz = dictionary["xyz"] as? Bool
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["_id"] = id
        }
        if arcadeCode != nil{
            dictionary["arcade_code"] = arcadeCode
        }
        if arcadeId != nil{
            dictionary["arcade_id"] = arcadeId
        }
        if buy != nil{
            dictionary["buy"] = buy
        }
        if clientApp != nil{
            dictionary["client_app"] = clientApp
        }
        if credit != nil{
            dictionary["credit"] = credit
        }
        if debit != nil{
            dictionary["debit"] = debit
        }
        if deductedSourceAmount != nil{
            dictionary["deducted_source_amount"] = deductedSourceAmount
        }
        if destCoin != nil{
            dictionary["dest_coin"] = destCoin
        }
        if destination != nil{
            dictionary["destination"] = destination
        }
        if directTransaction != nil{
            dictionary["direct_transaction"] = directTransaction
        }
        if email != nil{
            dictionary["email"] = email
        }
        if indirectTransaction != nil{
            dictionary["indirect_transaction"] = indirectTransaction
        }
        if internalTrade != nil{
            dictionary["internal_trade"] = internalTrade
        }
        if lottery != nil{
            dictionary["lottery"] = lottery
        }
        if lotteryId != nil{
            dictionary["lottery_id"] = lotteryId
        }
        if lotteryTopup != nil{
            dictionary["lottery_topup"] = lotteryTopup
        }
        if participantJackpotReward != nil{
            dictionary["participant_jackpot_reward"] = participantJackpotReward
        }
        if previousBalance != nil{
            dictionary["previous_balance"] = previousBalance
        }
        if reclaim != nil{
            dictionary["reclaim"] = reclaim
        }
        if source != nil{
            dictionary["source"] = source
        }
        if sourceCoin != nil{
            dictionary["source_coin"] = sourceCoin
        }
        if ticketPrice != nil{
            dictionary["ticket_price"] = ticketPrice
        }
        if tickets != nil{
            dictionary["tickets"] = tickets
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if txnId != nil{
            dictionary["txn_id"] = txnId
        }
        if txnType != nil{
            dictionary["txn_type"] = txnType
        }
        if txnValue != nil{
            dictionary["txn_value"] = txnValue
        }
        if updatedBalance != nil{
            dictionary["updated_balance"] = updatedBalance
        }
        if vaultCoin != nil{
            dictionary["vault_coin"] = vaultCoin
        }
        if xyz != nil{
            dictionary["xyz"] = xyz
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         id = aDecoder.decodeObject(forKey: "_id") as? String
         arcadeCode = aDecoder.decodeObject(forKey: "arcade_code") as? String
         arcadeId = aDecoder.decodeObject(forKey: "arcade_id") as? String
         buy = aDecoder.decodeObject(forKey: "buy") as? Bool
         clientApp = aDecoder.decodeObject(forKey: "client_app") as? String
         credit = aDecoder.decodeObject(forKey: "credit") as? Bool
         debit = aDecoder.decodeObject(forKey: "debit") as? Bool
         deductedSourceAmount = aDecoder.decodeObject(forKey: "deducted_source_amount") as? Double
         destCoin = aDecoder.decodeObject(forKey: "dest_coin") as? String
         destination = aDecoder.decodeObject(forKey: "destination") as? String
         directTransaction = aDecoder.decodeObject(forKey: "direct_transaction") as? Bool
         email = aDecoder.decodeObject(forKey: "email") as? String
         indirectTransaction = aDecoder.decodeObject(forKey: "indirect_transaction") as? Bool
         internalTrade = aDecoder.decodeObject(forKey: "internal_trade") as? Bool
         lottery = aDecoder.decodeObject(forKey: "lottery") as? String
         lotteryId = aDecoder.decodeObject(forKey: "lottery_id") as? String
         lotteryTopup = aDecoder.decodeObject(forKey: "lottery_topup") as? Bool
         participantJackpotReward = aDecoder.decodeObject(forKey: "participant_jackpot_reward") as? Bool
         previousBalance = aDecoder.decodeObject(forKey: "previous_balance") as? Double
         reclaim = aDecoder.decodeObject(forKey: "reclaim") as? Bool
         source = aDecoder.decodeObject(forKey: "source") as? String
         sourceCoin = aDecoder.decodeObject(forKey: "source_coin") as? String
         ticketPrice = aDecoder.decodeObject(forKey: "ticket_price") as? Double
         tickets = aDecoder.decodeObject(forKey: "tickets") as? [String]
         timestamp = aDecoder.decodeObject(forKey: "timestamp") as? Double
         txnId = aDecoder.decodeObject(forKey: "txn_id") as? String
         txnType = aDecoder.decodeObject(forKey: "txn_type") as? String
         txnValue = aDecoder.decodeObject(forKey: "txn_value") as? Double
         updatedBalance = aDecoder.decodeObject(forKey: "updated_balance") as? Double
         vaultCoin = aDecoder.decodeObject(forKey: "vault_coin") as? String
         xyz = aDecoder.decodeObject(forKey: "xyz") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "_id")
        }
        if arcadeCode != nil{
            aCoder.encode(arcadeCode, forKey: "arcade_code")
        }
        if arcadeId != nil{
            aCoder.encode(arcadeId, forKey: "arcade_id")
        }
        if buy != nil{
            aCoder.encode(buy, forKey: "buy")
        }
        if clientApp != nil{
            aCoder.encode(clientApp, forKey: "client_app")
        }
        if credit != nil{
            aCoder.encode(credit, forKey: "credit")
        }
        if debit != nil{
            aCoder.encode(debit, forKey: "debit")
        }
        if deductedSourceAmount != nil{
            aCoder.encode(deductedSourceAmount, forKey: "deducted_source_amount")
        }
        if destCoin != nil{
            aCoder.encode(destCoin, forKey: "dest_coin")
        }
        if destination != nil{
            aCoder.encode(destination, forKey: "destination")
        }
        if directTransaction != nil{
            aCoder.encode(directTransaction, forKey: "direct_transaction")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if indirectTransaction != nil{
            aCoder.encode(indirectTransaction, forKey: "indirect_transaction")
        }
        if internalTrade != nil{
            aCoder.encode(internalTrade, forKey: "internal_trade")
        }
        if lottery != nil{
            aCoder.encode(lottery, forKey: "lottery")
        }
        if lotteryId != nil{
            aCoder.encode(lotteryId, forKey: "lottery_id")
        }
        if lotteryTopup != nil{
            aCoder.encode(lotteryTopup, forKey: "lottery_topup")
        }
        if participantJackpotReward != nil{
            aCoder.encode(participantJackpotReward, forKey: "participant_jackpot_reward")
        }
        if previousBalance != nil{
            aCoder.encode(previousBalance, forKey: "previous_balance")
        }
        if reclaim != nil{
            aCoder.encode(reclaim, forKey: "reclaim")
        }
        if source != nil{
            aCoder.encode(source, forKey: "source")
        }
        if sourceCoin != nil{
            aCoder.encode(sourceCoin, forKey: "source_coin")
        }
        if ticketPrice != nil{
            aCoder.encode(ticketPrice, forKey: "ticket_price")
        }
        if tickets != nil{
            aCoder.encode(tickets, forKey: "tickets")
        }
        if timestamp != nil{
            aCoder.encode(timestamp, forKey: "timestamp")
        }
        if txnId != nil{
            aCoder.encode(txnId, forKey: "txn_id")
        }
        if txnType != nil{
            aCoder.encode(txnType, forKey: "txn_type")
        }
        if txnValue != nil{
            aCoder.encode(txnValue, forKey: "txn_value")
        }
        if updatedBalance != nil{
            aCoder.encode(updatedBalance, forKey: "updated_balance")
        }
        if vaultCoin != nil{
            aCoder.encode(vaultCoin, forKey: "vault_coin")
        }
        if xyz != nil{
            aCoder.encode(xyz, forKey: "xyz")
        }

    }

}
