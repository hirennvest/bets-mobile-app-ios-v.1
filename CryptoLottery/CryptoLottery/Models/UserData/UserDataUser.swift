//
//	UserDataUser.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserDataUser : NSObject, NSCoding{

	var bos : Bool!
	var bosProfileId : String!
	var malls : Bool!
	var mallsProfileId : String!
	var pS : Bool!
	var pSProfileId : String!
	var vSA : Bool!
	var vSAProfileId : String!
	var accountingtool : Bool!
	var accountingtoolProfileId : String!
	var affiliateId : String!
	var betseco : Bool!
	var betsecoProfileId : String!
	var broker : Bool!
	var counsel : Bool!
	var counselProfileId : String!
	var cryb : Bool!
	var crybProfileId : String!
	var cryd : Bool!
	var crydProfileId : String!
	var email : String!
	var funds : Bool!
	var fundsProfileId : String!
	var gxBusinessAccount : Bool!
	var gxBusinessAccountProfileId : String!
	var gxtrade : Bool!
	var gxtradeProfileId : String!
	var instacrypto : Bool!
	var instacryptoProfileId : String!
	var insurancepremiums : Bool!
	var insurancepremiumsProfileId : String!
	var lx : Bool!
	var lxProfileId : String!
	var mailkings : Bool!
	var mailkingsProfileId : String!
	var name : String!
	var nvestapp : Bool!
	var nvestappProfileId : String!
	var onhold : Bool!
	var onholdProfileId : String!
	var profileImg : String!
	var refAffiliate : String!
	var teller : Bool!
	var tellerProfileId : String!
	var terminals : Bool!
	var terminalsProfileId : String!
	var tokenOptions : Bool!
	var tokenOptionsProfileId : String!
	var userId : String!
	var username : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		bos = dictionary["Bos"] as? Bool
		bosProfileId = dictionary["Bos_profile_id"] as? String
		malls = dictionary["Malls"] as? Bool
		mallsProfileId = dictionary["Malls_profile_id"] as? String
		pS = dictionary["PS"] as? Bool
		pSProfileId = dictionary["PS_profile_id"] as? String
		vSA = dictionary["VSA"] as? Bool
		vSAProfileId = dictionary["VSA_profile_id"] as? String
		accountingtool = dictionary["accountingtool"] as? Bool
		accountingtoolProfileId = dictionary["accountingtool_profile_id"] as? String
		affiliateId = dictionary["affiliate_id"] as? String
		betseco = dictionary["betseco"] as? Bool
		betsecoProfileId = dictionary["betseco_profile_id"] as? String
		broker = dictionary["broker"] as? Bool
		counsel = dictionary["counsel"] as? Bool
		counselProfileId = dictionary["counsel_profile_id"] as? String
		cryb = dictionary["cryb"] as? Bool
		crybProfileId = dictionary["cryb_profile_id"] as? String
		cryd = dictionary["cryd"] as? Bool
		crydProfileId = dictionary["cryd_profile_id"] as? String
		email = dictionary["email"] as? String
		funds = dictionary["funds"] as? Bool
		fundsProfileId = dictionary["funds_profile_id"] as? String
		gxBusinessAccount = dictionary["gx_business_account"] as? Bool
		gxBusinessAccountProfileId = dictionary["gx_business_account_profile_id"] as? String
		gxtrade = dictionary["gxtrade"] as? Bool
		gxtradeProfileId = dictionary["gxtrade_profile_id"] as? String
		instacrypto = dictionary["instacrypto"] as? Bool
		instacryptoProfileId = dictionary["instacrypto_profile_id"] as? String
		insurancepremiums = dictionary["insurancepremiums"] as? Bool
		insurancepremiumsProfileId = dictionary["insurancepremiums_profile_id"] as? String
		lx = dictionary["lx"] as? Bool
		lxProfileId = dictionary["lx_profile_id"] as? String
		mailkings = dictionary["mailkings"] as? Bool
		mailkingsProfileId = dictionary["mailkings_profile_id"] as? String
		name = dictionary["name"] as? String
		nvestapp = dictionary["nvestapp"] as? Bool
		nvestappProfileId = dictionary["nvestapp_profile_id"] as? String
		onhold = dictionary["onhold"] as? Bool
		onholdProfileId = dictionary["onhold_profile_id"] as? String
		profileImg = dictionary["profile_img"] as? String
		refAffiliate = dictionary["ref_affiliate"] as? String
		teller = dictionary["teller"] as? Bool
		tellerProfileId = dictionary["teller_profile_id"] as? String
		terminals = dictionary["terminals"] as? Bool
		terminalsProfileId = dictionary["terminals_profile_id"] as? String
		tokenOptions = dictionary["tokenOptions"] as? Bool
		tokenOptionsProfileId = dictionary["tokenOptions_profile_id"] as? String
		userId = dictionary["user_id"] as? String
		username = dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if bos != nil{
			dictionary["Bos"] = bos
		}
		if bosProfileId != nil{
			dictionary["Bos_profile_id"] = bosProfileId
		}
		if malls != nil{
			dictionary["Malls"] = malls
		}
		if mallsProfileId != nil{
			dictionary["Malls_profile_id"] = mallsProfileId
		}
		if pS != nil{
			dictionary["PS"] = pS
		}
		if pSProfileId != nil{
			dictionary["PS_profile_id"] = pSProfileId
		}
		if vSA != nil{
			dictionary["VSA"] = vSA
		}
		if vSAProfileId != nil{
			dictionary["VSA_profile_id"] = vSAProfileId
		}
		if accountingtool != nil{
			dictionary["accountingtool"] = accountingtool
		}
		if accountingtoolProfileId != nil{
			dictionary["accountingtool_profile_id"] = accountingtoolProfileId
		}
		if affiliateId != nil{
			dictionary["affiliate_id"] = affiliateId
		}
		if betseco != nil{
			dictionary["betseco"] = betseco
		}
		if betsecoProfileId != nil{
			dictionary["betseco_profile_id"] = betsecoProfileId
		}
		if broker != nil{
			dictionary["broker"] = broker
		}
		if counsel != nil{
			dictionary["counsel"] = counsel
		}
		if counselProfileId != nil{
			dictionary["counsel_profile_id"] = counselProfileId
		}
		if cryb != nil{
			dictionary["cryb"] = cryb
		}
		if crybProfileId != nil{
			dictionary["cryb_profile_id"] = crybProfileId
		}
		if cryd != nil{
			dictionary["cryd"] = cryd
		}
		if crydProfileId != nil{
			dictionary["cryd_profile_id"] = crydProfileId
		}
		if email != nil{
			dictionary["email"] = email
		}
		if funds != nil{
			dictionary["funds"] = funds
		}
		if fundsProfileId != nil{
			dictionary["funds_profile_id"] = fundsProfileId
		}
		if gxBusinessAccount != nil{
			dictionary["gx_business_account"] = gxBusinessAccount
		}
		if gxBusinessAccountProfileId != nil{
			dictionary["gx_business_account_profile_id"] = gxBusinessAccountProfileId
		}
		if gxtrade != nil{
			dictionary["gxtrade"] = gxtrade
		}
		if gxtradeProfileId != nil{
			dictionary["gxtrade_profile_id"] = gxtradeProfileId
		}
		if instacrypto != nil{
			dictionary["instacrypto"] = instacrypto
		}
		if instacryptoProfileId != nil{
			dictionary["instacrypto_profile_id"] = instacryptoProfileId
		}
		if insurancepremiums != nil{
			dictionary["insurancepremiums"] = insurancepremiums
		}
		if insurancepremiumsProfileId != nil{
			dictionary["insurancepremiums_profile_id"] = insurancepremiumsProfileId
		}
		if lx != nil{
			dictionary["lx"] = lx
		}
		if lxProfileId != nil{
			dictionary["lx_profile_id"] = lxProfileId
		}
		if mailkings != nil{
			dictionary["mailkings"] = mailkings
		}
		if mailkingsProfileId != nil{
			dictionary["mailkings_profile_id"] = mailkingsProfileId
		}
		if name != nil{
			dictionary["name"] = name
		}
		if nvestapp != nil{
			dictionary["nvestapp"] = nvestapp
		}
		if nvestappProfileId != nil{
			dictionary["nvestapp_profile_id"] = nvestappProfileId
		}
		if onhold != nil{
			dictionary["onhold"] = onhold
		}
		if onholdProfileId != nil{
			dictionary["onhold_profile_id"] = onholdProfileId
		}
		if profileImg != nil{
			dictionary["profile_img"] = profileImg
		}
		if refAffiliate != nil{
			dictionary["ref_affiliate"] = refAffiliate
		}
		if teller != nil{
			dictionary["teller"] = teller
		}
		if tellerProfileId != nil{
			dictionary["teller_profile_id"] = tellerProfileId
		}
		if terminals != nil{
			dictionary["terminals"] = terminals
		}
		if terminalsProfileId != nil{
			dictionary["terminals_profile_id"] = terminalsProfileId
		}
		if tokenOptions != nil{
			dictionary["tokenOptions"] = tokenOptions
		}
		if tokenOptionsProfileId != nil{
			dictionary["tokenOptions_profile_id"] = tokenOptionsProfileId
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bos = aDecoder.decodeObject(forKey: "Bos") as? Bool
         bosProfileId = aDecoder.decodeObject(forKey: "Bos_profile_id") as? String
         malls = aDecoder.decodeObject(forKey: "Malls") as? Bool
         mallsProfileId = aDecoder.decodeObject(forKey: "Malls_profile_id") as? String
         pS = aDecoder.decodeObject(forKey: "PS") as? Bool
         pSProfileId = aDecoder.decodeObject(forKey: "PS_profile_id") as? String
         vSA = aDecoder.decodeObject(forKey: "VSA") as? Bool
         vSAProfileId = aDecoder.decodeObject(forKey: "VSA_profile_id") as? String
         accountingtool = aDecoder.decodeObject(forKey: "accountingtool") as? Bool
         accountingtoolProfileId = aDecoder.decodeObject(forKey: "accountingtool_profile_id") as? String
         affiliateId = aDecoder.decodeObject(forKey: "affiliate_id") as? String
         betseco = aDecoder.decodeObject(forKey: "betseco") as? Bool
         betsecoProfileId = aDecoder.decodeObject(forKey: "betseco_profile_id") as? String
         broker = aDecoder.decodeObject(forKey: "broker") as? Bool
         counsel = aDecoder.decodeObject(forKey: "counsel") as? Bool
         counselProfileId = aDecoder.decodeObject(forKey: "counsel_profile_id") as? String
         cryb = aDecoder.decodeObject(forKey: "cryb") as? Bool
         crybProfileId = aDecoder.decodeObject(forKey: "cryb_profile_id") as? String
         cryd = aDecoder.decodeObject(forKey: "cryd") as? Bool
         crydProfileId = aDecoder.decodeObject(forKey: "cryd_profile_id") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         funds = aDecoder.decodeObject(forKey: "funds") as? Bool
         fundsProfileId = aDecoder.decodeObject(forKey: "funds_profile_id") as? String
         gxBusinessAccount = aDecoder.decodeObject(forKey: "gx_business_account") as? Bool
         gxBusinessAccountProfileId = aDecoder.decodeObject(forKey: "gx_business_account_profile_id") as? String
         gxtrade = aDecoder.decodeObject(forKey: "gxtrade") as? Bool
         gxtradeProfileId = aDecoder.decodeObject(forKey: "gxtrade_profile_id") as? String
         instacrypto = aDecoder.decodeObject(forKey: "instacrypto") as? Bool
         instacryptoProfileId = aDecoder.decodeObject(forKey: "instacrypto_profile_id") as? String
         insurancepremiums = aDecoder.decodeObject(forKey: "insurancepremiums") as? Bool
         insurancepremiumsProfileId = aDecoder.decodeObject(forKey: "insurancepremiums_profile_id") as? String
         lx = aDecoder.decodeObject(forKey: "lx") as? Bool
         lxProfileId = aDecoder.decodeObject(forKey: "lx_profile_id") as? String
         mailkings = aDecoder.decodeObject(forKey: "mailkings") as? Bool
         mailkingsProfileId = aDecoder.decodeObject(forKey: "mailkings_profile_id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         nvestapp = aDecoder.decodeObject(forKey: "nvestapp") as? Bool
         nvestappProfileId = aDecoder.decodeObject(forKey: "nvestapp_profile_id") as? String
         onhold = aDecoder.decodeObject(forKey: "onhold") as? Bool
         onholdProfileId = aDecoder.decodeObject(forKey: "onhold_profile_id") as? String
         profileImg = aDecoder.decodeObject(forKey: "profile_img") as? String
         refAffiliate = aDecoder.decodeObject(forKey: "ref_affiliate") as? String
         teller = aDecoder.decodeObject(forKey: "teller") as? Bool
         tellerProfileId = aDecoder.decodeObject(forKey: "teller_profile_id") as? String
         terminals = aDecoder.decodeObject(forKey: "terminals") as? Bool
         terminalsProfileId = aDecoder.decodeObject(forKey: "terminals_profile_id") as? String
         tokenOptions = aDecoder.decodeObject(forKey: "tokenOptions") as? Bool
         tokenOptionsProfileId = aDecoder.decodeObject(forKey: "tokenOptions_profile_id") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bos != nil{
			aCoder.encode(bos, forKey: "Bos")
		}
		if bosProfileId != nil{
			aCoder.encode(bosProfileId, forKey: "Bos_profile_id")
		}
		if malls != nil{
			aCoder.encode(malls, forKey: "Malls")
		}
		if mallsProfileId != nil{
			aCoder.encode(mallsProfileId, forKey: "Malls_profile_id")
		}
		if pS != nil{
			aCoder.encode(pS, forKey: "PS")
		}
		if pSProfileId != nil{
			aCoder.encode(pSProfileId, forKey: "PS_profile_id")
		}
		if vSA != nil{
			aCoder.encode(vSA, forKey: "VSA")
		}
		if vSAProfileId != nil{
			aCoder.encode(vSAProfileId, forKey: "VSA_profile_id")
		}
		if accountingtool != nil{
			aCoder.encode(accountingtool, forKey: "accountingtool")
		}
		if accountingtoolProfileId != nil{
			aCoder.encode(accountingtoolProfileId, forKey: "accountingtool_profile_id")
		}
		if affiliateId != nil{
			aCoder.encode(affiliateId, forKey: "affiliate_id")
		}
		if betseco != nil{
			aCoder.encode(betseco, forKey: "betseco")
		}
		if betsecoProfileId != nil{
			aCoder.encode(betsecoProfileId, forKey: "betseco_profile_id")
		}
		if broker != nil{
			aCoder.encode(broker, forKey: "broker")
		}
		if counsel != nil{
			aCoder.encode(counsel, forKey: "counsel")
		}
		if counselProfileId != nil{
			aCoder.encode(counselProfileId, forKey: "counsel_profile_id")
		}
		if cryb != nil{
			aCoder.encode(cryb, forKey: "cryb")
		}
		if crybProfileId != nil{
			aCoder.encode(crybProfileId, forKey: "cryb_profile_id")
		}
		if cryd != nil{
			aCoder.encode(cryd, forKey: "cryd")
		}
		if crydProfileId != nil{
			aCoder.encode(crydProfileId, forKey: "cryd_profile_id")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if funds != nil{
			aCoder.encode(funds, forKey: "funds")
		}
		if fundsProfileId != nil{
			aCoder.encode(fundsProfileId, forKey: "funds_profile_id")
		}
		if gxBusinessAccount != nil{
			aCoder.encode(gxBusinessAccount, forKey: "gx_business_account")
		}
		if gxBusinessAccountProfileId != nil{
			aCoder.encode(gxBusinessAccountProfileId, forKey: "gx_business_account_profile_id")
		}
		if gxtrade != nil{
			aCoder.encode(gxtrade, forKey: "gxtrade")
		}
		if gxtradeProfileId != nil{
			aCoder.encode(gxtradeProfileId, forKey: "gxtrade_profile_id")
		}
		if instacrypto != nil{
			aCoder.encode(instacrypto, forKey: "instacrypto")
		}
		if instacryptoProfileId != nil{
			aCoder.encode(instacryptoProfileId, forKey: "instacrypto_profile_id")
		}
		if insurancepremiums != nil{
			aCoder.encode(insurancepremiums, forKey: "insurancepremiums")
		}
		if insurancepremiumsProfileId != nil{
			aCoder.encode(insurancepremiumsProfileId, forKey: "insurancepremiums_profile_id")
		}
		if lx != nil{
			aCoder.encode(lx, forKey: "lx")
		}
		if lxProfileId != nil{
			aCoder.encode(lxProfileId, forKey: "lx_profile_id")
		}
		if mailkings != nil{
			aCoder.encode(mailkings, forKey: "mailkings")
		}
		if mailkingsProfileId != nil{
			aCoder.encode(mailkingsProfileId, forKey: "mailkings_profile_id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if nvestapp != nil{
			aCoder.encode(nvestapp, forKey: "nvestapp")
		}
		if nvestappProfileId != nil{
			aCoder.encode(nvestappProfileId, forKey: "nvestapp_profile_id")
		}
		if onhold != nil{
			aCoder.encode(onhold, forKey: "onhold")
		}
		if onholdProfileId != nil{
			aCoder.encode(onholdProfileId, forKey: "onhold_profile_id")
		}
		if profileImg != nil{
			aCoder.encode(profileImg, forKey: "profile_img")
		}
		if refAffiliate != nil{
			aCoder.encode(refAffiliate, forKey: "ref_affiliate")
		}
		if teller != nil{
			aCoder.encode(teller, forKey: "teller")
		}
		if tellerProfileId != nil{
			aCoder.encode(tellerProfileId, forKey: "teller_profile_id")
		}
		if terminals != nil{
			aCoder.encode(terminals, forKey: "terminals")
		}
		if terminalsProfileId != nil{
			aCoder.encode(terminalsProfileId, forKey: "terminals_profile_id")
		}
		if tokenOptions != nil{
			aCoder.encode(tokenOptions, forKey: "tokenOptions")
		}
		if tokenOptionsProfileId != nil{
			aCoder.encode(tokenOptionsProfileId, forKey: "tokenOptions_profile_id")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}