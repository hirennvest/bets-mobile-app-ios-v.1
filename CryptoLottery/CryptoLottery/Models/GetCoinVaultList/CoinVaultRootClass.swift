//
//    CoinVaultRootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CoinVaultRootClass : NSObject, NSCoding{

    var coins : [CoinVaultCoin]!
    var message : String!
    var status : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        coins = [CoinVaultCoin]()
        if let coinsArray = dictionary["coins"] as? [[String:Any]]{
            for dic in coinsArray{
                let value = CoinVaultCoin(fromDictionary: dic)
                coins.append(value)
            }
        }
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Bool
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if coins != nil{
            var dictionaryElements = [[String:Any]]()
            for coinsElement in coins {
                dictionaryElements.append(coinsElement.toDictionary())
            }
            dictionary["coins"] = dictionaryElements
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         coins = aDecoder.decodeObject(forKey :"coins") as? [CoinVaultCoin]
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if coins != nil{
            aCoder.encode(coins, forKey: "coins")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }

    }

}


class CoinSectionArray : NSObject, NSCoding{

    var sectionkey : String!
    var sectionArray : [CoinVaultCoin]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        sectionkey = dictionary["sectionkey"] as? String
        sectionArray = [CoinVaultCoin]()
        if let transactionListArray = dictionary["transaction_list"] as? [[String:Any]]{
            for dic in transactionListArray{
                let value = CoinVaultCoin(fromDictionary: dic)
                sectionArray.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if sectionkey != nil{
            dictionary["sectionkey"] = sectionkey
        }
        if sectionArray != nil{
            var dictionaryElements = [[String:Any]]()
            for transactionListElement in sectionArray {
                dictionaryElements.append(transactionListElement.toDictionary())
            }
            dictionary["transaction_list"] = dictionaryElements
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         sectionkey = aDecoder.decodeObject(forKey: "sectionkey") as? String
         sectionArray = aDecoder.decodeObject(forKey :"transaction_list") as? [CoinVaultCoin]

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if sectionkey != nil{
            aCoder.encode(sectionkey, forKey: "sectionkey")
        }
        if sectionArray != nil{
            aCoder.encode(sectionArray, forKey: "transaction_list")
        }

    }

}
