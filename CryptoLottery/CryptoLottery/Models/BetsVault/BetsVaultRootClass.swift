//
//	BetsVaultRootClass.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultRootClass : NSObject, NSCoding{
    

	var payload : BetsVaultPayload!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		if let payloadData = dictionary["payload"] as? NSDictionary{
			payload = BetsVaultPayload(fromDictionary: payloadData)
		}
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if payload != nil{
			dictionary["payload"] = payload.toDictionary()
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         payload = aDecoder.decodeObject(forKey: "payload") as? BetsVaultPayload
         status = aDecoder.decodeObject(forKey: "status") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if payload != nil{
			aCoder.encode(payload, forKey: "payload")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if payload != nil{
            aCoder.encode(payload, forKey: "payload")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }

}
