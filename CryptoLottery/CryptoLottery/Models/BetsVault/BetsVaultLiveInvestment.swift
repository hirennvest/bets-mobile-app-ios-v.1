//
//	BetsVaultLiveInvestment.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultLiveInvestment : NSObject, NSCoding{

	var nativeField : Double!
	var usd : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		nativeField = dictionary["native"] as? Double
		usd = dictionary["usd"] as? Double
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if nativeField != nil{
			dictionary["native"] = nativeField
		}
		if usd != nil{
			dictionary["usd"] = usd
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         nativeField = aDecoder.decodeObject(forKey: "native") as? Double
         usd = aDecoder.decodeObject(forKey: "usd") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if nativeField != nil{
			aCoder.encode(nativeField, forKey: "native")
		}
		if usd != nil{
			aCoder.encode(usd, forKey: "usd")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if nativeField != nil{
            aCoder.encode(nativeField, forKey: "native")
        }
        if usd != nil{
            aCoder.encode(usd, forKey: "usd")
        }
    }

}
