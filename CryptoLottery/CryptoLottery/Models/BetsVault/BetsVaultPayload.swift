//
//	BetsVaultPayload.swift
//
//	Create by Hiren on 29/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class BetsVaultPayload : NSObject, NSCoding{

	var coins : [BetsVaultCoin]!
	var email : String!
	var totalDepositAmount : Double!
	var totalDeposits : Double!
	var totalInvestment : Double!
	var totalLiveInvestment : Double!
	var totalTickets : Double!
	var totalWinnings : Double!
	var totalWithdrawAmount : Double!
	var totalWithdrawals : Double!
	var totalWonAmount : Double!
	var vault : [BetsVaultVault]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		coins = [BetsVaultCoin]()
		if let coinsArray = dictionary["coins"] as? [NSDictionary]{
			for dic in coinsArray{
				let value = BetsVaultCoin(fromDictionary: dic)
				coins.append(value)
			}
		}
		email = dictionary["email"] as? String
		totalDepositAmount = dictionary["total_deposit_amount"] as? Double
		totalDeposits = dictionary["total_deposits"] as? Double
		totalInvestment = dictionary["total_investment"] as? Double
		totalLiveInvestment = dictionary["total_live_investment"] as? Double
		totalTickets = dictionary["total_tickets"] as? Double
		totalWinnings = dictionary["total_winnings"] as? Double
		totalWithdrawAmount = dictionary["total_withdraw_amount"] as? Double
		totalWithdrawals = dictionary["total_withdrawals"] as? Double
		totalWonAmount = dictionary["total_won_amount"] as? Double
		vault = [BetsVaultVault]()
		if let vaultArray = dictionary["vault"] as? [NSDictionary]{
			for dic in vaultArray{
				let value = BetsVaultVault(fromDictionary: dic)
				vault.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if coins != nil{
			var dictionaryElements = [NSDictionary]()
			for coinsElement in coins {
				dictionaryElements.append(coinsElement.toDictionary())
			}
			dictionary["coins"] = dictionaryElements
		}
		if email != nil{
			dictionary["email"] = email
		}
		if totalDepositAmount != nil{
			dictionary["total_deposit_amount"] = totalDepositAmount
		}
		if totalDeposits != nil{
			dictionary["total_deposits"] = totalDeposits
		}
		if totalInvestment != nil{
			dictionary["total_investment"] = totalInvestment
		}
		if totalLiveInvestment != nil{
			dictionary["total_live_investment"] = totalLiveInvestment
		}
		if totalTickets != nil{
			dictionary["total_tickets"] = totalTickets
		}
		if totalWinnings != nil{
			dictionary["total_winnings"] = totalWinnings
		}
		if totalWithdrawAmount != nil{
			dictionary["total_withdraw_amount"] = totalWithdrawAmount
		}
		if totalWithdrawals != nil{
			dictionary["total_withdrawals"] = totalWithdrawals
		}
		if totalWonAmount != nil{
			dictionary["total_won_amount"] = totalWonAmount
		}
		if vault != nil{
			var dictionaryElements = [NSDictionary]()
			for vaultElement in vault {
				dictionaryElements.append(vaultElement.toDictionary())
			}
			dictionary["vault"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         coins = aDecoder.decodeObject(forKey: "coins") as? [BetsVaultCoin]
         email = aDecoder.decodeObject(forKey: "email") as? String
         totalDepositAmount = aDecoder.decodeObject(forKey: "total_deposit_amount") as? Double
         totalDeposits = aDecoder.decodeObject(forKey: "total_deposits") as? Double
         totalInvestment = aDecoder.decodeObject(forKey: "total_investment") as? Double
         totalLiveInvestment = aDecoder.decodeObject(forKey: "total_live_investment") as? Double
         totalTickets = aDecoder.decodeObject(forKey: "total_tickets") as? Double
         totalWinnings = aDecoder.decodeObject(forKey: "total_winnings") as? Double
         totalWithdrawAmount = aDecoder.decodeObject(forKey: "total_withdraw_amount") as? Double
         totalWithdrawals = aDecoder.decodeObject(forKey: "total_withdrawals") as? Double
         totalWonAmount = aDecoder.decodeObject(forKey: "total_won_amount") as? Double
         vault = aDecoder.decodeObject(forKey: "vault") as? [BetsVaultVault]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties Doubleo the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if coins != nil{
			aCoder.encode(coins, forKey: "coins")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if totalDepositAmount != nil{
			aCoder.encode(totalDepositAmount, forKey: "total_deposit_amount")
		}
		if totalDeposits != nil{
			aCoder.encode(totalDeposits, forKey: "total_deposits")
		}
		if totalInvestment != nil{
			aCoder.encode(totalInvestment, forKey: "total_investment")
		}
		if totalLiveInvestment != nil{
			aCoder.encode(totalLiveInvestment, forKey: "total_live_investment")
		}
		if totalTickets != nil{
			aCoder.encode(totalTickets, forKey: "total_tickets")
		}
		if totalWinnings != nil{
			aCoder.encode(totalWinnings, forKey: "total_winnings")
		}
		if totalWithdrawAmount != nil{
			aCoder.encode(totalWithdrawAmount, forKey: "total_withdraw_amount")
		}
		if totalWithdrawals != nil{
			aCoder.encode(totalWithdrawals, forKey: "total_withdrawals")
		}
		if totalWonAmount != nil{
			aCoder.encode(totalWonAmount, forKey: "total_won_amount")
		}
		if vault != nil{
			aCoder.encode(vault, forKey: "vault")
		}

	}
    
    func encode(with aCoder: NSCoder) {
        if coins != nil{
            aCoder.encode(coins, forKey: "coins")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if totalDepositAmount != nil{
            aCoder.encode(totalDepositAmount, forKey: "total_deposit_amount")
        }
        if totalDeposits != nil{
            aCoder.encode(totalDeposits, forKey: "total_deposits")
        }
        if totalInvestment != nil{
            aCoder.encode(totalInvestment, forKey: "total_investment")
        }
        if totalLiveInvestment != nil{
            aCoder.encode(totalLiveInvestment, forKey: "total_live_investment")
        }
        if totalTickets != nil{
            aCoder.encode(totalTickets, forKey: "total_tickets")
        }
        if totalWinnings != nil{
            aCoder.encode(totalWinnings, forKey: "total_winnings")
        }
        if totalWithdrawAmount != nil{
            aCoder.encode(totalWithdrawAmount, forKey: "total_withdraw_amount")
        }
        if totalWithdrawals != nil{
            aCoder.encode(totalWithdrawals, forKey: "total_withdrawals")
        }
        if totalWonAmount != nil{
            aCoder.encode(totalWonAmount, forKey: "total_won_amount")
        }
        if vault != nil{
            aCoder.encode(vault, forKey: "vault")
        }
    }

}
