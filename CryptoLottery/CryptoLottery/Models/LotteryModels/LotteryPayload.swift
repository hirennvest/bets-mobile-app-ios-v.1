//
//	LotteryPayload.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LotteryPayload : NSObject, NSCoding{

	var activeLotteries : [LotteryActiveLottery]!
	var closedLotteries : [LotteryClosedLottery]!
	var freezedLotteries : [LotteryActiveLottery]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		activeLotteries = [LotteryActiveLottery]()
		if let activeLotteriesArray = dictionary["active_lotteries"] as? [[String:Any]]{
			for dic in activeLotteriesArray{
				let value = LotteryActiveLottery(fromDictionary: dic)
				activeLotteries.append(value)
			}
		}
		closedLotteries = [LotteryClosedLottery]()
		if let closedLotteriesArray = dictionary["closed_lotteries"] as? [[String:Any]]{
			for dic in closedLotteriesArray{
				let value = LotteryClosedLottery(fromDictionary: dic)
				closedLotteries.append(value)
			}
		}
		freezedLotteries = [LotteryActiveLottery]()
		if let freezedLotteriesArray = dictionary["freezed_lotteries"] as? [[String:Any]]{
			for dic in freezedLotteriesArray{
				let value = LotteryActiveLottery(fromDictionary: dic)
				freezedLotteries.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if activeLotteries != nil{
			var dictionaryElements = [[String:Any]]()
			for activeLotteriesElement in activeLotteries {
				dictionaryElements.append(activeLotteriesElement.toDictionary())
			}
			dictionary["active_lotteries"] = dictionaryElements
		}
		if closedLotteries != nil{
			var dictionaryElements = [[String:Any]]()
			for closedLotteriesElement in closedLotteries {
				dictionaryElements.append(closedLotteriesElement.toDictionary())
			}
			dictionary["closed_lotteries"] = dictionaryElements
		}
		if freezedLotteries != nil{
			var dictionaryElements = [[String:Any]]()
			for freezedLotteriesElement in freezedLotteries {
				dictionaryElements.append(freezedLotteriesElement.toDictionary())
			}
			dictionary["freezed_lotteries"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         activeLotteries = aDecoder.decodeObject(forKey :"active_lotteries") as? [LotteryActiveLottery]
         closedLotteries = aDecoder.decodeObject(forKey :"closed_lotteries") as? [LotteryClosedLottery]
         freezedLotteries = aDecoder.decodeObject(forKey :"freezed_lotteries") as? [LotteryActiveLottery]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if activeLotteries != nil{
			aCoder.encode(activeLotteries, forKey: "active_lotteries")
		}
		if closedLotteries != nil{
			aCoder.encode(closedLotteries, forKey: "closed_lotteries")
		}
		if freezedLotteries != nil{
			aCoder.encode(freezedLotteries, forKey: "freezed_lotteries")
		}

	}

}
