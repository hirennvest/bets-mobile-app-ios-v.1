//
//	LotteryActiveLottery.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class LotteryActiveLottery : NSObject, NSCoding{

	var brokerJackpotRate : Double!
	var coin : String!
	var endTimestamp : Double!
	var freezeTimestamp : Double!
	var id : String!
	var interestRate : Double!
	var lottery : String!
	var lotteryBalance : Double!
	var participantsJackpotRate : Double!
	var startTimestamp : Double!
	var ticketPrice : Double!
	var ticketsSold : Double!
	var timeZone : String!
	var timeZoneExpanded : String!
	var totalJackpotRate : Double!
	var winnerJackpotRate : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		brokerJackpotRate = dictionary["broker_jackpot_rate"] as? Double
		coin = dictionary["coin"] as? String
		endTimestamp = dictionary["end_timestamp"] as? Double
		freezeTimestamp = dictionary["freeze_timestamp"] as? Double
		id = dictionary["id"] as? String
		interestRate = dictionary["interest_rate"] as? Double
		lottery = dictionary["lottery"] as? String
		lotteryBalance = dictionary["lottery_balance"] as? Double
		participantsJackpotRate = dictionary["participants_jackpot_rate"] as? Double
		startTimestamp = dictionary["start_timestamp"] as? Double
		ticketPrice = dictionary["ticket_price"] as? Double
		ticketsSold = dictionary["tickets_sold"] as? Double
		timeZone = dictionary["time_zone"] as? String
		timeZoneExpanded = dictionary["time_zone_expanded"] as? String
		totalJackpotRate = dictionary["total_jackpot_rate"] as? Double
		winnerJackpotRate = dictionary["winner_jackpot_rate"] as? Double
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if brokerJackpotRate != nil{
			dictionary["broker_jackpot_rate"] = brokerJackpotRate
		}
		if coin != nil{
			dictionary["coin"] = coin
		}
		if endTimestamp != nil{
			dictionary["end_timestamp"] = endTimestamp
		}
		if freezeTimestamp != nil{
			dictionary["freeze_timestamp"] = freezeTimestamp
		}
		if id != nil{
			dictionary["id"] = id
		}
		if interestRate != nil{
			dictionary["interest_rate"] = interestRate
		}
		if lottery != nil{
			dictionary["lottery"] = lottery
		}
		if lotteryBalance != nil{
			dictionary["lottery_balance"] = lotteryBalance
		}
		if participantsJackpotRate != nil{
			dictionary["participants_jackpot_rate"] = participantsJackpotRate
		}
		if startTimestamp != nil{
			dictionary["start_timestamp"] = startTimestamp
		}
		if ticketPrice != nil{
			dictionary["ticket_price"] = ticketPrice
		}
		if ticketsSold != nil{
			dictionary["tickets_sold"] = ticketsSold
		}
		if timeZone != nil{
			dictionary["time_zone"] = timeZone
		}
		if timeZoneExpanded != nil{
			dictionary["time_zone_expanded"] = timeZoneExpanded
		}
		if totalJackpotRate != nil{
			dictionary["total_jackpot_rate"] = totalJackpotRate
		}
		if winnerJackpotRate != nil{
			dictionary["winner_jackpot_rate"] = winnerJackpotRate
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         brokerJackpotRate = aDecoder.decodeObject(forKey: "broker_jackpot_rate") as? Double
         coin = aDecoder.decodeObject(forKey: "coin") as? String
         endTimestamp = aDecoder.decodeObject(forKey: "end_timestamp") as? Double
         freezeTimestamp = aDecoder.decodeObject(forKey: "freeze_timestamp") as? Double
         id = aDecoder.decodeObject(forKey: "id") as? String
         interestRate = aDecoder.decodeObject(forKey: "interest_rate") as? Double
         lottery = aDecoder.decodeObject(forKey: "lottery") as? String
         lotteryBalance = aDecoder.decodeObject(forKey: "lottery_balance") as? Double
         participantsJackpotRate = aDecoder.decodeObject(forKey: "participants_jackpot_rate") as? Double
         startTimestamp = aDecoder.decodeObject(forKey: "start_timestamp") as? Double
         ticketPrice = aDecoder.decodeObject(forKey: "ticket_price") as? Double
         ticketsSold = aDecoder.decodeObject(forKey: "tickets_sold") as? Double
         timeZone = aDecoder.decodeObject(forKey: "time_zone") as? String
         timeZoneExpanded = aDecoder.decodeObject(forKey: "time_zone_expanded") as? String
         totalJackpotRate = aDecoder.decodeObject(forKey: "total_jackpot_rate") as? Double
         winnerJackpotRate = aDecoder.decodeObject(forKey: "winner_jackpot_rate") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if brokerJackpotRate != nil{
			aCoder.encode(brokerJackpotRate, forKey: "broker_jackpot_rate")
		}
		if coin != nil{
			aCoder.encode(coin, forKey: "coin")
		}
		if endTimestamp != nil{
			aCoder.encode(endTimestamp, forKey: "end_timestamp")
		}
		if freezeTimestamp != nil{
			aCoder.encode(freezeTimestamp, forKey: "freeze_timestamp")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if interestRate != nil{
			aCoder.encode(interestRate, forKey: "interest_rate")
		}
		if lottery != nil{
			aCoder.encode(lottery, forKey: "lottery")
		}
		if lotteryBalance != nil{
			aCoder.encode(lotteryBalance, forKey: "lottery_balance")
		}
		if participantsJackpotRate != nil{
			aCoder.encode(participantsJackpotRate, forKey: "participants_jackpot_rate")
		}
		if startTimestamp != nil{
			aCoder.encode(startTimestamp, forKey: "start_timestamp")
		}
		if ticketPrice != nil{
			aCoder.encode(ticketPrice, forKey: "ticket_price")
		}
		if ticketsSold != nil{
			aCoder.encode(ticketsSold, forKey: "tickets_sold")
		}
		if timeZone != nil{
			aCoder.encode(timeZone, forKey: "time_zone")
		}
		if timeZoneExpanded != nil{
			aCoder.encode(timeZoneExpanded, forKey: "time_zone_expanded")
		}
		if totalJackpotRate != nil{
			aCoder.encode(totalJackpotRate, forKey: "total_jackpot_rate")
		}
		if winnerJackpotRate != nil{
			aCoder.encode(winnerJackpotRate, forKey: "winner_jackpot_rate")
		}

	}

}
