//
//	CMCPrices.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CMCPrices : NSObject, NSCoding{

    var bTC : Double!
    var eTH : Double!
    var gXT : Double!
    var lTC : Double!
    var uSDT : Double!
    var xMR : Double!
    var xRP : Double!
    var btcPrice : Double!
    var ethPrice : Double!
    var gxtPrice : Double!
    var message : String!
    var status : Bool!
    var usdtPrice : Double!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        bTC = dictionary["BTC"] as? Double
        eTH = dictionary["ETH"] as? Double
        gXT = dictionary["GXT"] as? Double
        lTC = dictionary["LTC"] as? Double
        uSDT = dictionary["USDT"] as? Double
        xMR = dictionary["XMR"] as? Double
        xRP = dictionary["XRP"] as? Double
        btcPrice = dictionary["btc_price"] as? Double
        ethPrice = dictionary["eth_price"] as? Double
        gxtPrice = dictionary["gxt_price"] as? Double
        message = dictionary["message"] as? String
        status = dictionary["status"] as? Bool
        usdtPrice = dictionary["usdt_price"] as? Double
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bTC != nil{
            dictionary["BTC"] = bTC
        }
        if eTH != nil{
            dictionary["ETH"] = eTH
        }
        if gXT != nil{
            dictionary["GXT"] = gXT
        }
        if lTC != nil{
            dictionary["LTC"] = lTC
        }
        if uSDT != nil{
            dictionary["USDT"] = uSDT
        }
        if xMR != nil{
            dictionary["XMR"] = xMR
        }
        if xRP != nil{
            dictionary["XRP"] = xRP
        }
        if btcPrice != nil{
            dictionary["btc_price"] = btcPrice
        }
        if ethPrice != nil{
            dictionary["eth_price"] = ethPrice
        }
        if gxtPrice != nil{
            dictionary["gxt_price"] = gxtPrice
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if usdtPrice != nil{
            dictionary["usdt_price"] = usdtPrice
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         bTC = aDecoder.decodeObject(forKey: "BTC") as? Double
         eTH = aDecoder.decodeObject(forKey: "ETH") as? Double
         gXT = aDecoder.decodeObject(forKey: "GXT") as? Double
         lTC = aDecoder.decodeObject(forKey: "LTC") as? Double
         uSDT = aDecoder.decodeObject(forKey: "USDT") as? Double
         xMR = aDecoder.decodeObject(forKey: "XMR") as? Double
         xRP = aDecoder.decodeObject(forKey: "XRP") as? Double
         btcPrice = aDecoder.decodeObject(forKey: "btc_price") as? Double
         ethPrice = aDecoder.decodeObject(forKey: "eth_price") as? Double
         gxtPrice = aDecoder.decodeObject(forKey: "gxt_price") as? Double
         message = aDecoder.decodeObject(forKey: "message") as? String
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         usdtPrice = aDecoder.decodeObject(forKey: "usdt_price") as? Double

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if bTC != nil{
            aCoder.encode(bTC, forKey: "BTC")
        }
        if eTH != nil{
            aCoder.encode(eTH, forKey: "ETH")
        }
        if gXT != nil{
            aCoder.encode(gXT, forKey: "GXT")
        }
        if lTC != nil{
            aCoder.encode(lTC, forKey: "LTC")
        }
        if uSDT != nil{
            aCoder.encode(uSDT, forKey: "USDT")
        }
        if xMR != nil{
            aCoder.encode(xMR, forKey: "XMR")
        }
        if xRP != nil{
            aCoder.encode(xRP, forKey: "XRP")
        }
        if btcPrice != nil{
            aCoder.encode(btcPrice, forKey: "btc_price")
        }
        if ethPrice != nil{
            aCoder.encode(ethPrice, forKey: "eth_price")
        }
        if gxtPrice != nil{
            aCoder.encode(gxtPrice, forKey: "gxt_price")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if usdtPrice != nil{
            aCoder.encode(usdtPrice, forKey: "usdt_price")
        }

    }

}
