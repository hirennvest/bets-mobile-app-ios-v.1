//
//    UserBioArcadeName.swift
//
//    Create by Hiren on 30/7/2020
//    Copyright © 2020. All rights reserved.
//    Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserBioArcadeName : NSObject, NSCoding{
    
    var arcadeName : String!
    var arcadeUserid : String!
    var registered : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        arcadeName = dictionary["arcade_name"] as? String
        arcadeUserid = dictionary["arcade_userid"] as? String
        registered = dictionary["registered"] as? Bool
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if arcadeName != nil{
            dictionary["arcade_name"] = arcadeName
        }
        if arcadeUserid != nil{
            dictionary["arcade_userid"] = arcadeUserid
        }
        if registered != nil{
            dictionary["registered"] = registered
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        arcadeName = aDecoder.decodeObject(forKey: "arcade_name") as? String
         arcadeUserid = aDecoder.decodeObject(forKey: "arcade_userid") as? String
         registered = aDecoder.decodeObject(forKey: "registered") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if arcadeName != nil{
            aCoder.encode(arcadeName, forKey: "arcade_name")
        }
        if arcadeUserid != nil{
            aCoder.encode(arcadeUserid, forKey: "arcade_userid")
        }
        if registered != nil{
            aCoder.encode(registered, forKey: "registered")
        }

    }
    
    func encode(with aCoder: NSCoder) {
        if arcadeName != nil{
            aCoder.encode(arcadeName, forKey: "arcade_name")
        }
        if arcadeUserid != nil{
            aCoder.encode(arcadeUserid, forKey: "arcade_userid")
        }
        if registered != nil{
            aCoder.encode(registered, forKey: "registered")
        }
    }

}
