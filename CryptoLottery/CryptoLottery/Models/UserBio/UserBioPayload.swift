//
//	UserBioPayload.swift
//
//	Create by Hiren on 24/7/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserBioPayload : NSObject, NSCoding {
    

	var arcadeIds : [UserBioArcadeId]!
	var arcadeNames : [UserBioArcadeName]!
	var betsLevel : Bool!
	var dealer : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		arcadeIds = [UserBioArcadeId]()
		if let arcadeIdsArray = dictionary["arcade_ids"] as? [NSDictionary]{
			for dic in arcadeIdsArray{
				let value = UserBioArcadeId(fromDictionary: dic)
				arcadeIds.append(value)
			}
		}
		arcadeNames = [UserBioArcadeName]()
		if let arcadeNamesArray = dictionary["arcade_names"] as? [NSDictionary]{
			for dic in arcadeNamesArray{
				let value = UserBioArcadeName(fromDictionary: dic)
				arcadeNames.append(value)
			}
		}
		betsLevel = dictionary["bets_level"] as? Bool
		dealer = dictionary["dealer"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
        let dictionary = NSMutableDictionary()
		if arcadeIds != nil{
			var dictionaryElements = [NSDictionary]()
			for arcadeIdsElement in arcadeIds {
				dictionaryElements.append(arcadeIdsElement.toDictionary())
			}
			dictionary["arcade_ids"] = dictionaryElements
		}
		if arcadeNames != nil{
			var dictionaryElements = [NSDictionary]()
			for arcadeNamesElement in arcadeNames {
				dictionaryElements.append(arcadeNamesElement.toDictionary())
			}
			dictionary["arcade_names"] = dictionaryElements
		}
		if betsLevel != nil{
			dictionary["bets_level"] = betsLevel
		}
		if dealer != nil{
			dictionary["dealer"] = dealer
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        arcadeIds = aDecoder.decodeObject(forKey: "arcade_ids") as? [UserBioArcadeId]
        arcadeNames = aDecoder.decodeObject(forKey: "arcade_names") as? [UserBioArcadeName]
        betsLevel = aDecoder.decodeObject(forKey: "bets_level") as? Bool
        dealer = aDecoder.decodeObject(forKey: "dealer") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if arcadeIds != nil{
			aCoder.encode(arcadeIds, forKey: "arcade_ids")
		}
		if arcadeNames != nil{
			aCoder.encode(arcadeNames, forKey: "arcade_names")
		}
		if betsLevel != nil{
			aCoder.encode(betsLevel, forKey: "bets_level")
		}
		if dealer != nil{
			aCoder.encode(dealer, forKey: "dealer")
		}

	}

    func encode(with aCoder: NSCoder)
    {
        if arcadeIds != nil{
            aCoder.encode(arcadeIds, forKey: "arcade_ids")
        }
        if arcadeNames != nil{
            aCoder.encode(arcadeNames, forKey: "arcade_names")
        }
        if betsLevel != nil{
            aCoder.encode(betsLevel, forKey: "bets_level")
        }
        if dealer != nil{
            aCoder.encode(dealer, forKey: "dealer")
        }

    }

}
