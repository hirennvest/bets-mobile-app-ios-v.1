//
//  RegistrationVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class RegistrationVC: BaseVC {

    // MARK:- IBOutlet Define
    @IBOutlet weak var SubMain: UIView!
    @IBOutlet weak var Stackview: UIStackView!
    @IBOutlet weak var Stack_Center: NSLayoutConstraint!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var UserView: UIView!
    @IBOutlet weak var UserLBL: UILabel!
    @IBOutlet weak var TXTUser: UITextField!
    @IBOutlet weak var UserBTN: UIButton!
    @IBOutlet weak var UserLine: UILabel!
    
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var EmailLBL: UILabel!
    @IBOutlet weak var TXTEmail: UITextField!
    @IBOutlet weak var EmailBTN: UIButton!
    @IBOutlet weak var EmailLine: UILabel!
    
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var PasswordLBL: UILabel!
    @IBOutlet weak var TXTPassword: UITextField!
    @IBOutlet weak var PasswordBTN: UIButton!
    @IBOutlet weak var ShowPasswordBTN: UIButton!
    @IBOutlet weak var PasswordLine: UILabel!
    
    @IBOutlet weak var ConPasswordView: UIView!
    @IBOutlet weak var ConPasswordLBL: UILabel!
    @IBOutlet weak var TXTConPassword: UITextField!
    @IBOutlet weak var ConPasswordBTN: UIButton!
    @IBOutlet weak var ShowConPasswordBTN: UIButton!
    @IBOutlet weak var ConPasswordLine: UILabel!
    
    @IBOutlet weak var PassValidationBTN: UIButton!
    
    @IBOutlet weak var PassValidationStack: UIStackView!
    @IBOutlet var PassValidationLBL: [UILabel]!
    @IBOutlet var DotValidationLBL: [UILabel]!
    
    @IBOutlet weak var LoginBTN: UIButton!
    
    
    // MARK:- Variable define
    
    let char = "Should Be 8 Amount Of Characters"
    let upper = "Contains Atleast One Capital Letter"
    let num = "Contains Atleast One Number"
    let special = "Contains Atleast One Special Character"
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.view.theme_backgroundColor = GlobalPicker.backgroundColor
        
        self.setupView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- User Define Methods
    
    func setupView() {
        
        DispatchQueue.main.async {
            self.view.theme_backgroundColor = GlobalPicker.backgroundColor
            
            self.Stack_Center.constant = 0
            self.TitleLBL.text = "REGISTER"
            self.TitleLBL.font = Font().MontserratBoldFont(font: 35)
            self.TitleLBL.theme_textColor = GlobalPicker.textColor
            
            self.UserView.isHidden = false
            self.EmailView.isHidden = true
            self.PasswordView.isHidden = true
            self.ConPasswordView.isHidden = true
            self.PassValidationBTN.isHidden = true
            self.PassValidationStack.isHidden = true
            
            self.UserBTN.alpha = 0.5
            self.UserBTN.isUserInteractionEnabled = false
            
            self.UserLBL.text = "Choose A Bets Username"
            self.UserLBL.font = Font().MontserratBoldFont(font: 16)
            self.UserLBL.textColor = AppDarkColor
            self.TXTUser.placeholder = "Choose A Bets Username"
            self.TXTUser.font = Font().MontserratBoldFont(font: 13)
            self.TXTUser.text = "@"
            self.TXTUser.textColor = AppDarkColor
            self.TXTUser.delegate = self
            self.UserBTN.tintColor = AppDarkColor
            self.UserLine.backgroundColor = AppDarkColor
            self.TXTUser.attributedPlaceholder =
                NSAttributedString(string: "Choose A Bets Username", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratBoldFont(font: 13)])
            
            self.EmailLBL.text = "What’s Your Email?"
            self.EmailLBL.font = Font().MontserratBoldFont(font: 16)
            self.EmailLBL.textColor = AppDarkColor
            self.TXTEmail.placeholder = "Enter Email"
            self.TXTEmail.font = Font().MontserratBoldFont(font: 13)
            self.TXTEmail.textColor = AppDarkColor
            self.TXTEmail.delegate = self
            self.EmailBTN.tintColor = AppDarkColor
            self.EmailLine.backgroundColor = AppDarkColor
            self.TXTEmail.attributedPlaceholder =
                NSAttributedString(string: "Enter Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratBoldFont(font: 13)])
            
            self.PasswordLBL.text = "Set Password"
            self.PasswordLBL.font = Font().MontserratBoldFont(font: 16)
            self.PasswordLBL.textColor = AppDarkColor
            self.TXTPassword.placeholder = "Enter Password"
            self.TXTPassword.font = Font().MontserratBoldFont(font: 13)
            self.TXTPassword.textColor = AppDarkColor
            self.TXTPassword.isSecureTextEntry = true
            self.TXTPassword.delegate = self
            self.PasswordBTN.tintColor = AppDarkColor
            self.PasswordLine.backgroundColor = AppDarkColor
            self.ShowPasswordBTN.alpha = 0.5
            self.ShowPasswordBTN.isUserInteractionEnabled = false
            self.TXTPassword.attributedPlaceholder =
                NSAttributedString(string: "Enter Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratBoldFont(font: 13)])
            
            self.ConPasswordLBL.text = "Confirm Password"
            self.ConPasswordLBL.font = Font().MontserratBoldFont(font: 16)
            self.ConPasswordLBL.textColor = AppDarkColor
            self.TXTConPassword.placeholder = "Enter Confirm Password"
            self.TXTConPassword.font = Font().MontserratBoldFont(font: 13)
            self.TXTConPassword.textColor = AppDarkColor
            self.TXTConPassword.isSecureTextEntry = true
            self.TXTConPassword.delegate = self
            self.ConPasswordBTN.tintColor = AppDarkColor
            self.ConPasswordLine.backgroundColor = AppDarkColor
            self.ShowConPasswordBTN.alpha = 0.5
            self.ShowConPasswordBTN.isUserInteractionEnabled = false
            self.TXTConPassword.attributedPlaceholder =
                NSAttributedString(string: "Enter Confirm Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratBoldFont(font: 13)])
            
            self.PassValidationBTN.setTitle("View Password Security Requirements", for: .normal)
            self.PassValidationBTN.titleLabel?.font = Font().MontserratBoldFont(font: 11)
            self.PassValidationBTN.theme_setTitleColor(GlobalPicker.textColor, forState: .normal)
            self.PassValidationBTN.setTitleColor(AppDarkColor, for: .normal)
            
            self.PassValidation(str: "")
            
            self.LoginBTN.setTitle("Login", for: .normal)
            self.LoginBTN.titleLabel?.font = Font().MontserratBoldFont(font: 11)
            self.LoginBTN.theme_setTitleColor(GlobalPicker.textColor, forState: .normal)
            self.LoginBTN.setTitleColor(AppDarkColor, for: .normal)
            
            self.TXTUser.becomeFirstResponder()
        }
    }
    
    func PassValidation(str: String) {
        for (index, lbl) in self.PassValidationLBL.enumerated() {
            let dot = self.DotValidationLBL.filter { (label) -> Bool in
                label.tag == index
            }.first
            dot!.clipsToBounds = true
            dot!.layer.cornerRadius = dot!.frame.height / 2
            
            lbl.font = Font().MontserratRegularFont(font: 10)
            
            if index == 0 {
                lbl.text = self.char
                lbl.font = UIFont.systemFont(ofSize: 15.0)
                lbl.textColor = str.count >= 8 ? AppDarkColor : UIColor.lightGray
                
                dot!.backgroundColor = str.count >= 8 ? AppDarkColor : UIColor.lightGray
            }
            else if index == 1 {
                lbl.text = self.upper
                lbl.font = UIFont.systemFont(ofSize: 15.0)
                lbl.textColor = self.IsContainUppercase(textvalue: str) ? AppDarkColor : UIColor.lightGray
                
                dot!.backgroundColor = self.IsContainUppercase(textvalue: str) ? AppDarkColor : UIColor.lightGray
            }
            else if index == 2 {
                lbl.text = self.num
                lbl.font = UIFont.systemFont(ofSize: 15.0)
                lbl.textColor = self.IsContainDigit(textvalue: str) ? AppDarkColor : UIColor.lightGray
                
                dot!.backgroundColor = self.IsContainDigit(textvalue: str) ? AppDarkColor : UIColor.lightGray
            }
            else if index == 3 {
                lbl.text = self.special
                lbl.font = UIFont.systemFont(ofSize: 15.0)
                lbl.textColor = self.IsContainSpecialCharacter(textvalue: str)  ? AppDarkColor : UIColor.lightGray
                
                dot!.backgroundColor = self.IsContainSpecialCharacter(textvalue: str)  ? AppDarkColor : UIColor.lightGray
            }
        }
        
        if str.count >= 8 && self.IsContainUppercase(textvalue: str as String) && self.IsContainDigit(textvalue: str as String) && self.IsContainSpecialCharacter(textvalue: str as String) {
            self.PasswordBTN.alpha = 1.0
            self.PasswordBTN.isUserInteractionEnabled = true
            
            self.ShowPasswordBTN.alpha = 1.0
            self.ShowPasswordBTN.isUserInteractionEnabled = true
        }
        else {
            self.PasswordBTN.alpha = 0.5
            self.PasswordBTN.isUserInteractionEnabled = false
            
            self.ShowPasswordBTN.alpha = 0.5
            self.ShowPasswordBTN.isUserInteractionEnabled = false
        }
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedUserBTN(_ sender: UIButton) {
        self.UserView.isHidden = true
        self.EmailView.isHidden = false
        self.PasswordView.isHidden = true
        self.ConPasswordView.isHidden = true
        self.PassValidationBTN.isHidden = true
        self.PassValidationStack.isHidden = true
        
        self.Stack_Center.constant = 0
        self.EmailBTN.alpha = 0.5
        self.EmailBTN.isUserInteractionEnabled = false
        self.TXTEmail.becomeFirstResponder()
    }
    
    @IBAction func TappedEmailBTN(_ sender: UIButton) {
        self.UserView.isHidden = true
        self.EmailView.isHidden = true
        self.PasswordView.isHidden = false
        self.ConPasswordView.isHidden = true
        self.PassValidationBTN.isHidden = false
        self.PassValidationStack.isHidden = true
        
        self.PassValidation(str: self.TXTPassword.text!)
        self.Stack_Center.constant = 0
        self.PasswordBTN.alpha = 0.5
        self.PasswordBTN.isUserInteractionEnabled = false
        self.TXTPassword.becomeFirstResponder()
    }
    
    @IBAction func TappedPasswordBTN(_ sender: UIButton) {
        self.UserView.isHidden = true
        self.EmailView.isHidden = true
        self.PasswordView.isHidden = true
        self.ConPasswordView.isHidden = false
        self.PassValidationBTN.isHidden = true
        self.PassValidationStack.isHidden = true
        
        self.Stack_Center.constant = 0
        self.ConPasswordBTN.alpha = 0.5
        self.ConPasswordBTN.isUserInteractionEnabled = false
        self.TXTConPassword.becomeFirstResponder()
    }
    
    @IBAction func TappedShowPasswordBTN(_ sender: UIButton) {
        if sender.isSelected {
            self.TXTPassword.isSecureTextEntry = true
            self.ShowPasswordBTN.alpha = 0.5
            sender.isSelected = false
        }
        else {
            self.TXTPassword.isSecureTextEntry = false
            self.ShowPasswordBTN.alpha = 1.0
            sender.isSelected = true
        }
    }
    
    @IBAction func TappedPassValidation(_ sender: UIButton) {
        self.PassValidationBTN.isHidden = true
        self.PassValidationStack.isHidden = false
        
        self.Stack_Center.constant = -50
        self.PassValidation(str: self.TXTPassword.text!)
    }
    
    @IBAction func TappedShowConPasswordBTN(_ sender: UIButton) {
        if sender.isSelected {
            self.TXTConPassword.isSecureTextEntry = true
            self.ShowConPasswordBTN.alpha = 0.5
            sender.isSelected = false
        }
        else {
            self.TXTConPassword.isSecureTextEntry = false
            self.ShowConPasswordBTN.alpha = 1.0
            sender.isSelected = true
        }
    }
    
    @IBAction func TappedConPasswordBTN(_ sender: UIButton) {
        let vc = LoginVerifyVC.init(nibName: "LoginVerifyVC", bundle: nil)
        vc.username = self.TXTUser.text!
        vc.Email = self.TXTEmail.text!
        vc.passowrd = self.TXTPassword.text!
        vc.ComeFrom = "SignUp"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func TappedLogin(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension RegistrationVC: UITextFieldDelegate {
    
    func IsContainCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainUppercase(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainDigit(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "0123456789")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainSpecialCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if textvalue.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
     
        // at least one uppercase,
        // at least one digit
        // at least one special character
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            self.TappedUserBTN(self.UserBTN)
            self.TXTEmail.becomeFirstResponder()
        }
        else if textField == self.TXTEmail {
            self.TappedEmailBTN(self.EmailBTN)
            self.TXTPassword.becomeFirstResponder()
        }
        else if textField == self.TXTPassword {
            self.TappedPasswordBTN(self.PasswordBTN)
            self.TXTConPassword.becomeFirstResponder()
        }
        else {
            self.TappedConPasswordBTN(self.ConPasswordBTN)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            
        }
        else if textField == self.TXTEmail {
            if textField.isValidEmail(testStr: self.TXTEmail.text) {
                self.EmailBTN.isUserInteractionEnabled = true
                self.EmailBTN.alpha = 1.0
            }
            else {
                self.EmailBTN.isUserInteractionEnabled = false
                self.EmailBTN.alpha = 0.5
            }
        }
        else if textField == self.TXTPassword {
            self.PassValidation(str: self.TXTPassword.text!)
        }
        else {
            if self.TXTPassword.text?.uppercased() == self.TXTConPassword.text?.uppercased() {
                self.ConPasswordBTN.alpha = 1.0
                self.ConPasswordBTN.isUserInteractionEnabled = true
            }
            else {
                self.ConPasswordBTN.alpha = 0.5
                self.ConPasswordBTN.isUserInteractionEnabled = false
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.TXTUser {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            var username: String = ""
            if newString.hasPrefix("@") {
                username = String.init(format: "%@", newString)
            }
            else {
                username = String.init(format: "%@%@", "@", newString)
            }
            if username.count > 1 {
                self.UserBTN.isUserInteractionEnabled = true
                self.UserBTN.alpha = 1.0
                return true
            }
            self.TXTUser.text = String.init(format: "%@", username)
            self.UserBTN.isUserInteractionEnabled = false
            self.UserBTN.alpha = 0.5
            return false
        }
        else if textField == self.TXTEmail {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            if textField.isValidEmail(testStr: newString as String) {
                self.EmailBTN.isUserInteractionEnabled = true
                self.EmailBTN.alpha = 1.0
            }
            else {
                self.EmailBTN.isUserInteractionEnabled = false
                self.EmailBTN.alpha = 0.5
            }
        }
        else if textField == self.TXTPassword {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            self.PassValidation(str: newString as String)
        }
        else {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            if self.TXTPassword.text?.uppercased() == newString.uppercased {
                self.ConPasswordBTN.alpha = 1.0
                self.ConPasswordBTN.isUserInteractionEnabled = true
                
                self.ShowConPasswordBTN.alpha = 1.0
                self.ShowConPasswordBTN.isUserInteractionEnabled = true
            }
            else {
                self.ConPasswordBTN.alpha = 0.5
                self.ConPasswordBTN.isUserInteractionEnabled = false
                
                self.ShowConPasswordBTN.alpha = 0.5
                self.ShowConPasswordBTN.isUserInteractionEnabled = false
            }
        }
        return true
    }
    
}
