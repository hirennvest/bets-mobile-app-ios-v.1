//
//  BetsRegistrationVC.swift
//  CryptoGame
//
//  Created by Hiren on 24/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import JWT
import CRNotifications

class BetsRegistrationVC: BaseVC {

    // MARK:- IBOutlet Define
    @IBOutlet weak var UserMain: UIView!
    @IBOutlet weak var Stackview: UIStackView!
    @IBOutlet weak var Stack_Center: NSLayoutConstraint!
    
    @IBOutlet weak var LogoView: UIView!
    @IBOutlet var LogoIMG: [UIImageView]!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var UserView: UIView!
    @IBOutlet weak var TXTUser: UITextField!
    @IBOutlet weak var UserLine: UILabel!
    @IBOutlet weak var UserLBL: UILabel!
    
    @IBOutlet var BottomView: [UIView]!
    @IBOutlet weak var UserBTN: UIButton!
    
    @IBOutlet weak var ProfileMain: UIView!
    
    @IBOutlet weak var ProTitleLBL: UILabel!
    
    @IBOutlet weak var ProfilePicBTN: UIButton!
    @IBOutlet weak var ReUploadBTN: UIButton!
    
    @IBOutlet weak var CompleteBTN: UIButton!
    
    // MARK:- Variable define
    
    var ProfilePicName: String = ""
    var Username: String = ""
    var imageURL: String = ""
    var Dev_Email: String = "shorupan@gmail.com"
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.view.theme_backgroundColor = GlobalPicker.backgroundColor
        
        for view in self.BottomView {
            view.backgroundColor = AppDarkColor
        }
        
        for img in self.LogoIMG {
            img.tintColor = AppDarkColor
            img.image = UIImage.init(named: "BetsText")
        }
        
        self.setupUserView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- User Define Methods
    
    func setupUserView() {
        self.view.theme_backgroundColor = GlobalPicker.backgroundColor
        
        self.UserMain.isHidden = false
        self.ProfileMain.isHidden = true
        
        self.Stack_Center.constant = 0
        self.TitleLBL.text = "Just One Sec. You Need To Compete Your Bets Profile Setup. Please Select A Unique Bets Username"
        self.TitleLBL.font = Font().MontserratRegularFont(font: 17)
        self.TitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.TXTUser.placeholder = "@Username"
        self.TXTUser.font = Font().MontserratBoldFont(font: 13)
        self.TXTUser.text = "@"
        self.TXTUser.textColor = AppDarkColor
        self.TXTUser.delegate = self
        self.UserBTN.tintColor = AppDarkColor
        self.UserLine.backgroundColor = AppDarkColor
        self.TXTUser.attributedPlaceholder =
            NSAttributedString(string: "@Username", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratBoldFont(font: 13)])
        
        self.UserLBL.text = "Congrats, @________ Is Available"
        self.UserLBL.font = Font().MontserratBoldFont(font: 16)
        self.UserLBL.textColor = AppDarkColor
        
        self.BottomView[0].isHidden = true
        self.UserBTN.setTitle("Confirm Username", for: .normal)
        self.UserBTN.setTitleColor(.white, for: .normal)
    }
    
    func SetupProfileView(username: String) {
        self.UserMain.isHidden = true
        self.ProfileMain.isHidden = false
        
        self.ProTitleLBL.text = "Great Job \(username). Just Upload A Profile Pic & You Are Done!"
        self.ProTitleLBL.font = Font().MontserratRegularFont(font: 17)
        self.ProTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.ProfilePicBTN.clipsToBounds = true
        self.ProfilePicBTN.layer.borderColor = AppDarkColor.cgColor
        self.ProfilePicBTN.layer.borderWidth = 1.0
        self.ProfilePicBTN.layer.cornerRadius = self.ProfilePicBTN.frame.height / 2
        
        self.BottomView[1].isHidden = true
        self.ReUploadBTN.isHidden = true
        
        self.ReUploadBTN.setTitle("Upload Another Pic", for: .normal)
        self.ReUploadBTN.setTitleColor(AppDarkColor, for: .normal)
        self.ReUploadBTN.clipsToBounds = true
        self.ReUploadBTN.layer.borderColor = AppDarkColor.cgColor
        self.ReUploadBTN.layer.borderWidth = 1.0
        
        self.CompleteBTN.setTitle("Complete Setup", for: .normal)
        self.CompleteBTN.setTitleColor(.white, for: .normal)
    }
    
    // MARK:- API Calling
    
    func VerifyUsername(uname: String) {
        background {
            let paramDict = GXUsernameCheckDict.init(email: HeaderSetter().email.removeWhiteSpace(), username: uname)
            
            NetworkingRequests.shared.requestPOST(API_GXRegister, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    let payload = responseObject["payload"] as! String
                    if statuscode {
                        if payload.uppercased() == "Username Available".uppercased() {
                            self.BottomView[0].isHidden = false
                            self.Username = uname
                            self.UserLBL.text = "Congrats, " + self.TXTUser.text! + " Is Available"
                        }
                        else {
                            self.BottomView[0].isHidden = true
                            self.UserLBL.text = payload
                        }
                    }
                    else {
                        self.BottomView[0].isHidden = true
                        self.UserLBL.text = payload
                    }
                }
            }) { (msg, code) in
                main {
                    
                }
            }
        }
    }
    
    func GenerateJWT_taken() {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: 2, to: Date().localDate())
        var claims = ClaimSet()
        claims.issuer = "gxjwtenchs512"
        claims.expiration = date
        claims["email"] = self.Dev_Email
        claims["name"] = self.ProfilePicName.replaceLocalized(fromvalue: ["@", "_"], tovalue: ["", ""])
        let JWT_Token = JWT.encode(claims: claims, algorithm: .hs512("uyrw7826^&(896GYUFWE&*#GBjkbuaf".data(using: .utf8)!))
        
        let path_inside_brain = "root/Testing"
        let url = String.init(format: "%@email=%@&path=%@&token=%@&name=%@", API_UploadBaseURL, self.Dev_Email, path_inside_brain, JWT_Token, self.ProfilePicName.replaceLocalized(fromvalue: ["@", "_"], tovalue: ["", ""]))
        
        self.ProfilePicBTN.alpha = 0.3
        self.view.isUserInteractionEnabled = false
        let activity = UIActivityIndicatorView.init()
        activity.center = self.ProfilePicBTN.center
        activity.color = .red
        activity.hidesWhenStopped = true
        activity.startAnimating()
        self.ProfilePicBTN.addSubview(activity)
        NetworkingRequests.shared.uploadImage(url, param: ["": ""], image: (self.ProfilePicBTN.imageView?.image)!, filename: "files", imageName: self.ProfilePicName) { (response, isSuccess, message) in
            self.ProfilePicBTN.alpha = 1
            self.view.isUserInteractionEnabled = true
            activity.stopAnimating()
            if isSuccess {
                  let responseDic  = response as!Dictionary<String,Any>
                if responseDic["status"] as! Bool{
                    let localDic = responseDic["payload"] as! Dictionary<String,Any>
                    if localDic.keys.contains("upload_success") {
                        let status = localDic["upload_success"] as! Bool
                        if status {
                            if localDic.keys.contains("url") {
                                let profileImageURLStr = localDic["url"] as! String
                                self.BottomView[1].isHidden = false
                                self.imageURL = profileImageURLStr
                            }
                            else {
                                CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: localDic["message"] as! String, dismissDelay: 3, completion: {
                                    
                                })
                            }
                        }
                    }
                } else {
                    let localDic = responseDic["payload"] as! Dictionary<String,Any>
                    self.imageURL = ""
                    self.BottomView[1].isHidden = true
                    self.ProfilePicBTN.setImage(UIImage.init(named: "Profiledummy"), for: .normal)
                    self.ProfilePicName = ""
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: localDic["message"] as! String, dismissDelay: 3, completion: {
                        
                    })
                }
            }
            else {
                self.imageURL = ""
                self.BottomView[1].isHidden = true
                self.ProfilePicBTN.setImage(UIImage.init(named: "Profiledummy"), for: .normal)
                self.ProfilePicName = ""
                CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message!, dismissDelay: 3, completion: {
                    
                })
            }
        }
    }
    
    func RegisterBets() {
        background {
            let header = CommanHeader.init(email: HeaderSetter().email.removeWhiteSpace(), token: UserInfoData.shared.GetUserToken()!)
            let paramDict = RegisterBetsParamDict.init(email: HeaderSetter().email.removeWhiteSpace())
            
            NetworkingRequests.shared.requestPOST(API_UserBIO, Parameters: paramDict.description, Headers: header.description, onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserBioRootClass.init(fromDictionary: responseObject as NSDictionary)
                        UserInfoData.shared.SaveObjectdata(data: userinfo.payload, forkey: BetsRegister)
                        if userinfo.payload.betsLevel {
                            App?.SetDashboardVC(index: 0)
                            ApiLoader.shared.stopHUD()
                        }
                        else {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                    else {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    ApiLoader.shared.stopHUD()
                }
            }) { (msg, code) in
                main {
                    self.navigationController?.popToRootViewController(animated: true)
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedUserBTN(_ sender: UIButton) {
        self.SetupProfileView(username: self.Username)
    }
    
    @IBAction func TappedProfileBTN(_ sender: UIButton) {
        if sender == self.ProfilePicBTN || sender == self.ReUploadBTN {
            AttachmentHandler.shared.ShowSelectedAttachOption(vc: self, constant: [.photoLibrary, .camera])
            AttachmentHandler.shared.imagePickedBlock = { (selectedIMG) in
                self.ProfilePicBTN.setImage(selectedIMG, for: .normal)
                self.ProfilePicName = String.init(format: "%@_%0.0f", (UserInfoData.shared.GetUserdata()?.username)!, NSDate().timeIntervalSince1970 as CVarArg)
                self.ReUploadBTN.isHidden = false
                self.GenerateJWT_taken()
            }
        }
        else {
            if self.imageURL.count != 0 {
                background {
                    ApiLoader.shared.showHUD()
                    let paramDict = GXUsernameDict.init(email: HeaderSetter().email.removeWhiteSpace(), username: self.Username, image: self.imageURL)
                    
                    NetworkingRequests.shared.requestPOST(API_GXRegister, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                        main {
                            if statuscode {
                                if responseObject.keys.contains("payload") {
                                    let message = responseObject["payload"] as! String
                                    if message.uppercased() == "User Registration Success".uppercased() {
                                        self.RegisterBets()
                                    }
                                }
                            }
                            else {
                                ApiLoader.shared.stopHUD()
                            }
                        }
                    }) { (msg, code) in
                        main {
                            ApiLoader.shared.stopHUD()
                        }
                    }
                }
            }
        }
    }

}

extension BetsRegistrationVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            self.TappedUserBTN(self.UserBTN)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.TXTUser {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            var username: String = ""
            if newString.hasPrefix("@") {
                username = String.init(format: "%@", newString)
            }
            else {
                username = String.init(format: "%@%@", "@", newString)
            }
            if username.count > 1 {
                self.VerifyUsername(uname: username)
                return true
            }
            self.TXTUser.text = String.init(format: "%@", username)
            self.UserLBL.text = "Congrats, @_______ Is Available"
            self.BottomView[0].isHidden = true
            return false
        }
        return true
    }
    
}
