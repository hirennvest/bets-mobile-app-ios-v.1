//
//  OTPVerifyVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications

class OTPVerifyVC: BaseVC {
    
    // MARK:- IBOutlet
    @IBOutlet weak var Mainstackview: UIStackView!
    
    @IBOutlet weak var PinHeaderView: UIView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var Title_Height: NSLayoutConstraint!
    
    @IBOutlet weak var PinView: UIStackView!
    @IBOutlet var PinValueView: [UIView]!
    @IBOutlet var PinValueLBL: [UILabel]!
    @IBOutlet var PinLineLBL: [UILabel]!
    
    @IBOutlet weak var ResendView: UIView!
    @IBOutlet weak var ResendBTN: UIButton!
    @IBOutlet weak var ResendLBL: UILabel!
    
    @IBOutlet weak var PinDigitView: UIView!
    @IBOutlet var DigitsBTN: [UIButton]!
    
    // MARK:- Variable Define
    
    let CodeCount: Int = 6
    var PinArray: NSMutableArray!
    var FinalOTP: String!
    var emailAddress: String!
    
    var counter = 60
    var timer: Timer!
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- User Define Methods
    
    func setupUI() {
        
        self.PinArray = NSMutableArray.init(capacity: self.CodeCount)
        
        let string1: String = "VERIFICATION"
        var string2: String = "\n\n Enter The Code That Was Just Sent To EMAILADDRESS " as String
        let finalSTR2: String = string2.replaceLocalized(fromvalue: ["EMAILADDRESS"], tovalue: [self.emailAddress])
        let mainstring: String = string1 + finalSTR2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratBoldFont(font: 30), AppDarkColor), (finalSTR2, Font().MontserratBoldFont(font: 15), AppDarkColor)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, finalSTR2])))
        self.TitleLBL.attributedText = myMutableString
        
        let height1 = string1.heightWithConstrainedWidth(width: Screen_width - 60, font: Font().MontserratBoldFont(font: 30.0))
        let height2 = finalSTR2.heightWithConstrainedWidth(width: Screen_width - 60, font: Font().MontserratBoldFont(font: 15.0))
        self.Title_Height.constant = height1 + height2
        
        self.ResendLBL.text = "In 60 Seconds"
        self.ResendLBL.font = Font().MontserratBoldFont(font: 12)
        self.ResendLBL.textColor = UIColor.colorWithHexString(hexStr: "#6D778B")
        self.ResendBTN.setTitleColor(UIColor.colorWithHexString(hexStr: "#6D778B"), for: .normal)
        self.ResendBTN.setTitle("Resend Code", for: .normal)
        self.ResendBTN.titleLabel!.font = Font().MontserratRegularFont(font: 12)
        self.ResendBTN.isUserInteractionEnabled = false
        
        for n in 0...self.PinValueLBL.count - 1 {
            let lbl = self.PinValueLBL[n]
            lbl.textColor = AppDarkColor
            lbl.textAlignment = .center
            lbl.text = ""
        }
        
        for n in 0...self.PinLineLBL.count - 1 {
            let lbl = self.PinLineLBL[n]
            lbl.backgroundColor = AppDarkColor
        }
        
        for item in self.DigitsBTN {
            if item.tag == 100 {
                let inset = (item.frame.height * 40) / 100
                item.imageEdgeInsets = UIEdgeInsets.init(top: inset, left: inset, bottom: inset, right: inset)
            }
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
        //example functionality
        if counter > 0 {
            counter -= 1
            self.ResendLBL.text = String.init(format: "In %02i Seconds", counter)
            self.ResendBTN.setTitleColor(UIColor.colorWithHexString(hexStr: "#6D778B"), for: .normal)
            self.ResendBTN.isUserInteractionEnabled = false
        }
        else {
            self.timer.invalidate()
            self.timer = nil
            self.ResendLBL.text = ""
            self.ResendBTN.setTitleColor(AppDarkColor, for: .normal)
            self.ResendBTN.isUserInteractionEnabled = true
        }
    }
    
    func EnterPin(digit: String) {
        if self.PinArray.count <= self.CodeCount - 2 {
            self.PinArray.add(digit)
        }
        else {
            if self.PinArray.count <= self.CodeCount - 1 {
                self.PinArray.add(digit)
            }
        }
        self.reloadPin()
    }
    
    func reloadPin() {
        for (index, item) in self.PinArray.enumerated() {
            if index <= self.CodeCount - 2 {
                self.PinValueLBL[index].text = item as? String
            }
            if index == self.CodeCount - 1 {
                self.PinValueLBL[index].text = item as? String
                self.FinalOTP = self.PinArray.componentsJoined(by: "")
                print("Entered Pin is: \(String(describing: self.FinalOTP))")
                self.APIVerifyOTP()
            }
        }
    }
    
    func backspace() {
        for index in 0...self.CodeCount - 1 {
            if index < self.PinArray.count {
                self.PinValueLBL[index].text = self.PinArray.object(at: index) as? String
            }
            else {
                self.PinValueLBL[index].text = ""
                self.PinValueLBL[index].text = ""
            }
        }
    }
    
    // MARK:- Api Calling
    
    func APIVerifyOTP() {
        background {
            ApiLoader.shared.showHUD()
            
            let paramDict = ConfirmParamDict.init(email: self.emailAddress.removeWhiteSpace(), code: self.FinalOTP)

            NetworkingRequests.shared.requestPOST(API_Confirm, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    else {
                        let message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message as! String, dismissDelay: 3, completion: {
                            
                        })
                    }
                    ApiLoader.shared.stopHUD()
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        
                    })
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedResendBTN(_ sender: Any) {
        self.counter = 60
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @IBAction func TappedDigitsBTN(_ sender: UIButton) {
        sender.titleLabel?.font = Font().MontserratRegularFont(font: 20)
        switch sender.tag {
        case 1:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 2:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 3:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 4:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 5:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 6:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 7:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 8:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 9:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 100:
            self.PinArray.removeLastObject()
            self.backspace()
            break
        case 0:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 200:
            break
        default:
            break
        }
    }
    
}
