//
//  VideoStreamingVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-03.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayerView: UIView {

    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }

}


class VideoStreamingVC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var PlayerView: PlayerView!
    
    @IBOutlet weak var CloseBTN: UIButton!
    
    @IBOutlet weak var Viewers: CustomView!
    @IBOutlet weak var ViewersLBL: UILabel!
    @IBOutlet weak var ViewersIMG: UIImageView!
    
    @IBOutlet weak var TrailingStack: UIStackView!
    
    @IBOutlet weak var InviteBTN: CustomBTN!
    @IBOutlet weak var InviteLBL: UILabel!
    
    @IBOutlet weak var HeartBTN: CustomBTN!
    @IBOutlet weak var HeartLBL: UILabel!
    
    @IBOutlet weak var ChatBTN: CustomBTN!
    @IBOutlet weak var ChatLBL: UILabel!
    
    @IBOutlet weak var ShareBTN: CustomBTN!
    @IBOutlet weak var ShareLBL: UILabel!
    
    @IBOutlet weak var Followers: CustomView!
    @IBOutlet weak var FollowersLBL: UILabel!
    @IBOutlet weak var FollowersIMG: UIImageView!
    
    @IBOutlet weak var Detailsview: UIView!
    @IBOutlet weak var TagLBL: UILabel!
    @IBOutlet weak var NameLBL: UILabel!
    
    @IBOutlet weak var LeadingStack: UIStackView!
    
    @IBOutlet weak var VolumeBTN: CustomBTN!
    @IBOutlet weak var PIPBTN: CustomBTN!
    @IBOutlet weak var ZoomBTN: CustomBTN!
    
//    MARK:- Variable Defines
    weak var delegate: VideoStreamingVCDelegate?
    var player: AVPlayer?
    private var pictureInPictureController: AVPictureInPictureController!
    private var pictureInPictureObservations = [NSKeyValueObservation]()
    private var strongSelf: Any?
    var dismissControls: Bool = false
    
    deinit {
        // without this line vanilla AVPictureInPictureController will crash due to KVO issue
        pictureInPictureObservations = []
    }
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ViewersLBL.text = "123k"
        self.ViewersLBL.font = Font().MontserratRegularFont(font: 13)
        self.ViewersLBL.textColor = UIColor.white
        
        self.InviteLBL.text = "Invite"
        self.InviteLBL.font = Font().MontserratRegularFont(font: 13)
        self.InviteLBL.textColor = UIColor.white
        
        self.HeartLBL.text = "567"
        self.HeartLBL.font = Font().MontserratRegularFont(font: 13)
        self.HeartLBL.textColor = UIColor.white
        
        self.ChatLBL.text = "123"
        self.ChatLBL.font = Font().MontserratRegularFont(font: 13)
        self.ChatLBL.textColor = UIColor.white
        
        self.ShareLBL.text = "Share"
        self.ShareLBL.font = Font().MontserratRegularFont(font: 13)
        self.ShareLBL.textColor = UIColor.white
        
        self.FollowersLBL.text = "Followers"
        self.FollowersLBL.font = Font().MontserratRegularFont(font: 13)
        self.FollowersLBL.textColor = UIColor.white
        
        self.TagLBL.text = "@Millian"
        self.TagLBL.font = Font().MontserratRegularFont(font: 13)
        self.TagLBL.textColor = UIColor.white
        
        self.NameLBL.text = "Roack & Roll - Zone 7"
        self.NameLBL.font = Font().MontserratRegularFont(font: 15)
        self.NameLBL.textColor = UIColor.white
        
//        self.ZoomBTN.isHidden = true
//        self.VolumeBTN.isHidden = true
        self.SetupPIPview()
        
        let dismissViewTap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        self.PlayerView.addGestureRecognizer(dismissViewTap)
        
    }
    
    // MARK:- User Define Methods
    
    @objc private func dismissView() {
        if self.dismissControls {
           self.dismissControls = false
        }
        else {
            self.dismissControls = true
        }
        self.LeadingStack.isHidden = self.dismissControls
        self.TrailingStack.isHidden = self.dismissControls
        self.LeadingStack.isHidden = true
        self.Viewers.isHidden = self.dismissControls
        self.Followers.isHidden = self.dismissControls
        self.Detailsview.isHidden = self.dismissControls
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        player?.play()
    }
    
    // MARK:- API Calling methods
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedCloseBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func TappedInviteBTN(_ sender: Any) {
        
    }
    @IBAction func TappedHeartBTN(_ sender: Any) {
        
    }
    @IBAction func TappedChatBTN(_ sender: Any) {
        
    }
    @IBAction func TappedShareBTN(_ sender: Any) {
        
    }
    @IBAction func TappedVolumeBTN(_ sender: Any) {
        if self.VolumeBTN.isSelected {
            self.VolumeBTN.isSelected = false
            player?.volume = 100
        }
        else {
            self.VolumeBTN.isSelected = true
            player?.volume = 0
        }
    }
    @IBAction func TappedPIPBTN(_ sender: Any) {
        if self.PIPBTN.isSelected {
            pictureInPictureController.stopPictureInPicture()
        } else {
            pictureInPictureController.startPictureInPicture()
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func TappedZoomBTN(_ sender: Any) {
        if self.ZoomBTN.isSelected {
            self.ZoomBTN.isSelected = false
            self.PlayerView.playerLayer.videoGravity = .resizeAspect
        }
        else {
            self.ZoomBTN.isSelected = true
            self.PlayerView.playerLayer.videoGravity = .resizeAspectFill
        }
    }
    
}

protocol VideoStreamingVCDelegate: class {
    
    func customPlayerViewController(_ customPlayerViewController: VideoStreamingVC, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void)
    
}

//TODO:- Picture in Picture View Methods
extension VideoStreamingVC {
    
    func SetupPIPview() {
        self.PlayerView.playerLayer.player = player
        setupPictureInPicture()
    }
    
    // https://developer.apple.com/documentation/avkit/adopting_picture_in_picture_in_a_custom_player
    func setupPictureInPicture() {
        self.PIPBTN.setImage(AVPictureInPictureController.pictureInPictureButtonStartImage(compatibleWith: nil), for: .normal)
        self.PIPBTN.setImage(AVPictureInPictureController.pictureInPictureButtonStopImage(compatibleWith: nil), for: .selected)
        self.PIPBTN.setImage(AVPictureInPictureController.pictureInPictureButtonStopImage(compatibleWith: nil), for: [.selected, .highlighted])
        
        guard AVPictureInPictureController.isPictureInPictureSupported(),
            let pictureInPictureController = AVPictureInPictureController(playerLayer: self.PlayerView.playerLayer) else {
                self.PIPBTN.isEnabled = false
                return
        }
        
        self.pictureInPictureController = pictureInPictureController
        pictureInPictureController.delegate = self
        self.PIPBTN.isEnabled = pictureInPictureController.isPictureInPicturePossible
        
        pictureInPictureObservations.append(pictureInPictureController.observe(\.isPictureInPictureActive) { [weak self] pictureInPictureController, change in
            guard let `self` = self else { return }
            
            self.PIPBTN.isSelected = pictureInPictureController.isPictureInPictureActive
        })
        
        pictureInPictureObservations.append(pictureInPictureController.observe(\.isPictureInPicturePossible) { [weak self] pictureInPictureController, change in
            guard let `self` = self else { return }
            
            self.PIPBTN.isEnabled = pictureInPictureController.isPictureInPicturePossible
        })
    }
    
}

// MARK: - AVPictureInPictureControllerDelegate
extension VideoStreamingVC: AVPictureInPictureControllerDelegate {
    
    func pictureInPictureControllerWillStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        strongSelf = self
    }
    
    func pictureInPictureControllerDidStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        strongSelf = nil
    }
    
    func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        if let delegate = delegate {
            delegate.customPlayerViewController(self, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler: completionHandler)
        } else {
            completionHandler(true)
        }
    }
    
}
