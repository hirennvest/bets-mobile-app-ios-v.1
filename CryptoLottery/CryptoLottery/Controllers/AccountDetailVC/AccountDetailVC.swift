//
//  AccountDetailVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-23.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications


class AccountDetailVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var TopBG: UIView!
    
    @IBOutlet weak var TransactionsTBL: UITableView!
    @IBOutlet weak var DepositBTN: UIButton!
    @IBOutlet weak var FundBTN: UIButton!
    
    //    MARK:- Variable Define
    
    var OptionArry: [String] = ["Bets", "Deposits", "Withdrawals"]
    var OptionType: [String] = []
    var SelectedOption: Int = 0
    
    var VaultTransaction : [VaultTransaction_Payload]!
    var sectionArray = [TransactionSection]()
    lazy var FilterTransaction: [VaultTransaction_TransactionList]! = {
        () -> [VaultTransaction_TransactionList] in
        let array = [VaultTransaction_TransactionList]()
        return array
    }()
    var Carousel_Coins : [CoinListPayload]!
    var Balance_payload : BetsVaultPayload!
    var SelectedBottomCell: Int = 0
    
    var CMC_Prices: CMCPrices!
    
    lazy var nodatafound: NodatafoundView = {
        () -> NodatafoundView in
        let bounds = self.TransactionsTBL.frame
        let nodata = NodatafoundView.init(frame: bounds)
        nodata.SetupView(title: "No data available!", Notes: "There are no Data available yet!", image: "", enable: true)
        self.view.addSubview(nodata)
        return nodata
    }()
    
    lazy var Headrview: AccountHeaderView = {
        () -> AccountHeaderView in
        var headerView: AccountHeaderView
        headerView = AccountHeaderView(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 400))
        return headerView
    }()
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCMCPrices()
        self.TopBG.backgroundColor = AppDarkColor
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .ReloadAccount, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getVaultTransactions()
        
        self.TransactionsTBL.register(UINib.init(nibName: "TransactionsCell", bundle: nil), forCellReuseIdentifier: "TransactionsCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 100))
        footer.backgroundColor = .clear
        self.TransactionsTBL.tableFooterView = footer
        self.TransactionsTBL.tableHeaderView = self.Headrview
        
        self.DepositBTN.backgroundColor = AppDarkColor
        self.DepositBTN.clipsToBounds = true
        self.DepositBTN.layer.cornerRadius = 15
        self.DepositBTN.setTitle("Fund", for: .normal)
        self.DepositBTN.setTitleColor(.white, for: .normal)
        
        self.FundBTN.backgroundColor = AppDarkColor
        self.FundBTN.clipsToBounds = true
        self.FundBTN.layer.cornerRadius = 15
        self.FundBTN.setTitle("Widthdraw", for: .normal)
        self.FundBTN.setTitleColor(.white, for: .normal)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.SelectedOption = 0
        self.SelectedBottomCell = 0
    }
    
    //    MARK:- User Define Methods
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.viewDidLoad()
    }
    
    func Nodatafound(hideshow: Bool) {
        main {
            ApiLoader.shared.stopHUD()
            self.nodatafound.isHidden = hideshow
            self.nodatafound.didActionBlock = {
                self.getVaultTransactions()
            }
            self.TransactionsTBL.isHidden = !hideshow
            self.FundBTN.isHidden = !hideshow
            self.DepositBTN.isHidden = !hideshow
        }
    }
    
    func AccountHeader() {
        
        if self.SelectedBottomCell == 0 {
            self.Headrview.NameLBL.text = "All"
            
            self.Headrview.TotalLBL.text = String.init(format: "%f", self.AllTotalCounts().0!).CoinPriceThumbRules(Coin: "USD")
            self.Headrview.BalanceLBL.text = String.init(format: "%f ", self.AllTotalCounts().1!).CoinPriceThumbRules(Coin: "USD")
            
            self.Headrview.InvestAmount.text = String.init(format: "%f", self.AllTotalCounts().2!).CoinPriceThumbRules(Coin: "USD")
            self.Headrview.LiquidAmount.text = String.init(format: "%f", self.AllTotalCounts().3!).CoinPriceThumbRules(Coin: "USD")
            self.TransactionFilter(Index: 0)
        }
        else {
            let Vault = self.FilterCoinBalance(index: self.SelectedBottomCell - 1).0
            let coins = self.FilterCoinBalance(index: self.SelectedBottomCell - 1).1
            
            let coindata = self.Carousel_Coins[self.SelectedBottomCell - 1]
            self.Headrview.NameLBL.text = coindata.name
            
            if Vault != nil {
                self.Headrview.TotalLBL.text = String.init(format: "%f", Double((Vault?.first?.liveBalance.nativeField)!) as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
                self.Headrview.BalanceLBL.text = String.init(format: "%f", Double((Vault?.first?.liveBalance.usd)!) as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
            }
            if coins != nil {
                if coins?.first?.investments.liveInvestment == nil {
                    let usd: Double = 0.0
                    self.Headrview.InvestAmount.text = String.init(format: "%f", usd as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
                }
                else {
                    self.Headrview.InvestAmount.text = String.init(format: "%f", Double((coins?.first?.investments.liveInvestment.usd)!) as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
                }
                if coins?.first?.investments.totalInvestment == nil {
                    let usd: Double = 0.0
                    self.Headrview.LiquidAmount.text = String.init(format: "%f", usd as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
                }
                else {
                    self.Headrview.LiquidAmount.text = String.init(format: "%f", Double((coins?.first?.investments.totalInvestment.usd)!) as CVarArg).CoinPriceThumbRules(Coin: coindata.coin)
                }
            }
            self.TransactionFilter(Index: self.SelectedBottomCell - 1)
        }
        
        self.Headrview.OptionTitleLBL.text = self.OptionArry[self.SelectedOption]
        self.Headrview.NextactionHandler {
            self.SelectedOption += 1
            if self.SelectedOption == self.OptionArry.count {
                self.SelectedOption = 0
            }
            self.Headrview.OptionTitleLBL.text = self.OptionArry[self.SelectedOption]
            self.Headrview.shake()
            self.OptionsFilter()
        }
        
        self.Headrview.PreviousactionHandler {
            if self.SelectedOption == 0 {
                self.SelectedOption = self.OptionArry.count - 1
            }
            else {
                self.SelectedOption -= 1
            }
            self.Headrview.OptionTitleLBL.text = self.OptionArry[self.SelectedOption]
            self.Headrview.shake()
            self.OptionsFilter()
//            self.TransactionsTBL.beginUpdates()
//            let indexpath = IndexPath.init(row: 0, section: 0)
//            self.TransactionsTBL.reloadSections(IndexSet(integer: indexpath.section), with: .none)
//            self.TransactionsTBL.endUpdates()
        }
        
        self.Headrview.Carousel.type = .coverFlow2
        self.Headrview.Carousel.delegate = self
        self.Headrview.Carousel.dataSource = self
        self.Headrview.Carousel.reloadData()
        self.Headrview.Carousel.scrollToItem(at: self.SelectedBottomCell, animated: true)
        
        self.OptionsFilter()
    }
    
    func AllTotalCounts() -> (Double?, Double?, Double?, Double?) {
        if self.Balance_payload != nil {
            var TotallblCount: Double? = 0.0
            var BalanceLBLCount: Double? = 0.0
            TotallblCount = self.Balance_payload.vault.reduce(0, { (result, next) -> Double in
                if next.liveBalance != nil {
                    if next.liveBalance.nativeField != nil {
                        return result + next.liveBalance.nativeField
                    }
                }
                return result
            })
            BalanceLBLCount = self.Balance_payload.vault.reduce(0, { (result, next) -> Double in
                if next.liveBalance.usd != nil {
                    if next.liveBalance != nil {
                        return result + next.liveBalance.usd
                    }
                }
                return result
            })
            print(TotallblCount as Any , BalanceLBLCount as Any)
            
            var InvestAmountCount: Double? = 0.0
            var LiquidAmountCount: Double? = 0.0
            InvestAmountCount = self.Balance_payload.coins.reduce(0, { (result, next) -> Double in
                if next.investments.liveInvestment != nil {
                    if next.investments.liveInvestment.usd != nil {
                        return result + next.investments.liveInvestment.usd
                    }
                }
                return result
            })
            LiquidAmountCount = self.Balance_payload.coins.reduce(0, { (result, next) -> Double in
                if next.investments.totalInvestment != nil {
                    if next.investments.totalInvestment.usd != nil {
                        return result + next.investments.totalInvestment.usd
                    }
                }
                return result
            })
            print(InvestAmountCount as Any , LiquidAmountCount as Any)
            
            return (TotallblCount , BalanceLBLCount, InvestAmountCount , LiquidAmountCount)
        }
        return (0.00, 0.00, 0.00, 0.00)
    }
    
    func FilterCoinBalance(index: Int) -> ([BetsVaultVault]?, [BetsVaultCoin]?) {
        let coindata = self.Carousel_Coins[index]
        if self.Balance_payload != nil {
            let vault = self.Balance_payload.vault.filter { (vault) -> Bool in
                vault.coin == coindata.coin
            }
            let coins = self.Balance_payload.coins.filter { (coins) -> Bool in
                coins.coin == coindata.coin
            }
            return (vault, coins)
        }
        return (nil, nil)
    }
    
    func TransactionFilter(Index: Int = 0) {
        self.FilterTransaction.removeAll()
        if self.SelectedBottomCell == 0 {
            for item in self.VaultTransaction {
                self.FilterTransaction.append(contentsOf: item.transactionList)
            }
        }
        else {
            self.FilterTransaction.removeAll()
            let coindata = self.Carousel_Coins[self.SelectedBottomCell - 1]
            let data: [VaultTransaction_Payload] = self.VaultTransaction.filter { (transaction) -> Bool in
                transaction.coin == coindata.coin
            }
            if data.count != 0 {
                self.FilterTransaction.append(contentsOf: (data.first?.transactionList)!)
            }
        }
        self.TransactionsTBL.beginUpdates()
        let indexpath = IndexPath.init(row: 0, section: 0)
        self.TransactionsTBL.reloadSections(IndexSet(integer: indexpath.section), with: .none)
        self.TransactionsTBL.endUpdates()
    }
    
    func OptionsFilter() {
        self.FilterTransaction.removeAll()
        self.TransactionFilter()
        var type = self.OptionArry[self.SelectedOption]
        if self.SelectedOption == 0 {
            type = "Beats"
        }
        else if self.SelectedOption == 1 {
            let type2 = "buy"
            let type3 = "deposit"
            let filter = self.FilterTransaction.filter { (vault) -> Bool in
                vault.txnType.uppercased() == type.uppercased() || vault.txnType.uppercased() == type2.uppercased() || vault.txnType.uppercased() == type3.uppercased()
            }
            self.FilterTransaction.removeAll()
            self.FilterTransaction.append(contentsOf: filter)
        }
        else {
            type = "withdraw"
            let filter = self.FilterTransaction.filter { (vault) -> Bool in
                vault.txnType.uppercased() == type.uppercased()
            }
            self.FilterTransaction.removeAll()
            self.FilterTransaction.append(contentsOf: filter)
        }
        self.TransactionsTBL.reloadData()
    }
    
    func SubSectionArrange(data: VaultTransaction_Payload) -> [SectionArray] {
        let sections = TransactionSection.init(fromDictionary: [:])
        sections.sectionkey = data.coin
        
        // Buy Section create
        let buysub = SectionArray.init(fromDictionary: [:])
        buysub.sectionkey = "Deposits"
        sections.sectionArray.append(buysub)
        
        // Withdraw Section Create
        let withsub = SectionArray.init(fromDictionary: [:])
        withsub.sectionkey = "Withdrawals"
        sections.sectionArray.append(withsub)
        
        // Withdraw Section Create
        let betssub = SectionArray.init(fromDictionary: [:])
        betssub.sectionkey = "Bets"
        sections.sectionArray.append(betssub)
        
        for trans in data.transactionList {
            
            let status = self.OptionType.filter { (type) -> Bool in
                type.uppercased() == trans.txnType.uppercased()
            }.first
            if !(status != nil) {
                self.OptionType.append(trans.txnType)
            }
        
            switch trans.txnType.uppercased() {
            case "buy".uppercased(), "deposit".uppercased(), "Deposits".uppercased():
                buysub.transactionList.append(trans)
                break
                
            case "withdraw".uppercased(), "Withdrawals".uppercased():
                withsub.transactionList.append(trans)
                break
                
            default:
                betssub.transactionList.append(trans)
                break
            }
        }
        return sections.sectionArray
    }
    
    func CreateTransactionSection(transactions: [VaultTransaction_Payload]) {
        for items in transactions {
            let Key = items.coin
            if self.sectionArray.count == 0 {
                let sections = TransactionSection.init(fromDictionary: [:])
                sections.sectionkey = Key
                sections.sectionArray.append(contentsOf: self.SubSectionArrange(data: items))
                self.sectionArray.append(sections)
            }
            else {
                let available = self.sectionArray.filter { (sec_data) -> Bool in
                    sec_data.sectionkey.uppercased() == Key?.uppercased()
                }
                if available.count == 0 {
                    let sections = TransactionSection.init(fromDictionary: [:])
                    sections.sectionkey = Key
                    sections.sectionArray.append(contentsOf: self.SubSectionArrange(data: items))
                    self.sectionArray.append(sections)
                }
                else {
                    available.first?.sectionArray.append(contentsOf: self.SubSectionArrange(data: items))
                }
            }
        }
    }
    
    //    MARK:- Api Calling
    
    func getCMCPrices() {
        DispatchQueue.global(qos: .background).async {
            CommanApiCall().CMCprices { (CMC) in
                DispatchQueue.main.async {
                    self.CMC_Prices = CMC
                }
            }
        }
    }
    
    func getVaultTransactions() {
        background {
            let param = VaultTransactionParamDict.init(email: HeaderSetter().email)
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            ApiLoader.shared.showHUD()
            NetworkingRequests.shared.requestPOST(API_Vault_Transactions, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let betsvault: VaultTransaction_RootClass = VaultTransaction_RootClass.init(fromDictionary: responseObject)
                        if self.VaultTransaction == nil {
                            self.VaultTransaction = [VaultTransaction_Payload]()
                            self.CreateTransactionSection(transactions: betsvault.payload)
                        }
                        else {
                            self.VaultTransaction.removeAll()
                        }
                        self.VaultTransaction = betsvault.payload
                        self.getCoinList()
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.Nodatafound(hideshow: false)
                        })
                        ApiLoader.shared.stopHUD()
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.Nodatafound(hideshow: false)
                    })
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    func getCoinList() {
        background {
            if self.Carousel_Coins == nil {
                main {
                    self.Carousel_Coins = [CoinListPayload]()
                }
            }
            else {
                main {
                    self.Carousel_Coins.removeAll()
                }
            }
            
            let coinobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? CoinListRootClass
            if coinobj?.payload! == nil {
                
                let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
                NetworkingRequests.shared.requestPOST(API_CoinList, Parameters: [:], Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        let coinlist: CoinListRootClass = CoinListRootClass.init(fromDictionary: responseObject)
                        if status && coinlist.status {
                            // TODO: Remove GXT coin from list
                            // var array = coinlist.payload
                            // array?.removeAll(where: { (coin) -> Bool in
                            //    coin.coin.uppercased() == "gxt".uppercased()
                            // })
                            // self.Carousel_Coins = UserUtility().CoinArrangeOrder(coins: array!)
                            self.Carousel_Coins = UserUtility().CoinArrangeOrder(coins: coinlist.payload)
                            UserInfoData.shared.SaveObjectdata(data: coinlist as AnyObject, forkey: CoinsList)
                            self.getFundVault()
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.Nodatafound(hideshow: false)
                            })
                            ApiLoader.shared.stopHUD()
                        }
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            self.Nodatafound(hideshow: false)
                        })
                        ApiLoader.shared.stopHUD()
                    }
                }
            }
            else {
                main {
                    self.Carousel_Coins = UserUtility().CoinArrangeOrder(coins: (coinobj?.payload!)!)
                    self.getFundVault()
                }
            }
        }
    }
    
    func getFundVault() {
        background {
            let balanceobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? BetsVaultPayload
            if balanceobj == nil {
                
                let param = FundVaultParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
                let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
                NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        if status {
                            let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject as NSDictionary)
                            UserInfoData.shared.SaveObjectdata(data: betsvault.payload as AnyObject, forkey: Vault_Balancelist)
                            self.Balance_payload = betsvault.payload
                            self.Nodatafound(hideshow: true)
                            self.TransactionsTBL.delegate = self
                            self.TransactionsTBL.dataSource = self
                            self.TransactionsTBL.reloadData()
                            self.TransactionFilter()
                            self.AccountHeader()
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.Nodatafound(hideshow: false)
                            })
                        }
                        ApiLoader.shared.stopHUD()
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            self.Nodatafound(hideshow: false)
                        })
                        ApiLoader.shared.stopHUD()
                    }
                }
            }
            else {
                main {
                    self.Balance_payload = balanceobj
                    self.Nodatafound(hideshow: true)
                    self.TransactionsTBL.delegate = self
                    self.TransactionsTBL.dataSource = self
                    self.TransactionsTBL.reloadData()
                    self.TransactionFilter()
                    self.AccountHeader()
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedDeposit(_ sender: Any) {
        let popupNavController = loadViewController("Main", "PopupBottomNavigation") as! PopupBottomNavigation
        popupNavController.height = Screen_height
        popupNavController.topCornerRadius = 0
        popupNavController.presentDuration = 0.5
        popupNavController.dismissDuration = 0.5
        popupNavController.shouldDismissInteractivelty = true
        present(popupNavController, animated: true, completion: nil)
    }
    
    @IBAction func TappedFUnd(_ sender: Any) {
        let popupNavController = loadViewController("Main", "PopupBottomNavigation2") as! PopupBottomNavigation2
        popupNavController.height = Screen_height
        popupNavController.topCornerRadius = 0
        popupNavController.presentDuration = 0.5
        popupNavController.dismissDuration = 0.5
        popupNavController.shouldDismissInteractivelty = true
        present(popupNavController, animated: true, completion: nil)
    }
    
}

extension AccountDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func getTransactionOBJ() -> [VaultTransaction_TransactionList]? {
        let Carousel = self.Carousel_Coins[self.SelectedBottomCell - 1]
        let type = self.OptionArry[self.SelectedOption]
        let data = self.sectionArray.filter { (sec_data) -> Bool in
            sec_data.sectionkey.uppercased() == Carousel.coin.uppercased()
        }.first
        let sectionarry = data?.sectionArray.filter({ (array) -> Bool in
            array.sectionkey.uppercased() == type.uppercased()
            }).first
        guard sectionarry != nil else {
            return []
        }
        return (sectionarry?.transactionList)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.SelectedBottomCell == 0 {
            return self.FilterTransaction.count
        }
        else {
            let count = self.getTransactionOBJ()!.count
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TransactionsCell = tableView.dequeueReusableCell(withIdentifier: "TransactionsCell") as! TransactionsCell
        
        self.TransactionConfigCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func TransactionConfigCell(cell: TransactionsCell, indexPath: IndexPath) {
        let data: VaultTransaction_TransactionList = self.SelectedBottomCell == 0 ? self.FilterTransaction[indexPath.row] : self.getTransactionOBJ()![indexPath.row]
        var USDPrice: Double = 0.0
        
        if data.vaultCoin.uppercased() == "eth".uppercased() {
            USDPrice = data.txnValue ?? 0.0 * Double(self.CMC_Prices.ethPrice ?? 0.0)
        }
        else if data.vaultCoin.uppercased() == "usdt".uppercased() {
            USDPrice = data.txnValue ?? 0.0 * Double(self.CMC_Prices.usdtPrice ?? 0.0)
        }
        else if data.vaultCoin.uppercased() == "btc".uppercased() {
            USDPrice = data.txnValue ?? 0.0 * Double(self.CMC_Prices.btcPrice ?? 0.0)
        }
        else if data.vaultCoin.uppercased() == "btc".uppercased() {
            USDPrice = data.txnValue ?? 0.0 * Double(self.CMC_Prices.btcPrice ?? 0.0)
        }
        else if data.vaultCoin.uppercased() == "usd".uppercased() {
            USDPrice = data.txnValue ?? 0.0
        }
        
        if self.SelectedOption == 0 || self.SelectedOption == 2 {
            cell.TitleLBL.text = data.clientApp
            cell.LogoIMG.isHidden = true
            cell.TitleCenter.constant = -10
            cell.TitleLBL.isHidden = false
            cell.NotesLBL.isHidden = false
            cell.PriceLBL.text = String.init(format: "%f", USDPrice).CoinPriceThumbRules(Coin: data.vaultCoin)
            if let variableName = data.tickets { // If casting, use, eg, if let var = abc as? NSString
                cell.NotesLBL.text = String.init(format: "Quantity: %d", variableName.count)
            } else {
                cell.NotesLBL.text = String.init(format: "Quantity: 0")
            }
        }
        else {
            cell.LogoIMG.isHidden = false
            cell.NotesLBL.isHidden = true
            cell.TitleCenter.constant = 0
            
            switch data.vaultCoin.uppercased() {
            case "BTC".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "BTCDeposite")
                }
                break
                
            case "ETH".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "ethereum_logo")
                }
                break
                
            case "GXT".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "gxtoken_logo")
                }
                break
                
            case "USDT".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "Tether_Logo")
                }
                break
                
            case "xmr".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "Monero_logo")
                }
                break
                
            case "ltc".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "Litecoin_logo")
                }
                break
                
            case "xrp".uppercased():
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "Ripple_logo")
                }
                break
                
            default:
                if data.destination.uppercased() == "betsvault".uppercased() || data.destination.uppercased() == "lotteryvault".uppercased() {
                    cell.LogoIMG.image = UIImage.init(named: "VaultDeposit")
                }
                else {
                    cell.LogoIMG.image = UIImage.init(named: "BetsAddress")
                }
                break
            }
        
            cell.TitleLBL.isHidden = true
            cell.PriceLBL.text = String.init(format: "%f", USDPrice).CoinPriceThumbRules(Coin: data.vaultCoin)
        }
    }
}

//MARK:- Carousel Delegates
extension AccountDetailVC: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.Carousel_Coins.count + 1
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: CarouselCoinView
        
        if let view = view {
            itemView = view as! CarouselCoinView
        } else {
            itemView = CarouselCoinView(frame: CGRect(x: 5, y: 5, width: carousel.frame.height - 5, height: carousel.frame.height - 5))
            if index == 0 {
                itemView.setupView(image: UIImage.init(named: "SF_a"), imageURL: "", imgTin: (self.SelectedBottomCell == index ? UIColor.white : AppDarkColor), bgcolor: (self.SelectedBottomCell == index ? AppDarkColor : UIColor.white))
                itemView.HighlightView(imgTin: self.SelectedBottomCell == index ? UIColor.white : AppDarkColor, bgcolor: self.SelectedBottomCell == index ? AppDarkColor : UIColor.white)
            }
            else {
                let coindata = self.Carousel_Coins[index - 1]
                itemView.setupView(imageURL: coindata.imageUrl, imgTin: (self.SelectedBottomCell == index ? UIColor.white : AppDarkColor), bgcolor: (self.SelectedBottomCell == index ? AppDarkColor : UIColor.white))
                itemView.HighlightView(imgTin: self.SelectedBottomCell == index ? UIColor.white : AppDarkColor, bgcolor: self.SelectedBottomCell == index ? AppDarkColor : UIColor.white)
            }
        }
        
        return itemView
    }
    
    private func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.SelectedBottomCell = carousel.currentItemIndex
    }
    
    func carouselItemIndexDidChange(_ carousel: iCarousel, withpreviousIndex previous: Int, withcurrentIndex Current: Int) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCurlDown, animations: { () -> Void in
            var currentview: CarouselCoinView!
            currentview = carousel.itemView(at: Current) as? CarouselCoinView
            currentview.HighlightView(imgTin: .white, bgcolor: AppDarkColor)
            
            var previview: CarouselCoinView!
            previview = carousel.itemView(at: previous) as? CarouselCoinView
            previview.HighlightView(imgTin: AppDarkColor, bgcolor: .white)
        }, completion: { (finished:Bool) -> Void in
            self.AccountHeader()
        })
    }
    
}
