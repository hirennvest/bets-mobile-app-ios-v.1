//
//  AccountHeaderView.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-11.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class AccountHeaderView: UIView {

    @IBOutlet weak var AccountInfoView: CustomView!
    @IBOutlet weak var ProfileBTN: CustomBTN!
    @IBOutlet weak var TypeLBL: UILabel!
    @IBOutlet weak var NameLBL: UILabel!
    @IBOutlet weak var TotalLBL: UILabel!
    @IBOutlet weak var BalanceLBL: UILabel!
    
    @IBOutlet weak var InvestView: UIView!
    @IBOutlet weak var InvestLBL: UILabel!
    @IBOutlet weak var InvestAmount: UILabel!
    
    @IBOutlet weak var LiquidView: UIView!
    @IBOutlet weak var LiquidLBL: UILabel!
    @IBOutlet weak var LiquidAmount: UILabel!
    
    @IBOutlet weak var Carousel: iCarousel!
    
    @IBOutlet weak var OptionView: CustomView!
    @IBOutlet weak var PreviousBTN: UIButton!
    @IBOutlet weak var OptionTitleLBL: UILabel!
    @IBOutlet weak var NextBTN: UIButton!
    
    func NextactionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    func PreviousactionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "AccountHeaderView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.backgroundColor = .white
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
        self.setupview()
    }
    
    
    func setupview() {
        self.TypeLBL.theme_textColor = GlobalPicker.notesColor
        self.TypeLBL.text = HeaderSetter().email
        self.TypeLBL.font = Font().MontserratRegularFont(font: 12)
        self.NameLBL.theme_textColor = GlobalPicker.textColor
        self.NameLBL.font = Font().MontserratBoldFont(font: 18)
        self.TotalLBL.theme_textColor = GlobalPicker.notesColor
        self.TotalLBL.font = Font().MontserratRegularFont(font: 12)
        self.BalanceLBL.theme_textColor = GlobalPicker.textColor
        self.BalanceLBL.font = Font().MontserratBoldFont(font: 18)
        self.InvestLBL.text = "Invested"
        self.InvestLBL.font = Font().MontserratRegularFont(font: 8)
        self.InvestLBL.theme_textColor = GlobalPicker.notesColor
        self.InvestAmount.theme_textColor = GlobalPicker.textColor
        self.InvestAmount.font = Font().MontserratBoldFont(font: 15)
        self.LiquidLBL.text = "Liquid"
        self.LiquidLBL.font = Font().MontserratRegularFont(font: 8)
        self.LiquidLBL.theme_textColor = GlobalPicker.notesColor
        self.LiquidAmount.theme_textColor = GlobalPicker.textColor
        self.LiquidAmount.font = Font().MontserratBoldFont(font: 15)
        self.OptionTitleLBL.theme_textColor = GlobalPicker.textColor
        self.OptionTitleLBL.font = Font().MontserratBoldFont(font: 15)
        let userdata = UserInfoData.shared.GetUserdata()
        self.ProfileBTN.imageView?.downloadedFrom(link: userdata?.profileImg, contentMode: UIView.ContentMode.center, radious: self.ProfileBTN.frame.height / 2)
    }
    
    func shake(count : Float = 2,for duration : TimeInterval = 0.5,withTranslation translation : Float = 5) {

        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [translation, -translation]
        self.OptionTitleLBL.layer.add(animation, forKey: "shake")
    }
    
    @IBAction func TappedProfileBTN(_ sender: Any) {
    }
    
    @IBAction func TappedPreviousBTN(_ sender: UIButton) {
        self.PreviousactionHandler()
    }
    
    @IBAction func TappedNextBTN(_ sender: UIButton) {
        self.NextactionHandler()
    }

}
