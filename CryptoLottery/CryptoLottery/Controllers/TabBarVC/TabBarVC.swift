//
//  TabBarVC.swift
//  ClassRoom
//
//  Created by Shorupan Pirakaspathy on 2020-04-08.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import AVKit
import PiPhone


class TabBarVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var TabBGview: UIView!
    
    @IBOutlet weak var TabView: UIView!
    
    @IBOutlet weak var Tab1BTN: UIButton!
    
    @IBOutlet weak var Tab2View: CustomView!
    @IBOutlet weak var Tab2BTN: CustomBTN!
    
    @IBOutlet weak var Tab3BTN: UIButton!
    
    //    MARK:- Variable Define
    
    var isMenuActive: Bool!
    var menuTypeSwitch: Bool!
    var buttonSwitched : Bool = false
    var isSelectedCenter: Bool = false
    var isopenTab: Bool = false
    var isSelectedIndex: Int = 0
    var igcMenu: IGCMenu!
    
    
    let urls = ["https://video.twimg.com/ext_tw_video/1110679043915935745/pu/vid/720x1280/6OeRxCMr3tYaDNLB.mp4?tag=8", "https://youtu.be/vZNb8Yz5bsA"]
    
    let adjustmentBehaviors = [(behavior: PiPManagerContentInsetAdjustmentBehavior.navigationBar, title: "Navigation bar"),
                               (.tabBar, "Tab bar"),
                               (.navigationAndTabBars, "Navigation and Tab bars"),
                               (.safeArea, "Safe area"),
                               (.none, "None")]
    
    //    MARK:- Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .TabbarUpdate, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tab2View.BGColor = AppDarkColor.withAlphaComponent(0.5)
        self.Tab2BTN.BTN_BGColor = AppDarkColor
    }
    
    //    MARK:- UserDefine Methods
    
    @objc func onDidReceiveData(_ notification:Notification) {
        DispatchQueue.main.async {
            if let tab = notification.userInfo?["Tab"] as? Int {
                if tab == 0 {
                    self.TappedTab1(self.Tab1BTN as Any)
                }
                else if tab == 0 {
                    self.TappedTab2(self.Tab2BTN as Any)
                }
                else {
                    self.TappedTab3(self.Tab3BTN as Any)
                }
            }
        }
    }
    
    func setupContainerController(_ controller: SwiftyPageController) {
        // assign variable
        self.containerController = controller
        //        containerController.isEnabledInteractive = false
        
        // set delegate
        self.containerController.delegate = self
        
        // set animation type
        self.containerController.animator = .default
        
        // set view controllers
        let v1 = FeedsVC.init(nibName: "FeedsVC", bundle: nil)
        let v2 = BetIconVC.init(nibName: "BetIconVC", bundle: nil)
        let v3 = AccountDetailVC.init(nibName: "AccountDetailVC", bundle: nil)
        
        self.containerController.viewControllers = [v1, v2, v3]
        
        // select needed controller
        
        self.Tab1BTN.setImage(UIImage.init(named: "Activetab1"), for: .normal)
        self.Tab3BTN.setImage(UIImage.init(named: "Inactivetab3"), for: .normal)
        self.Tab1BTN.tintColor = AppDarkColor
        self.Tab2BTN.tintColor = .lightGray
        self.Tab3BTN.tintColor = .lightGray
        self.Tab1BTN.alpha = 1.0
        self.Tab3BTN.alpha = 0.7
        self.containerController.selectController(atIndex: self.isSelectedIndex, animated: false)
        
        self.isMenuActive = false
        self.menuTypeSwitch = true
        self.SetupMenu()
        
        let LongGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(BetCenterTab))
        self.Tab2BTN.addGestureRecognizer(LongGesture)
        
    }
    
    func SetupMenu() {
        if (self.igcMenu == nil) {
            self.igcMenu = IGCMenu.init()
        }
        self.igcMenu.menuButton = self.Tab2BTN   //Pass refernce of menu button
        self.igcMenu.menuSuperView = self.view      //Pass reference of menu button super view
        self.igcMenu.disableBackground = true       //Enable/disable menu background
        self.igcMenu.delegate = self
        self.igcMenu.numberOfMenuItem = 6
        self.igcMenu.backgroundType = BlurEffectExtraLight
        
        /* Optional
         Pass name of menu items
         **/
        self.igcMenu.menuItemsNameArray = ["Support", "Arcade", "Chats", "Stream", "Trending", "Logout"]
        self.igcMenu.menuBackgroundColorsArray = [AppDarkColor, AppDarkColor, AppDarkColor, AppDarkColor, AppDarkColor, AppDarkColor]
        self.igcMenu.menuImagesNameArray = ["Supports", "Arcade", "Chats", "Stream", "Trending", "Logout"]
        self.igcMenu.videoArray = ["hello", "hello"];
    }
    
    @objc func MenuOption() {
        if (isMenuActive) {
            self.dissmissingMenu()
            self.isMenuActive = false
            self.buttonSwitched = !self.buttonSwitched
            self.isopenTab = true
        }
        else{
//            if self.isopenTab {
//                self.BetCenterTab()
//            }
//            else {
                self.menuTypeSwitch ? self.igcMenu.showGridMenu() : self.igcMenu.showCircularMenu()
                self.isMenuActive = true
//            }
        }
    }
    
    @objc func BetCenterTab() {
        self.Tab1BTN.setImage(UIImage.init(named: "Inactivetab1"), for: .normal)
        self.Tab3BTN.setImage(UIImage.init(named: "Inactivetab3"), for: .normal)
        self.isopenTab = false
        self.isSelectedCenter = true
        self.buttonSwitched = !self.buttonSwitched
        self.containerController.selectController(atIndex: 1, animated: true)
        self.Tab1BTN.tintColor = .lightGray
        self.Tab2BTN.tintColor = AppDarkColor
        self.Tab3BTN.tintColor = .lightGray
        self.Tab1BTN.alpha = 0.7
        self.Tab3BTN.alpha = 0.7
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let containerController = segue.destination as? SwiftyPageController {
            setupContainerController(containerController)
        }
    }
    
    // MARK:- PIP View Configure
    func constructPlayer() -> AVPlayer {
        let playerItems = urls.compactMap(URL.init(string:)).map(AVPlayerItem.init)
        return AVQueuePlayer(items: playerItems)
    }
    
    func OpenPIPview() {
        DispatchQueue.main.async {
            let video = VideoStreamingVC.init(nibName: "VideoStreamingVC", bundle: nil)
            video.delegate = self
            video.player = self.constructPlayer()
            PiPManager.contentInsetAdjustmentBehavior = self.adjustmentBehaviors[1].behavior
            self.navigationController?.pushViewController(video, animated: true)
        }
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func TappedTab1(_ sender: Any) {
        self.isopenTab = false
        self.SetupMenu()
        if self.Tab1BTN.isSelected {
            NotificationCenter.default.post(name: .ReloadFeed, object: nil)
            NotificationCenter.default.post(name: .FeedTimer, object: nil)
            self.Tab1BTN.isSelected = false
        }
        else {
            self.Tab1BTN.isSelected = true
        }
        self.Tab1BTN.setImage(UIImage.init(named: "Activetab1"), for: .normal)
        self.Tab3BTN.setImage(UIImage.init(named: "Inactivetab3"), for: .normal)
        self.containerController.selectController(atIndex: 0, animated: true)
        self.Tab1BTN.tintColor = AppDarkColor
        self.Tab2BTN.tintColor = .lightGray
        self.Tab3BTN.tintColor = .lightGray
        self.Tab1BTN.alpha = 1.0
        self.Tab3BTN.alpha = 0.7
        self.isSelectedCenter = false
    }
    
    @IBAction func TappedTab2(_ sender: Any) {
        self.SetupMenu()
        self.MenuOption()
    }
    
    @IBAction func TappedTab3(_ sender: Any) {
        self.Tab1BTN.setImage(UIImage.init(named: "Inactivetab1"), for: .normal)
        self.Tab3BTN.setImage(UIImage.init(named: "Activetab3"), for: .normal)
        self.isopenTab = false
        self.SetupMenu()
        self.containerController.selectController(atIndex: 2, animated: true)
        self.Tab1BTN.tintColor = .lightGray
        self.Tab2BTN.tintColor = .lightGray
        self.Tab3BTN.tintColor = AppDarkColor
        self.isSelectedCenter = false
        self.Tab1BTN.alpha = 0.7
        self.Tab3BTN.alpha = 1.0
    }
    
}

// MARK: - PagesViewControllerDelegate

extension TabBarVC: SwiftyPageControllerDelegate {
    
    func swiftyPageController(_ controller: SwiftyPageController, alongSideTransitionToController toController: UIViewController) {
        
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, didMoveToController toController: UIViewController) {
        
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, willMoveToController toController: UIViewController) {
        
    }
    
}

extension TabBarVC: IGCMenuDelegate {
    
    func dissmissingMenu()  {
        if (self.menuTypeSwitch) {
            self.igcMenu.hideGridMenu()
        }
        else{
            self.igcMenu.hideCircularMenu()
        }
        self.isMenuActive = false
        self.buttonSwitched = !self.buttonSwitched
    }
    
    func igcMenuSelected(_ selectedMenuName: String!, at index: Int) {
        if index == 0 {
            let support: SupportVC = loadViewController("SupportTab", "SupportVC") as! SupportVC
            self.navigationController?.pushViewController(support, animated: true)
        }
        else if index == 1 {
            let call: IncomingCallVC = IncomingCallVC.init(nibName: "IncomingCallVC", bundle: nil)
            self.navigationController?.pushViewController(call, animated: true)
        }
        else if index == 2 {
            
        }
        else if index == 3 {
//            Camera
            let camera = StreamingVC.init(nibName: "StreamingVC", bundle: nil)
            self.navigationController?.pushViewController(camera, animated: true)
        }
        else if index == 4 {
            self.TappedTab1(self.Tab1BTN as Any)
            NotificationCenter.default.post(name: .ReloadFeed, object: nil)
        }
        else {
            DispatchQueue.main.async {
                UserInfoData.shared.UserLogout()
            }
        }
        self.dissmissingMenu()
    }
    
    func igcVideostreamingSelected(_ index: Int) {
        self.OpenPIPview()
        self.dissmissingMenu()
    }
    
}


extension TabBarVC: VideoStreamingVCDelegate {
    func customPlayerViewController(_ customPlayerViewController: VideoStreamingVC, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
        if navigationController!.viewControllers.firstIndex(of: customPlayerViewController) != nil {
            completionHandler(true)
        } else {
            navigationController!.pushViewController(customPlayerViewController, animated: true)
            completionHandler(true)
        }
    }
}
