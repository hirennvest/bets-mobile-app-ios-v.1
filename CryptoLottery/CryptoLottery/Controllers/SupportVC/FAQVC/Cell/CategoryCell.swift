//
//  CategoryCell.swift
//  CryptoLottery
//
//  Created by Hiren on 16/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var logoIMG: UIImageView!
    @IBOutlet weak var TItleLBL: UILabel!
    @IBOutlet weak var notesLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.backgroundColor = .white
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 20
        
        self.logoIMG.clipsToBounds = true
        self.logoIMG.layer.cornerRadius = self.logoIMG.frame.height / 2
        
        self.TItleLBL.font = Font().MontserratRegularFont(font: 17)
        self.notesLBL.font = Font().MontserratLightFont(font: 11)
        
    }

}
