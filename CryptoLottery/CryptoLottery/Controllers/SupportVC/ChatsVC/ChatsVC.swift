//
//  ChatsVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import GrowingTextView
import IHKeyboardAvoiding
import Lottie

class ChatsVC: UIViewController {

    // MARK:- IBOutlet Defines
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var NavLogo: UIImageView!
    
    @IBOutlet weak var ChatTBL: TPKeyboardAvoidingTableView!
    
    @IBOutlet weak var lblNewsBanner: UILabel!
    
    @IBOutlet weak var BottomView: UIView!
    
    @IBOutlet weak var NewsBannerView: UIView!
    @IBOutlet weak var NewsBannerView_Height: NSLayoutConstraint!
    @IBOutlet weak var TypingAnimation: UIView!
    @IBOutlet weak var lblOtherUserActivityStatus: UILabel!
    
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var SendBtn: UIButton!
    
    // MARK:- Variable Defines
    
    var bannerLabelTimer: Timer!
    var username: String = (UserInfoData.shared.GetUserdata()?.username)!
    var chat_Arry: [ChatsMessage] = []
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .OnHoldView, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleConnectedUserUpdateNotification), name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleDisconnectedUserUpdateNotification), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUserTypingNotification), name: NSNotification.Name(rawValue: "userTypingNotification"), object: nil)
        
        self.setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- User Define Methods
    func randomString(of length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var s = ""
        for _ in 0 ..< length {
            s.append(letters.randomElement()!)
        }
        return s
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.setupUI()
    }

    func setupUI() {
        
        self.NewsBannerView.isHidden = true
        self.NewsBannerView_Height.constant = 0
        
        self.NavView.backgroundColor = AppDarkColor
        self.NavLogo.tintColor = UIColor.white
        
        self.ChatTBL.register(UINib.init(nibName: "ChatCellSender", bundle: nil), forCellReuseIdentifier: "ChatCellSender")
        self.ChatTBL.register(UINib.init(nibName: "ChatCellReciver", bundle: nil), forCellReuseIdentifier: "ChatCellReciver")
        self.ChatTBL.backgroundColor = .clear
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 50))
        footer.backgroundColor = UIColor.clear
        self.ChatTBL.tableFooterView = footer
        
        ViewLoader.shared.showHUD(view: self)
        SocketConnect.shared.getChatMessage { (responseObject) in
            for item in responseObject {
                let message = ChatsMessage.init(fromDictionary: item)
                self.chat_Arry.append(message)
            }
            self.ChatTBL.delegate = self
            self.ChatTBL.dataSource = self
            self.ChatTBL.reloadData {
                ViewLoader.shared.stopHUD()
                self.textView.text = ""
                self.ChatTBL.scrollToBottom(animated: true)
            }
        }
        
        self.textView.delegate = self
        
        self.SendBtn.layer.cornerRadius = 15.0
        self.SendBtn.clipsToBounds = true
        
        self.BottomView.backgroundColor = .white
        
        self.textView.layer.cornerRadius = 4.0
        self.textView.clipsToBounds = true
        self.textView.layer.borderWidth = 1.0
        self.textView.layer.borderColor = AppDarkColor.cgColor
        self.textView.placeholder = "Type Your Message"
        self.textView.font = UIFont.systemFont(ofSize: 15)
        self.textView.minHeight = 30
        self.textView.maxHeight = 150
        self.textView.maxLength = 250
        
        self.textViewBottomConstraint.constant = 15
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 15
            self.ChatTBL.scrollToBottom(animated: true)
            view.layoutIfNeeded()
        }
    }

    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    func configureNewsBannerLabel() {
        lblNewsBanner.layer.cornerRadius = 15.0
        lblNewsBanner.clipsToBounds = true
        lblNewsBanner.alpha = 0.0
    }

    func configureOtherUserActivityLabel() {
        self.NewsBannerView.isHidden = true
        lblOtherUserActivityStatus.text = ""
    }
    
    func addLottieTyping() {
        let lottie = AnimationView(name: "Typing")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.TypingAnimation.addSubview(lottie)
        self.TypingAnimation.backgroundColor = .clear
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.TypingAnimation.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.TypingAnimation.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.TypingAnimation.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.TypingAnimation.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.TypingAnimation.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.TypingAnimation.frame.size.width).isActive = true
        lottie.play()
    }
    
    func scrollToBottom() {
        let delay = 0.1 * Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay){
            if self.chat_Arry.count > 0 {
                let lastRowIndexPath = NSIndexPath(row: self.chat_Arry.count - 1, section: 0)
                self.ChatTBL.scrollToRow(at: lastRowIndexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }
    }
    
    func showBannerLabelAnimated() {
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 1.0
            
            }) { (finished) -> Void in
                self.bannerLabelTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.hideBannerLabel), userInfo: nil, repeats: false)
        }
    }
    
    @objc func hideBannerLabel() {
        if bannerLabelTimer != nil {
            bannerLabelTimer.invalidate()
            bannerLabelTimer = nil
        }
        
        UIView.animate(withDuration: 0.75, animations: { () -> Void in
            self.lblNewsBanner.alpha = 0.0
            
            }) { (finished) -> Void in
        }
    }

    @objc func dismissKeyboard() {
        if self.textView.isFirstResponder {
            self.textView.resignFirstResponder()
            SocketConnect.shared.sendStopTypingMessage(nickname: "")
        }
    }
    
    @objc func handleConnectedUserUpdateNotification(notification: NSNotification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        let connectedUserNickname = connectedUserInfo["nickname"] as? String
        lblNewsBanner.text = "User \(connectedUserNickname!.uppercased()) was just connected."
        showBannerLabelAnimated()
    }
    
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased()) has left."
        showBannerLabelAnimated()
    }
    
    @objc func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String: AnyObject] {
            var names = ""
            var totalTypingUsers = 0
            for (typingUser, _) in typingUsersDictionary {
                if typingUser != "" {
                    names = (names == "") ? typingUser : "\(names), \(typingUser)"
                    totalTypingUsers += 1
                }
            }
            
            if totalTypingUsers > 0 {
                let verb = (totalTypingUsers == 1) ? "is" : "are"
                lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
                self.addLottieTyping()
                self.NewsBannerView_Height.constant = 30
                self.NewsBannerView.isHidden = false
            }
            else {
                self.NewsBannerView_Height.constant = 0
                self.NewsBannerView.isHidden = true
            }
        }
        
    }

    // MARK:- IBAction Methods
    @IBAction func TappedSendBTN(_ sender: Any) {
        if !self.textView.text.isEmpty || !self.textView.text.isBlank {
            SocketConnect.shared.sendMessage(message: self.textView.text, timestamp: Int64(Date().timeIntervalSince1970 * 1000)) { (responseObject) in
                let dict = [
                    "sender": responseObject.sender as Any,
                    "thread_id": self.randomString(of: 10),
                    "id": responseObject.identifier as Any,
                    "message": self.textView.text as Any,
                    "timestamp": Double(Int64(Date().timeIntervalSince1970 * 1000))
                    ] as [String : Any]
                let message = ChatsMessage.init(fromDictionary: dict)
                self.chat_Arry.append(message)
                self.ChatTBL.reloadData {
                    self.textView.text = ""
                    self.ChatTBL.scrollToBottom(animated: true)
                }
            }
        }
    }
    
}

// MARK:- GrowingTextViewDelegate
extension ChatsVC: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

// MARK:- TextViewDelegate
extension ChatsVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        SocketConnect.shared.sendStartTypingMessage(nickname: "")
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }

    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }

    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool {
        return true
    }
    
}

//MARK:- Tableview Delegate and datasource
extension ChatsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chat_Arry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let messages = self.chat_Arry[indexPath.row]
        if messages.sender.uppercased() == self.username.uppercased() {
            let cell: ChatCellReciver = tableView.dequeueReusableCell(withIdentifier: "ChatCellReciver") as! ChatCellReciver
            cell.SetupCell(message: messages)
            return cell
        }
        else {
            let cell: ChatCellSender = tableView.dequeueReusableCell(withIdentifier: "ChatCellSender") as! ChatCellSender
            cell.SetupCell(message: messages)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
