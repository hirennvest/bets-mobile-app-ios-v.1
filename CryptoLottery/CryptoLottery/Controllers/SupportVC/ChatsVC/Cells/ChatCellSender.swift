//
//  ChatCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-08.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class ChatCellSender: UITableViewCell {

    @IBOutlet weak var ChatView: UIView!
    @IBOutlet weak var bubble: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var CornerLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ChatView.backgroundColor = UIColor.white
        self.date.font = Font().MontserratExtraLightFont(font: 11)
        self.date.textColor = UIColor.black
        self.message.font = Font().MontserratRegularFont(font: 17)
        self.message.textColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    func setupBubble(color: UIColor = UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.00)) {
    func setupBubble(color: UIColor = UIColor.lightGray) {
        self.bubble.clipsToBounds = true
        self.bubble.backgroundColor = color
        self.CornerLBL.backgroundColor = color
    }
    
    func SetupCell(message: ChatsMessage) {
        self.setupBubble()
        let dates = NSDate(timeIntervalSince1970: message.date / 1000)
        self.date.text = String.init(format: "%@", self.getMSGDate(dates: dates as Date))
        self.message.text = message.message
        self.bubble.layer.cornerRadius = 15.0
    }
    
    func getMSGDate(dates: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy, h:mm a"
        return dateFormatter.string(from: dates)
    }
}
