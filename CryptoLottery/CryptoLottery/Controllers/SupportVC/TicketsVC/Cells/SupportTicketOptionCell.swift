//
//  SupportTicketOptionCell.swift
//  CryptoLottery
//
//  Created by Hiren on 14/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SupportTicketOptionCell: UITableViewCell {

    @IBOutlet weak var Titlelbl: UILabel!
    @IBOutlet weak var Noteslbl: UILabel!
    @IBOutlet weak var Images: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Titlelbl.textColor = .lightGray
        self.Titlelbl.font = Font().MontserratLightFont(font: 12)
        
        self.Noteslbl.textColor = .black
        self.Noteslbl.font = Font().MontserratBoldFont(font: 17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
