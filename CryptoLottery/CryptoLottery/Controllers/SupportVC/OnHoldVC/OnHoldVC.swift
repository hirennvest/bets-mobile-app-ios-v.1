//
//  OnHoldVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class OnHoldVC: UIViewController {
    
    // MARK:- IBOutlet Controls
    @IBOutlet weak var HoldTBL: UITableView!
    
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var Bottom_Height: NSLayoutConstraint!
    @IBOutlet weak var Bottom_Top: NSLayoutConstraint!
    
    @IBOutlet weak var HoldBTN: UIButton!
    
    @IBOutlet weak var TimerView: UIView!
    @IBOutlet weak var TimerLBL: UILabel!
    @IBOutlet weak var MinuteLBL: UILabel!
    @IBOutlet weak var SecondLBL: UILabel!
    
    @IBOutlet weak var SchedulerView: UIView!
    @IBOutlet weak var ScheduleTitleLBL: UILabel!
    @IBOutlet weak var CallmeNow: UIButton!
    @IBOutlet weak var ScheduleCall: UIButton!
    
    // MARK:- Variable Defines
    
    let IssueType_arry: [String] = ["Transaction", "Technical", "Communication"]
    let Nature_arry: [String] = ["I Couldn't Make The Transaction", "Question About A Transaction That Happened"]
    let transaction_arry: [String] = ["Deposit", "Withdrawal", "Buy", "Sell", "Earn"]
    var Selection_Arry: [String] = []
    var SelectedOption: Int = 0
    
    var timer: Timer!
    var count = 300
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .OnHoldView, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    // MARK:- User Define Methods
    @objc func onDidReceiveData(_ notification:Notification) {
        self.setupUI()
    }
    
    func setupUI() {
        
        self.HoldBTN.isHidden = false
        self.BottomView.isHidden = false
        self.SchedulerView.isHidden = true
        self.TimerView.isHidden = true
        
        self.Selection_Arry.removeAll()
        self.SelectedOption = 0
        self.view.backgroundColor = AppDarkColor
        
        self.HoldTBL.register(UINib.init(nibName: "HoldIssueCell", bundle: nil), forCellReuseIdentifier: "HoldIssueCell")
        self.HoldTBL.backgroundColor = .clear
        
        self.HoldTBL.delegate = self
        self.HoldTBL.dataSource = self
        self.HoldTBL.reloadData()
        
        self.BottomView.backgroundColor = .white
        let maskPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.BottomView.frame.width, height: self.BottomView.frame.height),
                                    byRoundingCorners: [.topRight, .topLeft],
                                    cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.BottomView.layer.mask = maskLayer
        
        self.HoldBTN.clipsToBounds = true
        self.HoldBTN.layer.cornerRadius = self.HoldBTN.frame.height / 2
        self.HoldBTN.layer.borderWidth = 1
        self.HoldBTN.layer.borderColor = AppDarkColor.cgColor
        
        self.HoldBTN.setTitle("Get OnHold", for: .normal)
        self.HoldBTN.alpha = 0.5
        self.HoldBTN.isUserInteractionEnabled = false
        self.HoldBTN.titleLabel?.font = Font().MontserratRegularFont(font: 20.0)
    }
    
    func SetupSchedulerView() {
        
        self.BottomView.isHidden = false
        self.SchedulerView.isHidden = false
        self.HoldBTN.isHidden = true
        self.TimerView.isHidden = true
        
        self.SchedulerView.backgroundColor = .white
        self.ScheduleTitleLBL.text = "Do You Wont Us To Call Right Now Or Do You Wont A Scheduled Call Back?"
        self.ScheduleTitleLBL.font = Font().MontserratRegularFont(font: 20.0)
        
        self.CallmeNow.setTitle("Call Me Now!!", for: .normal)
        self.CallmeNow.titleLabel?.font = Font().MontserratRegularFont(font: 18.0)
        self.CallmeNow.setTitleColor(.white, for: .normal)
        self.CallmeNow.backgroundColor = AppDarkColor
        self.CallmeNow.layer.cornerRadius = 10
        
        self.ScheduleCall.setTitle("Schedule", for: .normal)
        self.ScheduleCall.titleLabel?.font = Font().MontserratRegularFont(font: 18.0)
        self.ScheduleCall.setTitleColor(.white, for: .normal)
        self.ScheduleCall.backgroundColor = AppDarkColor
        self.ScheduleCall.layer.cornerRadius = 10
    }
    
    func SetupTimerView() {
        self.TimerView.isHidden = false
        self.BottomView.isHidden = false
        self.SchedulerView.isHidden = true
        self.HoldBTN.isHidden = true
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(CallingTimer), userInfo: nil, repeats: true)
        
        self.TimerView.backgroundColor = .white
        self.TimerView.clipsToBounds = true
        self.TimerView.layer.cornerRadius = self.TimerView.frame.height / 2
        self.TimerView.layer.borderWidth = 1
        self.TimerView.layer.borderColor = AppDarkColor.cgColor
        
        let minutes = String(format: "%02i", self.count / 60)
        let seconds = String(format: "%02i", self.count % 60)
        self.TimerLBL.text = minutes + ":" + seconds
        self.TimerLBL.font = Font().MontserratRegularFont(font: 40.0)
        self.MinuteLBL.font = Font().MontserratRegularFont(font: 12.0)
        self.SecondLBL.font = Font().MontserratRegularFont(font: 12.0)
    }
    
    @objc func CallingTimer() {
        //example functionality
        if self.count > 0 {
            let minutes = String(format: "%02i", self.count / 60)
            let seconds = String(format: "%02i", self.count % 60)
            self.TimerLBL.text = minutes + ":" + seconds
            self.count -= 1
        }
        else {
            self.timer.invalidate()
            self.timer = nil
            self.count = 300
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedHoldBTN(_ sender: Any) {
        self.SetupSchedulerView()
    }
    
    @IBAction func TappedSchedulerBTN(_ sender: UIButton) {
        if sender == self.CallmeNow{
            self.SetupTimerView()
        }
        else {
    
        }
    }
    
}

//MARK:- Tableview Delegate and datasource
extension OnHoldVC: UITableViewDelegate, UITableViewDataSource {
    
    func getSection() -> Int {
        if self.SelectedOption == 0 {
            return 1
        }
        else if self.SelectedOption == 1 {
            return 2
        }
        else {
            return 3
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.getSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 50))
        view.backgroundColor = AppDarkColor
        
        let lbl = UILabel.init(frame: CGRect(x: 15, y: 0, width: Screen_width - 20, height: 30))
        lbl.font = Font().MontserratRegularFont(font: 12.0)
        lbl.textColor = .white
        view.addSubview(lbl)
        if section == 0 {
            lbl.text = "What Best Describes Your Type Of Issues?"
        }
        else if section == 1 {
            lbl.text = "Nature Of Transaction?"
        }
        else if section == 2 {
            lbl.text = "What Type Of Transaction Were You Trying To Make?"
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HoldIssueCell = tableView.dequeueReusableCell(withIdentifier: "HoldIssueCell") as! HoldIssueCell
        
        cell.backgroundColor = .clear
        cell.Collection.backgroundColor = UIColor.clear
        cell.Collection.register(UINib.init(nibName: "SupportHoldCell", bundle: nil), forCellWithReuseIdentifier: "SupportHoldCell")
        if let flowLayout = cell.Collection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1,height: 1)
        }
        cell.Collection.tag = indexPath.section
        cell.Collection.dataSource = self
        cell.Collection.delegate = self
        cell.Collection.reloadData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension OnHoldVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.IssueType_arry.count
        }
        else if collectionView.tag == 1 {
            return self.Nature_arry.count
        }
        else {
            return self.transaction_arry.count
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SupportHoldCell", for: indexPath) as! SupportHoldCell
        self.ConfigCell(cell: cell, indexPath: indexPath, collectionView: collectionView)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.SelectedOption == 0 {
            let title_option = IssueType_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if !value {
                self.SelectedOption = 1
                self.Selection_Arry.append(title_option)
            }
        }
        else if self.SelectedOption == 1 {
            let title_option = Nature_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if !value {
                self.SelectedOption = 2
                self.Selection_Arry.append(title_option)
            }
        }
        else if self.SelectedOption == 2 {
            let title_option = transaction_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if !value {
                self.SelectedOption = 3
                self.Selection_Arry.append(title_option)
            }
        }
        self.HoldTBL.reloadData {
            if self.SelectedOption == 3 {
                self.HoldBTN.alpha = 1.0
                self.HoldBTN.isUserInteractionEnabled = true
            }
        }
    }
    
    func ConfigCell(cell: SupportHoldCell, indexPath: IndexPath, collectionView: UICollectionView) {
        if collectionView.tag == 0 {
            let title_option = IssueType_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if value {
                cell.HoldTitleLBL.font = Font().MontserratBoldFont(font: 12.0)
            }
            else {
                cell.HoldTitleLBL.font = Font().MontserratRegularFont(font: 12.0)
            }
            cell.HoldTitleLBL.text = title_option
        }
        else if collectionView.tag == 1 {
            let title_option = Nature_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if value {
                cell.HoldTitleLBL.font = Font().MontserratBoldFont(font: 12.0)
            }
            else {
                cell.HoldTitleLBL.font = Font().MontserratRegularFont(font: 12.0)
            }
            cell.HoldTitleLBL.text = title_option
        }
        else {
            let title_option = transaction_arry[indexPath.row]
            let value = self.Selection_Arry.contains { (title) -> Bool in
                title.uppercased() == title_option.uppercased()
            }
            if value {
                cell.HoldTitleLBL.font = Font().MontserratBoldFont(font: 12.0)
            }
            else {
                cell.HoldTitleLBL.font = Font().MontserratRegularFont(font: 12.0)
            }
            cell.HoldTitleLBL.text = title_option
        }
    }
    
}
