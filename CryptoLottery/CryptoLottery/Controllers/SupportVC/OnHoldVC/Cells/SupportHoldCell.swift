//
//  SupportHoldCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-07-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SupportHoldCell: UICollectionViewCell {

    @IBOutlet weak var HoldView: UIView!
    @IBOutlet weak var HoldTitleLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.HoldView.backgroundColor = .white
        self.HoldView.layer.cornerRadius = 10
        self.HoldView.clipsToBounds = true
        
        self.HoldTitleLBL.font = Font().MontserratRegularFont(font: 12.0)
        self.HoldTitleLBL.textColor = AppDarkColor
    }

}
