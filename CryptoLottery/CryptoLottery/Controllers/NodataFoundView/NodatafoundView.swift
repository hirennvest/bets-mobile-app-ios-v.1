//
//  NodatafoundView.swift
//
//  Created by Hiren Joshi on 21/08/19.
//  Copyright © 2019 Hiren Joshi. All rights reserved.
//

import UIKit

@objc public class NodatafoundView: UIView {

    @IBOutlet var nodata: UIView!
    @IBOutlet var TitleLBL: UILabel!
    @IBOutlet var Img: UIImageView!
    @IBOutlet var DetailLBL: UILabel!
    @IBOutlet var RefreshBTN: UIButton!
    
    var didActionBlock: (()->())?
    // other outlets
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        
        let view = UINib(nibName: "NodatafoundView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        nodata.frame = bounds
        nodata.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(nodata)
        self.backgroundColor = .white
        nodata.backgroundColor = .white

        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(RefreshBTN(_:)))
        self.nodata.addGestureRecognizer(gesture)
        self.addGestureRecognizer(gesture)
        
        self.RefreshBTN.setTitle("Refresh Now", for: .normal)
        self.RefreshBTN.titleLabel?.font = Font().MontserratRegularFont(font: 17)
        self.RefreshBTN.backgroundColor = AppDarkColor
        self.RefreshBTN.setTitleColor(UIColor.white, for: .normal)
        self.RefreshBTN.clipsToBounds = true
        self.RefreshBTN.layer.cornerRadius = 10.0
    }
    
    @objc public func SetupView(title: String, Notes: String, image: String, enable refresh: Bool) {
        self.TitleLBL.text = title
        self.TitleLBL.theme_textColor = GlobalPicker.textColor
        self.TitleLBL.font = Font().MontserratSemiBoldFont(font: 25)
        self.DetailLBL.text = Notes
        self.DetailLBL.theme_textColor = GlobalPicker.textColor
        if image.count == 0 {
            self.Img.image = UIImage.init(named: "Nodatafound")
            self.Img.isHidden = true
        }
        else{
            self.Img.image = UIImage.init(named: image)
            self.Img.isHidden = false
        }
        self.RefreshBTN.isHidden = refresh
    }
    
    @IBAction func RefreshBTN(_ sender: Any) {
        if self.didActionBlock != nil {
            self.didActionBlock!()
        }
    }
    
}
