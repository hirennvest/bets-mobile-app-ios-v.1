//
//  LoginSplashVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class LoginSplashVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var LogoIMG: UIImageView!
    @IBOutlet weak var SigninBTN: UIButton!
    @IBOutlet weak var LBL1: UILabel!
    @IBOutlet weak var RegisterBTN: UIButton!
    
    //    MARK:- Variable define
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        
        self.LogoIMG.tintColor = AppDarkColor
        
        self.SigninBTN.clipsToBounds = true
        self.SigninBTN.backgroundColor = .clear
        self.SigninBTN.layer.borderWidth = 1
        self.SigninBTN.layer.borderColor = AppDarkColor.cgColor
        self.SigninBTN.setTitleColor(AppDarkColor, for: .normal)
        self.SigninBTN.setTitle("SIGN WITH GX", for: .normal)
        self.SigninBTN.titleLabel?.font = Font().MontserratBoldFont(font: 14.0)
        self.LBL1.backgroundColor = AppDarkColor
        
        self.RegisterBTN.clipsToBounds = true
        self.RegisterBTN.backgroundColor = AppDarkColor
        self.RegisterBTN.setTitleColor(.white, for: .normal)
        self.RegisterBTN.setTitle("CREATE", for: .normal)
        self.RegisterBTN.titleLabel?.font = Font().MontserratBoldFont(font: 14.0)
        
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedSignin(_ sender: Any) {
        let vc = LoginVC.init(nibName: "LoginVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func TappedCreate(_ sender: Any) {
        let vc = RegistrationVC.init(nibName: "RegistrationVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
