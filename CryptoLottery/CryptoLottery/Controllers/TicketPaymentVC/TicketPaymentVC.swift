//
//  TicketPaymentVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications


class TicketPaymentVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var TopBG: UIView!
    
    @IBOutlet weak var TPpaymentTBL: UITableView!
    
    @IBOutlet weak var BottomBG: UIView!
    @IBOutlet weak var BackBTN: UIButton!
    @IBOutlet weak var TPProceedBTN: UIButton!
    
    //    MARK:- Variable Define
    
    
    // TODO: Previous Screen selected Datas
    var SelectedTicket_Dict: NSMutableDictionary!
    var ticket_array: [String]!
    var TicketPrice: Double!
    var price: Double!
    var CoinData: CoinListPayload!
    var activeLotteries: LotteryActiveLottery!
    var isUSDFormat: Bool = false
    
    var isPaymentConfirm: Bool = false
    
    var Currenydata = [staticpaymentCoin]()
    
    var betVault_arry: [BetsVaultVault]!
    
    var SelectPay: Int = 0
    
    lazy var nodatafound: NodatafoundView = {
        () -> NodatafoundView in
        let bounds = self.TPpaymentTBL.frame
        let nodata = NodatafoundView.init(frame: bounds)
        nodata.SetupView(title: "No data available!", Notes: "There are no Data available yet!", image: "", enable: true)
        self.view.addSubview(nodata)
        return nodata
    }()
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.GetFundVault()
        self.TopBG.backgroundColor = AppDarkColor
        
        self.BottomBG.clipsToBounds = true
        self.BottomBG.layer.cornerRadius = 15
        self.BottomBG.backgroundColor = AppDarkColor
        
        let maskPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.BackBTN.frame.width, height: self.BackBTN.frame.height),
                                    byRoundingCorners: [.bottomLeft, .topLeft],
                                    cornerRadii: CGSize(width: 15.0, height: 0.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.BackBTN.layer.mask = maskLayer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.isPaymentConfirm {
            self.TPProceedBTN.setTitle("Confirm", for: .normal)
        }
        else {
            self.TPProceedBTN.setTitle("Proceed", for: .normal)
        }
        
        self.TPpaymentTBL.register(UINib.init(nibName: "TPHeaderCell", bundle: nil), forCellReuseIdentifier: "TPHeaderCell")
        self.TPpaymentTBL.register(UINib.init(nibName: "TPCoinCell", bundle: nil), forCellReuseIdentifier: "TPCoinCell")
        self.TPpaymentTBL.register(UINib.init(nibName: "TPSummaryCell", bundle: nil), forCellReuseIdentifier: "TPSummaryCell")
        
        self.nodatafound.isHidden = false
        self.nodatafound.didActionBlock = {
            self.GetFundVault()
        }
        self.TPpaymentTBL.isHidden = true
        
    }
    
    //    MARK:- User Define Methods
    
    //    MARK:- Api Calling
    
    func GetFundVault() {
        background {
            let balanceobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? BetsVaultPayload
            if balanceobj == nil {
                ApiLoader.shared.showHUD()
                let param = FundVaultParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
                let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
                NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        if status {
                            let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject as NSDictionary)
                            UserInfoData.shared.SaveObjectdata(data: betsvault as AnyObject, forkey: Vault_Balancelist)
                            self.betVault_arry = betsvault.payload.vault
                            
                            for item in self.betVault_arry {
                                if item.coin.uppercased() == "USD".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: item.coin, coin: item.coin, color: UIColor.colorWithHexString(hexStr: "B22234"), image: "USD", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "BTC".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Bitcoin", coin: "BTC", color: UIColor.colorWithHexString(hexStr: "F79F1A"), image: "Bitcoin", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "ETH".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Etherum", coin: "ETH", color: UIColor.colorWithHexString(hexStr: "3C3C3D"), image: "Cubs", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "USDT".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Tether", coin: "USDT", color: UIColor.colorWithHexString(hexStr: "1BA27A"), image: "Tether", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                            }
                            
                            if self.Currenydata.count == 0 {
                                self.nodatafound.isHidden = false
                                self.TPpaymentTBL.isHidden = true
                            }
                            else {
                                self.nodatafound.isHidden = true
                                self.TPpaymentTBL.isHidden = false
                            }
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.GetFundVault()
                            })
                        }
                        ApiLoader.shared.stopHUD()
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            
                        })
                        ApiLoader.shared.stopHUD()
                    }
                }
            }
            else {
                main {
                    self.betVault_arry = balanceobj!.vault
                    
                    for item in self.betVault_arry {
                        if item.coin.uppercased() == "USD".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: item.coin, coin: item.coin, color: UIColor.colorWithHexString(hexStr: "B22234"), image: "USD", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "BTC".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Bitcoin", coin: "BTC", color: UIColor.colorWithHexString(hexStr: "F79F1A"), image: "Bitcoin", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "ETH".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Etherum", coin: "ETH", color: UIColor.colorWithHexString(hexStr: "3C3C3D"), image: "Cubs", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "USDT".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Tether", coin: "USDT", color: UIColor.colorWithHexString(hexStr: "1BA27A"), image: "Tether", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                    }
                    
                    if self.Currenydata.count == 0 {
                        self.nodatafound.isHidden = false
                        self.TPpaymentTBL.isHidden = true
                    }
                    else {
                        self.nodatafound.isHidden = true
                        self.TPpaymentTBL.isHidden = false
                    }
                }
            }
            
            main {
                if self.Currenydata.count != 0 {
                    
                    self.ticket_array = self.SelectedTicket_Dict.value(forKey: Buy_Tickets_Count) as? [String]
                    self.CoinData = self.SelectedTicket_Dict.value(forKey: Selected_Coin) as? CoinListPayload
                    self.activeLotteries = self.SelectedTicket_Dict.value(forKey: SelectedLottery) as? LotteryActiveLottery
                    self.TicketPrice = self.SelectedTicket_Dict.value(forKey: Buy_Price) as? Double
                    self.price = self.SelectedTicket_Dict.value(forKey: Buy_Price) as? Double
                    
                    let selectedBal: staticpaymentCoin = self.SelectedTicket_Dict.value(forKey: Buy_Vault) as! staticpaymentCoin
                    self.SelectPay = self.getcoinIndex(coin: selectedBal.coin.uppercased())
                    self.TPpaymentTBL.delegate = self
                    self.TPpaymentTBL.dataSource = self
                    self.TPpaymentTBL.reloadData()
                }
            }
        }
    }

    func getcoinIndex(coin: String) -> Int {
        for (index, item) in self.Currenydata.enumerated() {
            debugPrint("\(String(describing: item.coin)) == \(coin)")
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
        }
        return 0
    }
    
    func TappedSeletedCoinBTN(_ sender: Int) {
        if self.CoinData.coin.uppercased() != "USD" {
            if sender == 0 {
                self.isUSDFormat = true
            }
            else {
                self.isUSDFormat = false
            }
            self.TPpaymentTBL.reloadData()
        }
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func BackBTN(_ sender: Any) {
        if self.isPaymentConfirm {
            self.isUSDFormat = false
            self.isPaymentConfirm = false
            self.TPProceedBTN.setTitle("Proceed", for: .normal)
            self.TPpaymentTBL.reloadData()
        }
        else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func TappedTPProceedBTN(_ sender: Any) {
        if self.isPaymentConfirm {
            let loader = LuckLoaderVC.init(nibName: "LuckLoaderVC", bundle: nil)
            loader.SelectedTicket_Dict = self.SelectedTicket_Dict
            self.navigationController?.pushViewController(loader, animated: true)
            self.isPaymentConfirm = false
            self.TPProceedBTN.setTitle("Proceed", for: .normal)
        }
        else {
            self.isUSDFormat = false
            self.isPaymentConfirm = true
            self.TPProceedBTN.setTitle("Confirm", for: .normal)
        }
        self.TPpaymentTBL.reloadData()
    }
    
}

//MARK:- Tableview Delegate
extension TicketPaymentVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isPaymentConfirm {
            return 1
        }
        else {
            return section == 0 ? 1 : 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell: TPHeaderCell = tableView.dequeueReusableCell(withIdentifier: "TPHeaderCell") as! TPHeaderCell
            DispatchQueue.main.async {
                cell.TitleLBL.text = "Pick Payment Vault"
                
                cell.PaymentCollection.register(UINib.init(nibName: "TPPaymentCell", bundle: nil), forCellWithReuseIdentifier: "TPPaymentCell")
                cell.PaymentCollection.translatesAutoresizingMaskIntoConstraints = false
                
                cell.PaymentCollection.isUserInteractionEnabled = !self.isPaymentConfirm
                cell.PaymentCollection.isScrollEnabled = !self.isPaymentConfirm
                cell.PaymentCollection.delegate = self
                cell.PaymentCollection.dataSource = self
                cell.PaymentCollection.reloadData()
                cell.PaymentCollection.scrollToItem(at: IndexPath.init(row: self.SelectPay, section: 0), at: .centeredHorizontally, animated: true)
            }
            return cell
        }
        else {
            let count = Double(self.ticket_array.count)
            
            if self.isPaymentConfirm {
                let cell: TPSummaryCell = tableView.dequeueReusableCell(withIdentifier: "TPSummaryCell") as! TPSummaryCell
                
                let vault = self.Currenydata[self.SelectPay]
                
                cell.lotteryname.text = self.CoinData.name
                cell.ticketcost.text = String.init(format: "%f", self.TicketPrice!).CoinPriceThumbRules(Coin: self.CoinData.coin)
                cell.ticketcount.text = String.init(format: "%0.0f", count as CVarArg)
                cell.totalcost.text = String.init(format: "%f", (self.TicketPrice! * count)).CoinPriceThumbRules(Coin: self.CoinData.coin)
                cell.redemdate.text = AppUtillity.shared.StringDateformat(date: Date(), format: "MMM d, yyyy")
                
                let obj = self.Currenydata[self.getcoinIndex(coin: self.CoinData.coin)]
                
                CommanApiCall().ForexConversion(Buy: obj.coin, From: "USD") { (keyvalue1, keyvalue2) in
                    let price = (self.price! * keyvalue2)
                    cell.ticketcost2.text = String.init(format: "%f", price).CoinPriceThumbRules(Coin: vault.coin)
                }
                
                CommanApiCall().ForexConversion(Buy: obj.coin, From: vault.coin) { (key_value1, key_value2) in
                    cell.totalcost2.text = String.init(format: "%f", (key_value2 * self.price!) * count).CoinPriceThumbRules(Coin: vault.coin)
                }
                
                return cell
            }
            else {
                let cell: TPCoinCell = tableView.dequeueReusableCell(withIdentifier: "TPCoinCell") as! TPCoinCell
                if indexPath.row == 0 {
                    let obj = self.Currenydata[self.getcoinIndex(coin: self.CoinData.coin)]
                    if isUSDFormat {
                        CommanApiCall().ForexConversion(Buy: obj.coin, From: "USD") { (keyvalue1, keyvalue2) in
                            cell.CoinDetail.text = String.init(format: "%f", (self.price! * keyvalue2 * count)).CoinPriceThumbRules(Coin: "USD")
                        }
                        cell.SeletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
                        cell.UnseletedCoinBTN.setTitle(self.CoinData.coin.uppercased(), for: .normal)
                        cell.UnseletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.SeletedCoinBTN.setTitle("USD", for: .normal)
                    }
                    else {
                        cell.CoinDetail.text = String.init(format: "%f", (count * self.price!)).CoinPriceThumbRules(Coin: self.CoinData.coin)
                        cell.SeletedCoinBTN.setTitle("USD", for: .normal)
                        cell.UnseletedCoinBTN.setTitle(self.CoinData.coin.uppercased(), for: .normal)
                        cell.SeletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
                        cell.UnseletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
                    }
                    cell.CoinSTitle.text = String.init(format: "Cost of %0.0f Tickets", count as CVarArg)
                    
                    cell.CoinName.text = self.CoinData.name
                    cell.CoinConverterView.isHidden = false
                    cell.didConverterActionBlock = { (index) in
                        self.TappedSeletedCoinBTN(index)
                    }
                }
                else {
                    let data = self.Currenydata[self.SelectPay]
                    cell.CoinSTitle.text = String.init(format: "Your %@ Bets Vault Will Be Debits", data.coin)
                    cell.CoinName.text = data.coin
                    cell.IMGCoin.image = UIImage.init(named: data.image)
                    cell.CoinConverterView.isHidden = true
                    
                    let obj = self.Currenydata[self.getcoinIndex(coin: self.CoinData.coin)]
                    CommanApiCall().ForexConversion(Buy: obj.coin, From: data.coin) { (key_value1, key_value2) in
                        cell.CoinDetail.text = String.init(format: "%f", (key_value2 * self.price!)).CoinPriceThumbRules(Coin: data.coin)
                    }
                }
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.isPaymentConfirm {
            return self.SetupTBlHeader(title: "Checkout Summary")
        }
        else {
            return self.SetupTBlHeader(title: "Checkout Details")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //    MARK:- Cell Config
    
    func SetupTBlHeader(title: String) -> UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        view.backgroundColor = .white
        let lbl = UILabel.init(frame: view.bounds)
        lbl.textAlignment = .center
        lbl.text = title
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textColor = AppDarkColor
        view.addSubview(lbl)
        return view
    }
    
}

extension TicketPaymentVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Currenydata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TPPaymentCell",for: indexPath) as? TPPaymentCell else { fatalError() }
        
        let data = self.Currenydata[indexPath.row]
        cell.TPImage.image = UIImage.init(named: data.image)
        cell.TPImage.tintColor = UIColor.white
        cell.TPTitle.text = data.coin
        cell.TPPaymentView.backgroundColor = data.color
        cell.contentView.alpha = self.SelectPay == indexPath.row ? 1.0 : 0.4
        if isUSDFormat {
            CommanApiCall().ForexConversion(Buy: data.coin, From: "USD") { (keyvalue1, keyvalue2) in
                cell.TPDetail.text = String.init(format: "%f", data.nativeField * keyvalue2).WithoutCoinPriceThumbRules(Coin: "USD")
            }
        }
        else {
            cell.TPDetail.text = String.init(format: "%f", data.nativeField).WithoutCoinPriceThumbRules(Coin: data.coin)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isPaymentConfirm {
            
        }
        else
        {
            self.SelectPay = indexPath.row
            collectionView.reloadData()
            self.TPpaymentTBL.reloadData()
            collectionView.scrollToItem(at: IndexPath.init(row: self.SelectPay, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 1.5, height: collectionView.frame.height)
    }
    
}
