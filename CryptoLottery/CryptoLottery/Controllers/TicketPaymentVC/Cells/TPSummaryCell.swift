//
//  TPSummaryCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TPSummaryCell: UITableViewCell {

    @IBOutlet weak var lottery: UILabel!
    @IBOutlet weak var lotteryname: UILabel!
    
    @IBOutlet weak var ticket: UILabel!
    @IBOutlet weak var ticketcost: UILabel!
    @IBOutlet weak var ticketcost2: UILabel!
    
    @IBOutlet weak var ticketpurchase: UILabel!
    @IBOutlet weak var ticketcount: UILabel!
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var totalcost: UILabel!
    @IBOutlet weak var totalcost2: UILabel!
    
    @IBOutlet weak var redem: UILabel!
    @IBOutlet weak var redemdate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lottery.theme_textColor = GlobalPicker.textColor
        self.lotteryname.theme_textColor = GlobalPicker.textColor
        self.ticket.theme_textColor = GlobalPicker.textColor
        self.ticketcost.theme_textColor = GlobalPicker.textColor
        self.ticketpurchase.theme_textColor = GlobalPicker.textColor
        self.ticketcount.theme_textColor = GlobalPicker.textColor
        self.total.theme_textColor = GlobalPicker.textColor
        self.totalcost.theme_textColor = GlobalPicker.textColor
        self.redem.theme_textColor = GlobalPicker.textColor
        self.redemdate.theme_textColor = GlobalPicker.textColor
        
        self.lottery.text = "Native Currency Of Lottery"
        self.ticket.text = "Cost Of One Ticket"
        self.ticketpurchase.text = "Total Tickets Purchased"
        self.total.text = "Total Cost"
        self.redem.text = "Redemption Date"
        
        self.totalcost2.text = ""
        self.ticketcost2.text = ""
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
