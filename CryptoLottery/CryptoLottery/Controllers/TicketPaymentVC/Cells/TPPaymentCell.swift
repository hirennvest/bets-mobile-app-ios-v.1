//
//  TPPaymentCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TPPaymentCell: UICollectionViewCell {

    @IBOutlet weak var TPPaymentView: UIView!
    @IBOutlet weak var TPImage: UIImageView!
    @IBOutlet weak var TPTitle: UILabel!
    @IBOutlet weak var TPDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.TPPaymentView.clipsToBounds = true
        self.TPPaymentView.layer.cornerRadius = 15
        self.TPPaymentView.layer.borderWidth = 1.0
        self.TPPaymentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "186AB4").withAlphaComponent(0.25).cgColor
        self.TPImage.tintColor = UIColor.white
    }

}
