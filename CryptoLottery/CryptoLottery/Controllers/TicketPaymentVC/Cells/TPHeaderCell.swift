//
//  TPHeaderCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TPHeaderCell: UITableViewCell {

    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var PaymentCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.TitleLBL.theme_textColor = GlobalPicker.textColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
