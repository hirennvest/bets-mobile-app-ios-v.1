//
//  PSOptionCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PSOptionCell: UITableViewCell {

    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var CardIMG: UIImageView!
    @IBOutlet weak var NameLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            
        self.cardview.clipsToBounds = true
        self.cardview.layer.cornerRadius = 20
        self.cardview.backgroundColor = AppDarkColor
        self.CardIMG.tintColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
