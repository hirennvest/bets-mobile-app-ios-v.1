//
//  PSHeaderCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PSHeaderCell: UITableViewCell {
    
    @IBOutlet weak var CongretsLBL: UILabel!
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var IntervalStack: UIStackView!
    @IBOutlet weak var Dayview: UIView!
    @IBOutlet weak var DayValueLBL: UILabel!
    @IBOutlet weak var DayLBL: UILabel!
    
    @IBOutlet weak var Houreview: UIView!
    @IBOutlet weak var HoureValueLBL: UILabel!
    @IBOutlet weak var HoureLBL: UILabel!
    
    @IBOutlet weak var Minuteview: UIView!
    @IBOutlet weak var MinuteValueLBL: UILabel!
    @IBOutlet weak var MinuteLBL: UILabel!
    
    @IBOutlet weak var Secondview: UIView!
    @IBOutlet weak var SecondValueLBL: UILabel!
    @IBOutlet weak var SecondLBL: UILabel!
    
    @IBOutlet weak var MilliSecondview: UIView!
    @IBOutlet weak var MilliSecondValueLBL: UILabel!
    @IBOutlet weak var MilliSecondLBL: UILabel!
    
    var timestamp: Double!
    lazy var currentTime: Double = {
        let time = NSDate().timeIntervalSince1970
        return time
    }()
    var timer: Timer!
    var MLtimer: Timer!
    var MLstartTime: Double = 0
    var MLtime: Double = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
        if self.MLtimer != nil {
            self.MLtimer.invalidate()
            self.MLtimer = nil
        }
        self.currentTime = NSDate().timeIntervalSince1970
        
        self.DayValueLBL.text = String.init(format: "00")
        self.HoureValueLBL.text = String.init(format: "00")
        self.MinuteValueLBL.text = String.init(format: "00")
        self.SecondValueLBL.text = String.init(format: "00")
        self.MilliSecondValueLBL.text = String.init(format: "00")
        
        self.DayValueLBL.theme_textColor = GlobalPicker.textColor
        self.DayLBL.theme_textColor = GlobalPicker.textColor
        self.DayLBL.text = "Days"
        
        self.HoureValueLBL.theme_textColor = GlobalPicker.textColor
        self.HoureLBL.theme_textColor = GlobalPicker.textColor
        self.HoureLBL.text = "Hours"
        
        self.MinuteValueLBL.theme_textColor = GlobalPicker.textColor
        self.MinuteLBL.theme_textColor = GlobalPicker.textColor
        self.MinuteLBL.text = "Minutes"
        
        self.SecondValueLBL.theme_textColor = GlobalPicker.textColor
        self.SecondLBL.theme_textColor = GlobalPicker.textColor
        self.SecondLBL.text = "Seconds"
        
        self.MilliSecondValueLBL.theme_textColor = GlobalPicker.textColor
        self.MilliSecondLBL.theme_textColor = GlobalPicker.textColor
        self.MilliSecondLBL.text = "Milliseconds"
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .FeedTimer, object: nil)
        
    }
    
    func stopTimer() {
        DispatchQueue.main.async {
            if self.timer != nil {
                self.timer.invalidate()
                self.timer = nil
            }
            if self.MLtimer != nil {
                self.MLtimer.invalidate()
                self.MLtimer = nil
            }
            self.MilliSecondValueLBL.text = String.init(format: "00")
        }
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.stopTimer()
    }
    
    func UpdateTimer(stamp: Double) {
        DispatchQueue.main.async {
            self.timestamp = stamp / 1000
            let final:Double = self.timestamp - self.currentTime
            if final != 0 || final < 0 || self.timestamp > self.currentTime  {
                self.timer = Timer.scheduledTimer(timeInterval: 1.0,
                                                  target: self,
                                                  selector: #selector(self.Timerupdate),
                                                  userInfo: nil,
                                                  repeats: true)
                
                RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
                self.timer.fire()
            }
            
            print(Date().offset(from: Date(timeIntervalSinceNow: self.timestamp)))
        }
    }
    
    @objc func advanceTimer() {
        DispatchQueue.main.async {
            if self.timestamp == 0 || self.timestamp <= 0 || self.timestamp < self.currentTime {
                self.stopTimer()
                self.DayValueLBL.text = String.init(format: "00")
                self.HoureValueLBL.text = String.init(format: "00")
                self.MinuteValueLBL.text = String.init(format: "00")
                self.SecondValueLBL.text = String.init(format: "00")
                self.MilliSecondValueLBL.text = String.init(format: "00")
            }
            else {
                self.MLtime = Date().timeIntervalSinceReferenceDate - self.MLstartTime
                let timeString = String(format: "%2.2f", self.MLtime)
                let arry = timeString.components(separatedBy: ".")
                let intArray = arry.map { Int($0)!}
                let decimal = String(format: "%02i", Int(intArray.last!))
                
                self.MilliSecondValueLBL.text = decimal
            }
        }
    }
    
    @objc func Timerupdate() {
        DispatchQueue.main.async {
            let final:Double = self.timestamp - self.currentTime
            if self.timestamp == 0 || self.timestamp <= 0 || self.timestamp < self.currentTime {
                self.stopTimer()
                self.DayValueLBL.text = String.init(format: "00")
                self.HoureValueLBL.text = String.init(format: "00")
                self.MinuteValueLBL.text = String.init(format: "00")
                self.SecondValueLBL.text = String.init(format: "00")
                self.MilliSecondValueLBL.text = String.init(format: "00")
            }
            else {
                self.timestamp -= 1
                let time = final.secondsToHoursMinutesSeconds()
                
                self.MLstartTime = Date().timeIntervalSinceReferenceDate
                self.MLtimer = Timer.scheduledTimer(timeInterval: 0.005,
                                                    target: self,
                                                    selector: #selector(self.advanceTimer),
                                                    userInfo: nil,
                                                    repeats: true)
                
                self.DayValueLBL.text = String.init(format: "%02i", time.0!)
                self.HoureValueLBL.text = String.init(format: "%02i", time.1!)
                self.MinuteValueLBL.text = String.init(format: "%02i", time.2!)
                self.SecondValueLBL.text = String.init(format: "%02i", time.3!)
                self.MilliSecondValueLBL.text = String.init(format: "%02i", time.4!)
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
