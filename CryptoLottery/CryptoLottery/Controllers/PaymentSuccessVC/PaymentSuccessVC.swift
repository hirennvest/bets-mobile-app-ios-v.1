//
//  PaymentSuccessVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-21.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PaymentSuccessVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var TopBG: UIView!
    
    @IBOutlet weak var successTBL: UITableView!
    
    //    MARK:- Variable Define
    
    var isPaymentConfirm: Bool = false
    var SelectedTicket_Dict: NSMutableDictionary!
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.TopBG.backgroundColor = AppDarkColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: .ReloadFeed, object: nil)
        NotificationCenter.default.post(name: .FeedTimer, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.successTBL.register(UINib.init(nibName: "PSHeaderCell", bundle: nil), forCellReuseIdentifier: "PSHeaderCell")
        self.successTBL.register(UINib.init(nibName: "PSOptionCell", bundle: nil), forCellReuseIdentifier: "PSOptionCell")
        
        self.successTBL.delegate = self
        self.successTBL.dataSource = self
        self.successTBL.reloadData()
    }
    
    //    MARK:- User Define Methods
    
    //    MARK:- Api Calling
    
    //    MARK:- IBAction Methods
    
}

//MARK:- Tableview Delegate
extension PaymentSuccessVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PSOptionCell = tableView.dequeueReusableCell(withIdentifier: "PSOptionCell") as! PSOptionCell
        cell.CardIMG.tintColor = .white
        if indexPath.row == 0 {
            cell.CardIMG.image = UIImage.init(named: "Lottery_Cub")
            cell.NameLBL.text = "Explore More Lotteries"
        }
        else if indexPath.row == 1 {
            cell.CardIMG.image = UIImage.init(named: "BetsGraph")
            cell.NameLBL.text = "Go To Bets Dashboard"
        }
        else {
            cell.CardIMG.image = UIImage.init(named: "Firebets")
            cell.NameLBL.text = "Share & Get Free Tickets"
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 255
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell: PSHeaderCell = tableView.dequeueReusableCell(withIdentifier: "PSHeaderCell") as! PSHeaderCell
        self.updatecell(cell: cell, index: section)
        return cell
    }
    
    func updatecell(cell: PSHeaderCell, index: Int) {
        guard let selectedlottery = self.SelectedTicket_Dict.object(forKey: SelectedLottery) else {
            return
        }
        let lottery: LotteryActiveLottery = selectedlottery as! LotteryActiveLottery
        let LuckyNo_arry = self.SelectedTicket_Dict.value(forKey: Buy_Tickets_Count) as? [String]
        let count = Double(LuckyNo_arry!.count)
        cell.TitleLBL.text = String.init(format: "You Have Secured %0.0f Tickets", count as CVarArg)
        cell.UpdateTimer(stamp: Double(lottery.endTimestamp))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else if indexPath.row == 1 {
            self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: .ReloadFeed, object: nil, userInfo: ["Tab": 2])
        }
        else {
            
        }
    }
    
    //    MARK:- Cell Config
    
}
