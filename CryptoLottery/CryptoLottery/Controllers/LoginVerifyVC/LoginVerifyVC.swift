//
//  LoginVerifyVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import Lottie
import SVPinView

class LoginVerifyVC: BaseVC {
    
    // MARK:- IBOutlet Define
    
    @IBOutlet weak var SuccessView: UIView!
    @IBOutlet weak var LogoIMG: UIImageView!
    @IBOutlet weak var Progress: UIView!
    @IBOutlet weak var VerifyLBL: UILabel!
    
    @IBOutlet weak var FailView: UIView!
    @IBOutlet weak var FailIMG: UIImageView!
    @IBOutlet weak var FailLBL: UILabel!
    
    @IBOutlet weak var AuthenticationView: UIView!
    @IBOutlet weak var AuthTitle_LBL: UILabel!
    @IBOutlet weak var OTPView: UIView!
    @IBOutlet weak var Auth_AccessBTN: UIButton!
    
    
    // MARK:- Variable define
    var pinView: SVPinView!
    
    var username: String = ""
    var Email: String = ""
    var passowrd: String = ""
    
    var ComeFrom: String = "Login"
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ApiLoader.shared.showHUD()
        
        self.Progress.clipsToBounds = true
        self.Progress.layer.cornerRadius = 5
        //        self.Progress.layer.borderColor = AppDarkColor.cgColor
        //        self.Progress.layer.borderWidth = 1
        self.Progress.backgroundColor = .clear
        
        self.LogoIMG.isHidden = true
        self.FailView.isHidden = true
        self.SuccessView.isHidden = false
        self.AuthenticationView.isHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.VerifyLBL.text = ""
        if self.ComeFrom.uppercased() == "Login".uppercased() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.LoginApiCalling()
            }
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.SignUpApiCalling()
            }
        }
    }
    
    func setupAuthentication() {
        
        ApiLoader.shared.stopHUD()
        self.AuthenticationView.isHidden = false
        self.SuccessView.isHidden = true
        self.FailView.isHidden = true
        
        self.pinView = SVPinView.init(frame: CGRect(x: 0, y: 0, width: self.OTPView.frame.width, height: self.OTPView.frame.height))
        self.pinView.pinLength = 6
        self.pinView.interSpace = 5
        self.pinView.textColor = AppDarkColor
        self.pinView.shouldSecureText = true
        self.pinView.style = .box
        self.pinView.borderLineColor = AppDarkColor.withAlphaComponent(0.4)
        self.pinView.activeBorderLineColor = AppDarkColor
        self.pinView.borderLineThickness = 1
        self.pinView.activeBorderLineThickness = 3
        self.pinView.font = UIFont.systemFont(ofSize: 15)
        self.pinView.keyboardType = .phonePad
        self.pinView.placeholder = "000000"
        self.pinView.becomeFirstResponderAtIndex = 0
        self.pinView.didFinishCallback = { pin in
            print("The pin entered is \(pin)")
            self.AuthenticationView.isHidden = true
            self.SuccessView.isHidden = true
            self.FailView.isHidden = true
            if self.ComeFrom.uppercased() == "Login".uppercased() {
                self.LoginwithAuth(pin: pin)
            }
            else {
                
            }
        }
        self.pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItem.Style.done, target: self, action: #selector(DoneBTNAction))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        self.OTPView.addSubview(self.pinView)
        self.AuthTitle_LBL.text = "Enter The Six Digit Reset Code"
        self.AuthTitle_LBL.font = Font().MontserratRegularFont(font: 20)
        self.Auth_AccessBTN.setTitle("I Can’t Access My 2FA", for: .normal)
        self.Auth_AccessBTN.titleLabel?.font = Font().MontserratRegularFont(font: 14)
    }
    
    func ProgressAnimation() {
        self.LogoIMG.isHidden = true
        self.FailView.isHidden = true
        self.AuthenticationView.isHidden = true
        self.SuccessView.isHidden = false
        DispatchQueue.main.async {
            self.LogoIMG.tintColor = AppDarkColor
            self.VerifyLBL.textColor = AppDarkColor
            self.VerifyLBL.text = "VERIFYING"
            
            let lottie = AnimationView(name: "ProgressView")
            lottie.contentMode = .scaleAspectFit
            lottie.clipsToBounds = true
            lottie.loopMode = .loop
            self.Progress.addSubview(lottie)
            lottie.translatesAutoresizingMaskIntoConstraints = false
            lottie.leadingAnchor.constraint(equalTo: self.Progress.leadingAnchor, constant: 0).isActive = true
            lottie.trailingAnchor.constraint(equalTo: self.Progress.trailingAnchor, constant: 0).isActive = true
            lottie.centerXAnchor.constraint(equalTo: self.Progress.centerXAnchor, constant: 0).isActive = true
            lottie.heightAnchor.constraint(equalToConstant: self.Progress.frame.size.height).isActive = true
            lottie.widthAnchor.constraint(equalToConstant: self.Progress.frame.size.width).isActive = true
            lottie.play()
        }
    }
    
    func FailViewSet() {
        self.FailView.isHidden = false
        self.SuccessView.isHidden = true
        self.AuthenticationView.isHidden = true
        
        self.LogoIMG.image = UIImage.init(named: "LoginFail")
        
        self.LogoIMG.tintColor = AppDarkColor
        self.FailLBL.textColor = AppDarkColor
        self.FailLBL.font = Font().MontserratRegularFont(font: 20)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    // MARK:- Uder Define methods
    
    // MARK:- API Calling
    
    func LoginwithAuth(pin: String) {
        background {
            ApiLoader.shared.showHUD()
            let paramDict = AuthLoginParamDict.init(ids: self.Email.removeWhiteSpace(), password: self.passowrd.removeWhiteSpace(), totp_code: pin)
            
            NetworkingRequests.shared.requestPOST(API_Login, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if UserInfoData.shared.SaveUserInfodata(info: userinfo) {
                            self.Getuserdata()
                        }
                    }
                    else {
                        ApiLoader.shared.stopHUD()
                        self.FailViewSet()
                        let message = responseObject["message"] as AnyObject
                        self.FailLBL.text = (message as? String)!
                    }
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    self.FailViewSet()
                    self.FailLBL.text = msg
                }
            }
        }
    }
    
    func Getuserdata() {
        background {
            let param = UserdataParamDict.init(email: self.Email.removeWhiteSpace())
            NetworkingRequests.shared.requestsGET(API_GetUserData, Parameters: param.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject) in
                main {
                    let userdata = UserDataRootClass.init(fromDictionary: responseObject)
                    if userdata.status {
                        if UserInfoData.shared.SaveUserdata(info: userdata) {
                            UserInfoData.shared.SaveUserEmail(email: self.Email.removeWhiteSpace())
                            self.RegisterBets()
//                            App?.SetDashboardVC(index: 0)
//                            ApiLoader.shared.stopHUD()
                        }
                    }
                    else {
                        ApiLoader.shared.stopHUD()
                        self.FailViewSet()
                        let message = responseObject["message"] as AnyObject
                        self.FailLBL.text = (message as? String)!
                    }
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    self.FailViewSet()
                    self.FailLBL.text = msg
                }
            }
        }
    }
    
    func SignUpApiCalling() {
        background {
            let paramDict = SignupParamDict.init(username: self.username, email: self.Email.removeWhiteSpace(), password: self.passowrd.removeWhiteSpace())

            NetworkingRequests.shared.requestPOST(API_Signup, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let vc = OTPVerifyVC.init(nibName: "OTPVerifyVC", bundle: nil)
                        vc.emailAddress = self.Email
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    else {
                        ApiLoader.shared.stopHUD()
                        self.FailViewSet()
                        let message = responseObject["message"] as AnyObject
                        self.FailLBL.text = (message as? String)!
                    }
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    self.FailViewSet()
                    self.FailLBL.text = msg
                }
            }
        }
    }
    
    func LoginApiCalling() {
        background {
            let paramDict = LoginParamDict.init(ids: self.Email.removeWhiteSpace(), password: self.passowrd.removeWhiteSpace())
            
            NetworkingRequests.shared.requestPOST(API_Login, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if UserInfoData.shared.SaveUserInfodata(info: userinfo) {
                            self.Getuserdata()
                        }
                    }
                    else {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if userinfo.mfa ?? false {
                            self.setupAuthentication()
                        }
                        else {
                            ApiLoader.shared.stopHUD()
                            self.FailViewSet()
                            let message = responseObject["message"] as AnyObject
                            self.FailLBL.text = (message as? String)!
                        }
                    }
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    self.FailViewSet()
                    self.FailLBL.text = msg
                }
            }
        }
    }
    
    func RegisterBets() {
        background {
            let header = CommanHeader.init(email: self.Email.removeWhiteSpace(), token: UserInfoData.shared.GetUserToken()!)
            let paramDict = RegisterBetsParamDict.init(email: self.Email.removeWhiteSpace())
            
            NetworkingRequests.shared.requestPOST(API_UserBIO, Parameters: paramDict.description, Headers: header.description, onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserBioRootClass.init(fromDictionary: responseObject as NSDictionary)
                        UserInfoData.shared.SaveObjectdata(data: userinfo.payload, forkey: BetsRegister)
                        if userinfo.payload.betsLevel {
                            App?.SetDashboardVC(index: 0)
                            ApiLoader.shared.stopHUD()
                        }
                        else {
//                            let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
//                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else {
//                        let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
//                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }) { (msg, code) in
                main {
                    ApiLoader.shared.stopHUD()
                    self.FailViewSet()
                    self.FailLBL.text = msg
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedAuth_Access(_ sender: Any) {
    }
    
}
