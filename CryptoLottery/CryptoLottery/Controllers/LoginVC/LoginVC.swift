//
//  LoginVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IHKeyboardAvoiding
import CRNotifications

class LoginVC: UIViewController {
    
    //    MARK:- IBOutlet Define
    @IBOutlet weak var Stackview: UIStackView!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var EmailLBL: UILabel!
    @IBOutlet weak var TXTEmail: UITextField!
    @IBOutlet weak var EmailBTN: UIButton!
    @IBOutlet weak var EmailLine: UILabel!
    
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var PasswordLBL: UILabel!
    @IBOutlet weak var TXTPassword: UITextField!
    @IBOutlet weak var PasswordBTN: UIButton!
    @IBOutlet weak var PasswordLine: UILabel!
    
    @IBOutlet weak var ForgotPassBTN: UIButton!
    
    @IBOutlet weak var RegisterBTN: UIButton!
    
    //    MARK:- Variable define
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.view.theme_backgroundColor = GlobalPicker.backgroundColor
        
        self.setupView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        
        DispatchQueue.main.async {
            self.view.theme_backgroundColor = GlobalPicker.backgroundColor
            
            self.TitleLBL.text = "Login"
            self.TitleLBL.font = Font().MontserratBoldFont(font: 40.0)
            self.TitleLBL.theme_textColor = GlobalPicker.textColor
            
            self.EmailLBL.text = "Enter Email"
            self.EmailLBL.font = Font().MontserratBoldFont(font: 14.0)
            self.EmailLBL.textColor = AppDarkColor
            self.TXTEmail.placeholder = "Enter GX Email"
            self.TXTEmail.font = Font().MontserratRegularFont(font: 14.0)
            self.TXTEmail.textColor = AppDarkColor
            self.TXTEmail.delegate = self
            self.EmailBTN.tintColor = AppDarkColor
            
            self.EmailBTN.isEnabled = false
            self.PasswordBTN.isEnabled = false
            
            self.EmailLine.backgroundColor = AppDarkColor
            
            self.PasswordLBL.text = "Enter Password"
            self.PasswordLBL.font = Font().MontserratBoldFont(font: 14.0)
            self.PasswordLBL.textColor = AppDarkColor
            self.TXTPassword.placeholder = "Enter Password"
            self.TXTPassword.font = Font().MontserratRegularFont(font: 14.0)
            self.TXTPassword.textColor = AppDarkColor
            self.TXTPassword.isSecureTextEntry = true
            self.TXTPassword.delegate = self
            self.PasswordBTN.tintColor = AppDarkColor
            
            self.PasswordLine.backgroundColor = AppDarkColor
            
//            self.TXTEmail.text = "muhammad@nvestbank.com"
//            self.TXTPassword.text = "7!y^SNk#PtRfSq"
            
            self.TXTEmail.text = ""
            self.TXTPassword.text = ""
            
            self.EmailView.isHidden = false
            self.PasswordView.isHidden = false
            self.ForgotPassBTN.isHidden = false
            self.RegisterBTN.isHidden = false
            self.TXTEmail.resignFirstResponder()
            self.TXTPassword.resignFirstResponder()
            
            self.TXTEmail.attributedPlaceholder =
                NSAttributedString(string: "Enter GX Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratRegularFont(font: 14.0)])
            self.TXTPassword.attributedPlaceholder =
                NSAttributedString(string: "Enter Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font().MontserratRegularFont(font: 14.0)])
            
            self.ForgotPassBTN.setTitle("Forgot Password?", for: .normal)
            self.ForgotPassBTN.titleLabel?.font = Font().MontserratBoldFont(font: 11.0)
            self.ForgotPassBTN.theme_setTitleColor(GlobalPicker.textColor, forState: .normal)
            self.ForgotPassBTN.setTitleColor(AppDarkColor, for: .normal)
            
            self.RegisterBTN.setTitle("Don’t Have An Account? Click Here", for: .normal)
            self.RegisterBTN.titleLabel?.font = Font().MontserratRegularFont(font: 12.0)
            self.RegisterBTN.theme_setTitleColor(GlobalPicker.textColor, forState: .normal)
            self.RegisterBTN.setTitleColor(AppDarkColor, for: .normal)
            
            self.EmailView.isHidden = false
            self.PasswordView.isHidden = false
            self.ForgotPassBTN.isHidden = false
            self.RegisterBTN.isHidden = false
            
            if self.TXTPassword.text?.count != 0 {
                self.PasswordBTN.alpha = 1.0
                self.PasswordBTN.isUserInteractionEnabled = true
            }
            else {
                self.PasswordBTN.alpha = 0.5
                self.PasswordBTN.isUserInteractionEnabled = false
            }
        }
    }
    
    //    MARK:- API Calling
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedEmailBTN(_ sender: Any) {
        if self.TXTEmail.isValidEmail(testStr: self.TXTEmail.text) {
            self.EmailView.isHidden = true
            self.PasswordView.isHidden = false
            self.TXTPassword.becomeFirstResponder()
        }
    }
    
    @IBAction func TappedPasswordBTN(_ sender: Any) {
        if self.TXTEmail.isValidEmail(testStr: self.TXTEmail.text) || self.TXTPassword.text?.count != 0 {
            let vc = LoginVerifyVC.init(nibName: "LoginVerifyVC", bundle: nil)
            vc.Email = self.TXTEmail.text!
            vc.passowrd = self.TXTPassword.text!
            vc.ComeFrom = "Login"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            self.EmailView.isHidden = false
            self.PasswordView.isHidden = true
            self.TXTEmail.becomeFirstResponder()
        }
    }
    
    @IBAction func TappedForgotPass(_ sender: Any) {
    }
    
    @IBAction func TappedRegister(_ sender: Any) {
        let vc = RegistrationVC.init(nibName: "RegistrationVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
//        if let url = URL(string: "https://setupmywallet.com/") {
//            UIApplication.shared.open(url)
//        }
    }
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.TXTEmail {
            self.EmailView.isHidden = true
            self.PasswordView.isHidden = false
            self.TXTEmail.resignFirstResponder()
            self.TXTPassword.becomeFirstResponder()
        }
        else {
            if self.TXTEmail.isValidEmail(testStr: self.TXTEmail.text) || self.TXTPassword.text?.count != 0 {
                let vc = LoginVerifyVC.init(nibName: "LoginVerifyVC", bundle: nil)
                vc.Email = self.TXTEmail.text!
                vc.passowrd = self.TXTPassword.text!
                vc.ComeFrom = "Login"
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else {
                self.EmailView.isHidden = false
                self.PasswordView.isHidden = true
                self.TXTEmail.becomeFirstResponder()
                self.TXTPassword.resignFirstResponder()
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.TXTEmail {
            self.EmailView.isHidden = false
            self.PasswordView.isHidden = true
        }
        else {
            if self.TXTEmail.isValidEmail(testStr: self.TXTEmail.text) {
                self.EmailView.isHidden = true
                self.PasswordView.isHidden = false
            }
            else {
                self.EmailView.isHidden = false
                self.PasswordView.isHidden = true
            }
        }
        self.ForgotPassBTN.isHidden = true
        self.RegisterBTN.isHidden = true
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.TXTEmail {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            if textField.isValidEmail(testStr: newString as String) {
                self.EmailBTN.alpha = 1.0
                self.EmailBTN.isEnabled = true
                self.EmailBTN.isUserInteractionEnabled = true
            }
            else {
                self.EmailBTN.alpha = 0.5
                self.EmailBTN.isUserInteractionEnabled = false
            }
        }
        if textField == self.TXTPassword {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            if (newString as String).count > 0 {
                self.PasswordBTN.alpha = 1.0
                self.PasswordBTN.isEnabled = true
                self.PasswordBTN.isUserInteractionEnabled = true
            }
            else {
                self.PasswordBTN.alpha = 0.5
                self.PasswordBTN.isUserInteractionEnabled = false
            }
        }
        return true
    }
    
}
