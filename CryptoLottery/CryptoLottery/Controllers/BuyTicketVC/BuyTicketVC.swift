//
//  BuyTicketVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import CRNotifications
import SkeletonView

public var JustDismiss: (() -> Void)?

struct staticpaymentCoin {
    var name: String!
    var coin: String!
    var color: UIColor!
    var image: String!
    var nativeField : Double!
    var usd : Double!
}

class BuyTicketVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var Mainview: CustomView!
    
    @IBOutlet weak var PaymentView: UIView!
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var CoinTicketView: UIView!
    @IBOutlet weak var TicketView: CustomView!
    @IBOutlet weak var SubTicketView: CustomView!
    @IBOutlet var LinesLBL: [CustomLBL]!
    @IBOutlet weak var TicketIconView: CustomView!
    @IBOutlet weak var TicketIconBTN: CustomBTN!
    @IBOutlet weak var EqualLBL: UILabel!
    @IBOutlet weak var PriceLBL: UILabel!
    @IBOutlet weak var CurrencyLBL: UILabel!
    
    @IBOutlet weak var CoinSelectionView: UIView!
    @IBOutlet weak var CoinSelection_height: NSLayoutConstraint!
    @IBOutlet weak var DownArrow: UIImageView!
    @IBOutlet weak var CoinSTitle: UILabel!
    
    @IBOutlet weak var SeletedCoinBTN: UIButton!
    @IBOutlet weak var LineSeprator: UILabel!
    @IBOutlet weak var UnseletedCoinBTN: UIButton!
    
    @IBOutlet weak var ValutView: CustomView!
    @IBOutlet weak var CoinIMG: UIImageView!
    @IBOutlet weak var CoinName: UILabel!
    @IBOutlet weak var CoinDetail: UILabel!
    @IBOutlet weak var CoinBTN: UIButton!
    
    @IBOutlet weak var ValutpopupView: UIView!
    @IBOutlet weak var Vaultlbl: UILabel!
    @IBOutlet weak var ValutCollection: UICollectionView!
    
    @IBOutlet weak var AmountView: UIView!
    @IBOutlet weak var AmoutTitle: UILabel!
    @IBOutlet weak var AmountTotalLBL: UILabel!
    @IBOutlet weak var TicketTotalView: CustomView!
    @IBOutlet weak var TXTValue1: UITextField!
    @IBOutlet weak var LineLBL: UILabel!
    @IBOutlet weak var TXTValue2: UITextField!
    
    @IBOutlet weak var BottomBG: UIView!
    @IBOutlet weak var BackBTN: UIButton!
    @IBOutlet weak var BuyBTN: UIButton!
    
    //    MARK:- Variable Define
    
    var Currenydata = [staticpaymentCoin]()
    var Suggetion_Arry: NSMutableArray!
    var FromCome: String!
    var CoinData: CoinListPayload!
    var typedValue: Int = 0
    
    var CMC_Prices: CMCPrices!
    
    var activeLotteries : LotteryActiveLottery!
    var closedLotteries : LotteryClosedLottery!
    var freezedLotteries : LotteryActiveLottery!
    
    var SelectedTicket_Dict: NSMutableDictionary!
    var TicketPrice: Double = 0.000
    var isUSDFormat: Bool = false
    
    public var TicketDismissWithDictCallBack: ((NSMutableDictionary) -> Void)?
    
    var betVault_arry: [BetsVaultVault]!
    
    lazy var nodatafound: NodatafoundView = {
        () -> NodatafoundView in
        let bounds = self.Mainview.frame
        let nodata = NodatafoundView.init(frame: bounds)
        nodata.SetupView(title: "No data available!", Notes: "There are no Data available yet!", image: "", enable: true)
        self.Mainview.addSubview(nodata)
        return nodata
    }()
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.GetFundVault()
//        self.PaymentView.isHidden = true
//        self.nodatafound.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.LineSeprator.backgroundColor = AppDarkColor
        
        self.SelectedTicket_Dict = NSMutableDictionary.init()
        self.SelectedTicket_Dict.setValue(CoinData, forKey: Selected_Coin)
        self.SelectedTicket_Dict.setValue(self.CurrencyLBL.text, forKey: Buy_Currency)
        
        if self.activeLotteries != nil {
            self.TicketPrice = Double(self.activeLotteries.ticketPrice!)
            self.SelectedTicket_Dict.setValue(self.activeLotteries, forKey: SelectedLottery)
        }
        else if self.closedLotteries != nil {
            self.TicketPrice = Double(self.closedLotteries.ticketPrice) * Double(self.CMC_Prices.usdtPrice)
            self.SelectedTicket_Dict.setValue(self.closedLotteries, forKey: SelectedLottery)
        }
        else {
            self.TicketPrice = Double(self.freezedLotteries.ticketPrice!) * Double(self.CMC_Prices.usdtPrice)
            self.SelectedTicket_Dict.setValue(self.freezedLotteries, forKey: SelectedLottery)
        }
        self.SelectedTicket_Dict.setValue(self.TicketPrice, forKey: Buy_Price)
        
        self.PriceLBL.isSkeletonable = true
        self.CurrencyLBL.isSkeletonable = true
        self.SeletedCoinBTN.isSkeletonable = true
        self.UnseletedCoinBTN.isSkeletonable = true
        self.CoinDetail.isSkeletonable = true
        self.AmountTotalLBL.isSkeletonable = true
        self.TXTValue2.isSkeletonable = true
        
        self.setupUI()
    }
    
    //    MARK:- User Define Methods
    
    func setupUI() {
        
        self.PaymentView.isHidden = false
        self.nodatafound.isHidden = true
        
        if isIphone5 {
            KeyboardAvoiding.avoidingView = self.AmountView
        } else {
            KeyboardAvoiding.avoidingView = self.Mainview
        }
    
        //TODO:- Header Title
        let string1: String = "Ready To Play"
        let string2: String = "\n How Many Tickets Do YouWant To Buy?" as String
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratBoldFont(font: 25.0), AppDarkColor), (string2, Font().MontserratRegularFont(font: 15.0), AppDarkColor)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
        
        self.TitleLBL.attributedText = myMutableString
        
        //TODO:- Ticket Header View Setup
        
        self.UnseletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
        self.UnseletedCoinBTN.setTitle(self.CoinData.coin.uppercased(), for: .normal)
        self.SeletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
        self.SeletedCoinBTN.setTitle("USD", for: .normal)
        
        self.CoinTicketView.backgroundColor = .white
        self.TicketView.BGColor = .clear
        self.SubTicketView.BGColor = .clear
        
        self.SubTicketView.BorderColor = AppDarkColor
        self.SubTicketView.BorderWidth = 3
        for n in 0...self.LinesLBL.count - 1 {
            self.LinesLBL[n].LBL_BGColor = self.LinesLBL[n].tag == 100 ? .white : AppDarkColor
            self.LinesLBL[n].LBL_BorderColor = AppDarkColor
        }
        
        self.TicketIconBTN.imageView?.downloadedFrom(url: URL.init(string: self.CoinData.imageUrl)!)
        
        self.TicketIconView.BGColor = .clear
        self.TicketIconView.BorderColor = AppDarkColor
        self.TicketIconView.BorderWidth = 3
        
        self.TicketIconBTN.backgroundColor = .white
        self.TicketIconBTN.BTN_BorderColor = AppDarkColor
        self.TicketIconBTN.BTN_BorderWidth = 3
        self.TicketIconBTN.tintColor = AppDarkColor
        
        self.EqualLBL.theme_textColor = GlobalPicker.textColor
        self.PriceLBL.theme_textColor = GlobalPicker.textColor
        self.PriceLBL.text = String.init(format: "%f", self.TicketPrice).WithoutCoinPriceThumbRules(Coin: self.CoinData.coin)
        self.PriceLBL.numberOfLines = 1
        self.PriceLBL.minimumScaleFactor = 8 / self.PriceLBL.font.pointSize
        self.PriceLBL.adjustsFontSizeToFitWidth = true
        self.CurrencyLBL.text = self.CoinData.coin
        self.CurrencyLBL.numberOfLines = 1
        self.CurrencyLBL.minimumScaleFactor = 8 / self.CurrencyLBL.font.pointSize
        self.CurrencyLBL.adjustsFontSizeToFitWidth = true
        self.CurrencyLBL.theme_textColor = GlobalPicker.textColor
        
//        TODO:- Vault Design VIew
        self.ValutView.BGColor = .white
        self.ValutView.BorderColor = AppDarkColor
        self.ValutView.BorderWidth = 1
        self.ValutView.CornerRadius = 10
        
        self.CoinName.theme_textColor = GlobalPicker.textColor
        self.CoinDetail.theme_textColor = GlobalPicker.notesColor
        self.CoinSTitle.theme_textColor = GlobalPicker.textColor
        self.CoinSTitle.text = "Select Vault"
        
        self.CoinIMG.backgroundColor = AppDarkColor
        self.CoinIMG.tintColor = UIColor.white
        self.CoinIMG.clipsToBounds = true
        self.CoinIMG.layer.cornerRadius = self.CoinIMG.frame.height / 2
        self.CoinIMG.downloadedFrom(url: URL.init(string: self.CoinData.imageUrl)!)
        
        self.CoinSelection_height.constant = 80
        self.ValutpopupView.isHidden = true
        self.Vaultlbl.backgroundColor = AppDarkColor
        self.ValutCollection.register(UINib.init(nibName: "ValutCurrencyCell", bundle: nil), forCellWithReuseIdentifier: "ValutCurrencyCell")
        self.ValutCollection.translatesAutoresizingMaskIntoConstraints = false
        
//        TODO:- Ticket Ammout and Purchase Ticket View
        self.TicketTotalView.BGColor = .white
        self.TicketTotalView.BorderColor = AppDarkColor
        self.LineLBL.backgroundColor = AppDarkColor
        self.TicketTotalView.BorderWidth = 1
        self.TicketTotalView.CornerRadius = 10
        
        self.AmoutTitle.theme_textColor = GlobalPicker.textColor
        self.AmountTotalLBL.theme_textColor = GlobalPicker.textColor
        
        self.AmoutTitle.text = "Ticket Amount"
        self.AmountTotalLBL.text = self.CurrencyLBL.text! + " Total"
        
        self.TXTValue1.theme_textColor = GlobalPicker.textColor
        self.TXTValue1.delegate = self
        self.TXTValue1.text = "1"
        
        let array = NSMutableArray.init()
        array.add("0")
        self.SelectedTicket_Dict.setValue(array, forKey: Buy_Tickets_Count)
        
        self.TXTValue2.theme_textColor = GlobalPicker.textColor
        if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
            let f = Double(truncating: n)
            let totalprice = self.TicketPrice * f
            self.TXTValue2.text = String.init(format: "%f", totalprice).CoinPriceThumbRules(Coin: self.CoinData.coin)
        }
        
        let btntitle = String.init(format: "%@ %@ %@", "Buy", self.TXTValue1.text!, "Tickets")
        self.BuyBTN.setTitle(btntitle, for: .normal)
        self.BuyBTN.setTitleColor(.white, for: .normal)
        
        self.BottomBG.clipsToBounds = true
        self.BottomBG.layer.cornerRadius = 15
        self.BottomBG.backgroundColor = AppDarkColor
        
        let maskPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.BackBTN.frame.width, height: self.BackBTN.frame.height),
                                    byRoundingCorners: [.bottomLeft, .topLeft],
                                    cornerRadii: CGSize(width: 15.0, height: 0.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.BackBTN.layer.mask = maskLayer
        
        self.Mainview.backgroundColor = .white
    }
    
    func ShowSkeleton() {
//        self.PriceLBL.showGradientSkeleton()
//        self.CurrencyLBL.showGradientSkeleton()
        self.SeletedCoinBTN.showGradientSkeleton()
        self.UnseletedCoinBTN.showGradientSkeleton()
//        self.CoinDetail.showGradientSkeleton()
//        self.AmountTotalLBL.showGradientSkeleton()
//        self.TXTValue2.showGradientSkeleton()
    }
    
    func HideSkeleton() {
//        self.PriceLBL.hideSkeleton()
//        self.CurrencyLBL.hideSkeleton()
        self.SeletedCoinBTN.hideSkeleton()
        self.UnseletedCoinBTN.hideSkeleton()
//        self.CoinDetail.hideSkeleton()
//        self.AmountTotalLBL.hideSkeleton()
//        self.TXTValue2.hideSkeleton()
    }
    
    func getcoinIndex(coin: String) -> Int {
        for (index, item) in self.Currenydata.enumerated() {
            debugPrint("\(String(describing: item.coin)) == \(coin)")
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
            if item.coin.uppercased() == coin.uppercased() {
                return index
            }
        }
        return 0
    }
    
    func setupBetVault(index: Int) {
        DispatchQueue.main.async {
            let obj: staticpaymentCoin = self.Currenydata[index]
            
            self.SelectedTicket_Dict.setValue(obj, forKey: Buy_Vault)
            self.CoinName.text = obj.coin
            self.CoinDetail.text = String.init(format: "%f", obj.nativeField).CoinPriceThumbRules(Coin: obj.coin)
            self.CurrencyLBL.text = obj.coin
            self.UnseletedCoinBTN.setTitle(obj.coin.uppercased(), for: .normal)
            self.AmountTotalLBL.text = obj.coin + " Total"
            let coinimage = UIImage.init(named: obj.image)
            self.CoinIMG.image = coinimage!.resizableImage(withCapInsets: UIEdgeInsets(top: self.CoinIMG.frame.size.height / 10, left: self.CoinIMG.frame.size.height / 10, bottom: self.CoinIMG.frame.size.height / 10, right: self.CoinIMG.frame.size.height / 10), resizingMode: .stretch)
            self.TicketIconBTN.setImage(UIImage.init(named: obj.image), for: .normal)
            if self.isUSDFormat {
                CommanApiCall().ForexConversion(Buy: obj.coin, From: "USD") { (keyvalue1, keyvalue2) in
                    let price = self.activeLotteries.ticketPrice! * keyvalue2
                    self.PriceLBL.text = String.init(format: "%f", price).WithoutCoinPriceThumbRules(Coin: "USD")
                    self.CurrencyLBL.text = "USD"
                    self.AmountTotalLBL.text = "USD" + " Total"
                    self.CoinDetail.text = String.init(format: "%f", (obj.nativeField * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                    if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
                        let f = Double(truncating: n)
                        self.TXTValue2.text = String.init(format: "%f", (price * f)).CoinPriceThumbRules(Coin: "USD")
                    }
                }
            }
            else {
                self.PriceLBL.text = String.init(format: "%f", self.activeLotteries.ticketPrice!).WithoutCoinPriceThumbRules(Coin: obj.coin)
                self.CurrencyLBL.text = obj.coin
                self.AmountTotalLBL.text = obj.coin + " Total"
                self.CoinDetail.text = String.init(format: "%f", obj.nativeField).CoinPriceThumbRules(Coin: obj.coin)
                if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
                    let f = Double(truncating: n)
                    self.TXTValue2.text = String.init(format: "%f", (self.activeLotteries.ticketPrice! * f)).CoinPriceThumbRules(Coin: obj.coin)
                }
            }
            let data = self.Currenydata[index]
            self.SelectedTicket_Dict.setValue(data, forKey: Buy_Vault)
            self.SelectedTicket_Dict.setValue(self.TicketPrice, forKey: Buy_Price)
            self.SelectedTicket_Dict.setValue(self.CurrencyLBL.text, forKey: Buy_Currency)
        }
    }
    
    //    MARK:- Api Calling
    
    func GetFundVault() {
        
        background {
            let balanceobj = UserInfoData.shared.GetObjectdata(key: Vault_Balancelist) as? BetsVaultPayload
            if balanceobj == nil {
                ApiLoader.shared.showHUD()
                let param = FundVaultParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
                let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
                NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                    main {
                        if status {
                            let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject as NSDictionary)
                            UserInfoData.shared.SaveObjectdata(data: betsvault as AnyObject, forkey: Vault_Balancelist)
                            self.betVault_arry = betsvault.payload.vault
                            for item in self.betVault_arry {
                                if item.coin.uppercased() == "USD".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: item.coin, coin: item.coin, color: UIColor.colorWithHexString(hexStr: "B22234"), image: "USD", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "BTC".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Bitcoin", coin: "BTC", color: UIColor.colorWithHexString(hexStr: "F79F1A"), image: "Bitcoin", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "ETH".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Etherum", coin: "ETH", color: UIColor.colorWithHexString(hexStr: "3C3C3D"), image: "Cubs", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                                if item.coin.uppercased() == "USDT".uppercased() {
                                    self.Currenydata.append(staticpaymentCoin(name: "Tether", coin: "USDT", color: UIColor.colorWithHexString(hexStr: "1BA27A"), image: "Tether", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                                }
                            }
                            
                            self.ValutCollection.delegate = self
                            self.ValutCollection.dataSource = self
                            self.setupBetVault(index: self.getcoinIndex(coin: self.CoinData.coin))
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.dismiss(animated: true) {
                                    JustDismiss!()
                                }
                            })
                        }
                        ApiLoader.shared.stopHUD()
                    }
                }) { (Message, Code) in
                    main {
                        CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                            self.dismiss(animated: true) {
                                JustDismiss!()
                            }
                        })
                        ApiLoader.shared.stopHUD()
                    }
                }
            }
            else {
                main {
                    self.betVault_arry = balanceobj!.vault
                    for item in self.betVault_arry {
                        if item.coin.uppercased() == "USD".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: item.coin, coin: item.coin, color: UIColor.colorWithHexString(hexStr: "B22234"), image: "USD", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "BTC".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Bitcoin", coin: "BTC", color: UIColor.colorWithHexString(hexStr: "F79F1A"), image: "Bitcoin", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "ETH".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Etherum", coin: "ETH", color: UIColor.colorWithHexString(hexStr: "3C3C3D"), image: "Cubs", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                        if item.coin.uppercased() == "USDT".uppercased() {
                            self.Currenydata.append(staticpaymentCoin(name: "Tether", coin: "USDT", color: UIColor.colorWithHexString(hexStr: "1BA27A"), image: "Tether", nativeField: item.liveBalance.nativeField, usd: item.liveBalance.usd))
                        }
                    }
                    self.ValutCollection.delegate = self
                    self.ValutCollection.dataSource = self
                    self.setupBetVault(index: self.getcoinIndex(coin: self.CoinData.coin))
                }
            }
        }
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedDismiss(_ sender: Any) {
        self.dismiss(animated: true) {
            JustDismiss!()
        }
    }
    
    @IBAction func TappedCoinSelection(_ sender: Any) {
        if self.CoinBTN.isSelected {
            self.CoinSTitle.text = "Select Vault"
            self.CoinBTN.isSelected = false
            self.CoinSelection_height.constant = 80
            self.ValutpopupView.isHidden = true
            self.DownArrow.image = UIImage.init(named: "SF_arrowtriangle_down_circle_fill")
        }
        else {
            self.CoinSTitle.text = "Select Bets Vault"
            self.CoinBTN.isSelected = true
            self.CoinSelection_height.constant = 133
            self.ValutpopupView.isHidden = false
            self.DownArrow.image = UIImage.init(named: "SF_arrowtriangle_up_circle_fill")
        }
    }
    
    @IBAction func BackBTN(_ sender: Any) {
        self.dismiss(animated: true) {
            JustDismiss!()
        }
    }
    
    @IBAction func TappedBuyBTN(_ sender: Any) {
        self.dismiss(animated: true) {
            self.TicketDismissWithDictCallBack!(self.SelectedTicket_Dict)
        }
    }
    
    @IBAction func TappedSeletedCoinBTN(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.CoinData.coin.uppercased() != "USD" {
                let obj = self.Currenydata[self.getcoinIndex(coin: self.CoinName.text!)]
                if sender == self.SeletedCoinBTN {
                    self.SeletedCoinBTN.showGradientSkeleton()
                    CommanApiCall().ForexConversion(Buy: obj.coin, From: "USD") { (keyvalue1, keyvalue2) in
                        self.isUSDFormat = true
                        self.SeletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
                        self.UnseletedCoinBTN.setTitle(obj.coin.uppercased(), for: .normal)
                        self.UnseletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
                        self.SeletedCoinBTN.setTitle("USD", for: .normal)
                        
                        let price = self.activeLotteries.ticketPrice! * keyvalue2
                        self.PriceLBL.text = String.init(format: "%f", price).WithoutCoinPriceThumbRules(Coin: "USD")
                        self.CurrencyLBL.text = "USD"
                        self.AmountTotalLBL.text = "USD" + " Total"
                        self.CoinDetail.text = String.init(format: "%f", (obj.nativeField * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                        if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
                            let f = Double(truncating: n)
                            self.TXTValue2.text = String.init(format: "%f", (price * f)).CoinPriceThumbRules(Coin: "USD")
                        }
                        self.SeletedCoinBTN.hideSkeleton()
                    }
                }
                else {
                    self.isUSDFormat = false
                    self.UnseletedCoinBTN.setTitleColor(AppDarkColor, for: .normal)
                    self.UnseletedCoinBTN.setTitle(obj.coin.uppercased(), for: .normal)
                    self.SeletedCoinBTN.setTitleColor(UIColor.lightGray, for: .normal)
                    self.SeletedCoinBTN.setTitle("USD", for: .normal)
                    self.PriceLBL.text = String.init(format: "%f", self.activeLotteries.ticketPrice!).WithoutCoinPriceThumbRules(Coin: obj.coin)
                    self.CurrencyLBL.text = obj.coin
                    self.AmountTotalLBL.text = obj.coin + " Total"
                    self.CoinDetail.text = String.init(format: "%f", obj.nativeField).CoinPriceThumbRules(Coin: obj.coin)
                    if let n = NumberFormatter().number(from: self.TXTValue1.text! as String) {
                        let f = Double(truncating: n)
                        self.TXTValue2.text = String.init(format: "%f", (self.activeLotteries.ticketPrice! * f)).CoinPriceThumbRules(Coin: obj.coin)
                    }
                }
            }
        }
    }
    
}

//MARK:- UITextFieldDelegate
extension BuyTicketVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == self.TXTValue1 {
            let btntitle = String.init(format: "%@ %@ %@", "Buy", newString, "Tickets")
            self.BuyBTN.setTitle(btntitle, for: .normal)
            if let n = NumberFormatter().number(from: newString as String) {
                let f = Double(truncating: n)
                let totalprice = self.TicketPrice * f
                if self.isUSDFormat {
                    self.TXTValue2.isSkeletonable = true
                    self.TXTValue2.showGradientSkeleton()
                    CommanApiCall().ForexConversion(Buy: self.CoinData.coin, From: "USD") { (keyvalue1, keyvalue2) in
                        self.TXTValue2.text = String.init(format: "%f", totalprice * keyvalue2).CoinPriceThumbRules(Coin: "USD")
                        self.TXTValue2.hideSkeleton()
                    }
                }
                else {
                    self.TXTValue2.text = String.init(format: "%f", totalprice).CoinPriceThumbRules(Coin: self.CoinData.coin)
                }
                let array = NSMutableArray.init()
                for _ in 0...(Int(truncating: n) - 1) {
                    array.add("0")
                }
                self.SelectedTicket_Dict.setValue(array, forKey: Buy_Tickets_Count)
            }
        }
        else {
            return false
        }
        return true
    }
    
}

extension BuyTicketVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Currenydata.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ValutCurrencyCell",for: indexPath) as? ValutCurrencyCell else { fatalError() }
        
        let data = self.Currenydata[indexPath.row]
        
//        cell.ValutIMG.downloadedFrom(link: data.imageUrl, contentMode: .scaleAspectFit, radious: (cell.ValutIMG.frame.height / 2))
        cell.ValutTitle.text = data.coin
        cell.ValutIMG.image = UIImage.init(named: data.image)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.setupBetVault(index: indexPath.row)
            self.CoinBTN.isSelected = false
            self.CoinSelection_height.constant = 80
            self.ValutpopupView.isHidden = true
            self.DownArrow.image = UIImage.init(named: "SF_arrowtriangle_down_circle_fill")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 52, height: 52)
    }
    
}
