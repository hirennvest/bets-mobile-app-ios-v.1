//
//  FD_Step7VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications
import Lottie

class FD_Step7VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    
    // TODO:- Step 7 Animation View
    @IBOutlet weak var S7_Animationview: UIView!
    @IBOutlet weak var S7_Animation: UIView!
    
    var FromCome: String!
    var S1_CoinsVault: BetsVaultVault!
    var S4_CoinsVault: CoinVaultCoin!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    var S4_VaultType: String!
    var S1_VaultType: String!
    var Topupamount1: Double = 0.0
    var Topupamount2: Double = 0.0
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.cornerColor(color: UIColor.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DF_MainView_Height.constant = (Screen_height / 2) + bottom_padding
        if self.FromCome.uppercased() == "FUND".uppercased() {
            self.TopupAmount()
        }
        else {
            self.WithdrawAmount()
        }
    }

    // MARK:- API Calling
    func TopupAmount() {
        self.SetupAnimation_7()
        background {
            let param = BetVaultTopupParam.init(email: HeaderSetter().email, source_coin: self.S1_CoinsVault.coin, dest_coin: self.S4_CoinsVault.coinSymbol, amount: self.Topupamount2, arcade_userid: HeaderSetter().ArcadeUserid)
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            NetworkingRequests.shared.requestPOST(API_TopupVault, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let step8 = FD_Step8VC.init(nibName: "FD_Step8VC", bundle: nil)
                        step8.FromCome = self.FromCome
                        step8.S1_CoinsVault = self.S1_CoinsVault
                        step8.S4_CoinsVault = self.S4_CoinsVault
                        step8.Topupamount1 = self.Topupamount1
                        step8.Topupamount2 = self.Topupamount2
                        self.navigationController?.pushViewController(step8, animated: false)
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            App?.SetDashboardVC(index: 2)
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        App?.SetDashboardVC(index: 2)
                    })
                }
            }
        }
    }
    
    func WithdrawAmount() {
        self.SetupAnimation_7()
        background {
            
            let param = BetVaultWithdrawParam.init(email: HeaderSetter().email, source_coin: self.S1_CoinsVault.coin, dest_coin: self.S4_CoinsVault.coinSymbol, amount: self.Topupamount2, arcade_userid: "")
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            NetworkingRequests.shared.requestPOST(API_WithdrawVault, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let step8 = FD_Step8VC.init(nibName: "FD_Step8VC", bundle: nil)
                        step8.FromCome = self.FromCome
                        step8.S1_CoinsVault = self.S1_CoinsVault
                        step8.S4_CoinsVault = self.S4_CoinsVault
                        step8.Topupamount1 = self.Topupamount1
                        step8.Topupamount2 = self.Topupamount2
                        self.navigationController?.pushViewController(step8, animated: false)
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            App?.SetDashboardVC(index: 2)
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        App?.SetDashboardVC(index: 2)
                    })
                }
            }
        }
    }
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupAnimation_7() {
        let lottie = AnimationView(name: "wallet-coin")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.S7_Animation.addSubview(lottie)
        self.S7_Animation.backgroundColor = .clear
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.S7_Animation.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.S7_Animation.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.S7_Animation.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.S7_Animation.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.width).isActive = true
        lottie.play()
    }
    
    // MARK:- IBAction Methods
    

}
