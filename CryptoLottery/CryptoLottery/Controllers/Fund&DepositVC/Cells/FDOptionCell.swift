//
//  FDOptionCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FDOptionCell: UICollectionViewCell {

    @IBOutlet weak var cardview: CustomView!
    @IBOutlet weak var CardIMG: UIImageView!
    @IBOutlet weak var NameLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.CardIMG.tintColor = AppDarkColor
        self.NameLBL.textColor = AppDarkColor
    }

}
