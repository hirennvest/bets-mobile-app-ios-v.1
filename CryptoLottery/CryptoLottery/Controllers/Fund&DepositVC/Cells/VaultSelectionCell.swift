//
//  VaultSelectionCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-17.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class VaultSelectionCell: UITableViewCell {

    @IBOutlet weak var CoinSelectionView: UIView!
    @IBOutlet weak var IMGCoin: UIImageView!
    @IBOutlet weak var CoinName: UILabel!
    @IBOutlet weak var CoinDetail: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.theme_backgroundColor = GlobalPicker.backgroundColor
        self.CoinSelectionView.theme_backgroundColor = GlobalPicker.backgroundColor
        self.CoinName.theme_textColor = GlobalPicker.textColor
        self.CoinDetail.theme_textColor = GlobalPicker.textColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
