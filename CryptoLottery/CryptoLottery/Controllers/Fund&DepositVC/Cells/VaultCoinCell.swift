//
//  VaultCoinCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-03.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class VaultCoinCell: UITableViewCell {

    @IBOutlet weak var CoinSelectionView: UIView!
    @IBOutlet weak var CoinSTitle: UILabel!
    @IBOutlet weak var IMGCoin: UIImageView!
    @IBOutlet weak var CoinName: UILabel!
    @IBOutlet weak var Seprator: UILabel!
    @IBOutlet weak var CoinDetail: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.theme_backgroundColor = GlobalPicker.backgroundColor
        self.CoinSelectionView.theme_backgroundColor = GlobalPicker.backgroundColor
        
        self.CoinSTitle.theme_textColor = GlobalPicker.notesColor
        self.CoinName.theme_textColor = GlobalPicker.textColor
        self.CoinDetail.theme_textColor = GlobalPicker.notesColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
