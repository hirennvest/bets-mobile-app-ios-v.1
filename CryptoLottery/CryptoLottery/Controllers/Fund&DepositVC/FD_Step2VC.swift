//
//  FD_Step2VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications

class FD_Step2VC: BaseVC {

    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!

    // TODO:- Step 2 Funding Methods
    @IBOutlet weak var S2_ShowView: UIView!
    @IBOutlet weak var S2_TBL: UITableView!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S2_ProcessBTN: UIButton!

    
    var S1_CoinsVault: BetsVaultVault!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    
    var FinalCoinAddress: String = ""
    var FromCome: String!
    var VaultType: String!
    
    var Step2DismissCallBack: (() -> Void)?
    
    // TODO:- Setp 2 Variables
    var S2_SelectedPayIndex: Int!
    var S2_StaticPayOption: [String] = ["VaultDeposit", "snapay"]
    var isEnableforAmount: Bool = false
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
    
        self.cornerColor()
        self.SetupStep_2()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupStep_2() {
        self.DF_MainDismissBTN.tag = 1
        
        self.cornerColor(color: UIColor.white)
        
        if self.VaultType.uppercased() == "Crypto".uppercased() {
            self.DF_MainView_Height.constant = (Screen_height / 2) + 60
        }
        else {
            self.DF_MainView_Height.constant = (Screen_height / 2)
        }
        
        self.DF_MainTitleLBL.text = "Selecting Funding Method"
        
        self.S2_ShowView.backgroundColor = .white
        self.S2_TBL.backgroundColor = .white
        
        self.S2_ProcessBTN.setTitle("Proceed To Fund With _____", for: .normal)
        self.S2_ProcessBTN.isUserInteractionEnabled = false
        self.S2_ProcessBTN.alpha = 0.5
        self.S2_ProcessBTN.tintColor = .clear
        self.S2_ProcessBTN.backgroundColor = .clear
        self.S2_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S2_TBL.register(UINib.init(nibName: "FundingCell", bundle: nil), forCellReuseIdentifier: "FundingCell")
        
        self.S2_TBL.isScrollEnabled = false
        self.S2_TBL.delegate = self
        self.S2_TBL.dataSource = self
        self.S2_TBL.reloadData()
    }
    
    //    TODO:- S2 Tableview Config Methods
    func S2_TableviewCell(indexPath: IndexPath) -> FundingCell {
        let cell: FundingCell = self.S2_TBL.dequeueReusableCell(withIdentifier: "FundingCell") as! FundingCell
        cell.Icon.isSkeletonable = true
        cell.Name.isSkeletonable = true
        cell.MainIMG.isSkeletonable = true
        
        if self.VaultType.uppercased() == "Crypto".uppercased() {
            if indexPath.row == 0 {
                cell.Name.text = self.S1_CoinsVault.coin.uppercased()
                cell.Icon.isHidden = true
                cell.Name.isHidden = true
                cell.MainIMG.isHidden = false
                if self.S1_CoinsVault.coin.uppercased() == "BTC".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "BTCDeposite")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "ETH".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "ethereum_logo")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "GXT".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "gxtoken_logo")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "USDT".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "Tether_Logo")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "xmr".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "Monero_logo")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "ltc".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "Litecoin_logo")
                }
                else if self.S1_CoinsVault.coin.uppercased() == "xrp".uppercased() {
                    cell.MainIMG.image = UIImage.init(named: "Ripple_logo")
                }
                else {
                    cell.MainIMG.image = UIImage.init(named: "BetsAddress")
                }
            }
            else if indexPath.row == 1 {
                cell.enableMode = .enabled
                cell.MainIMG.image = UIImage.init(named: "VaultDeposit")
                cell.Icon.isHidden = true
                cell.Name.isHidden = true
                cell.MainIMG.isHidden = false
                cell.isEnableCell(status: true)
            }
            else {
                cell.enableMode = .disabled
                cell.MainIMG.image = UIImage.init(named: "snapay")
                cell.Icon.isHidden = true
                cell.Name.isHidden = true
                cell.MainIMG.isHidden = false
                cell.isEnableCell(status: false)
            }
        }
        else {
            if indexPath.row == 0 {
                cell.enableMode = .enabled
                cell.MainIMG.image = UIImage.init(named: "VaultDeposit")
                cell.Icon.isHidden = true
                cell.Name.isHidden = true
                cell.MainIMG.isHidden = false
                cell.isEnableCell(status: true)
            }
            else {
                cell.enableMode = .disabled
                cell.MainIMG.image = UIImage.init(named: "snapay")
                cell.Icon.isHidden = true
                cell.Name.isHidden = true
                cell.MainIMG.isHidden = false
                cell.isEnableCell(status: false)
            }
        }
        return cell
    }
    
    // MARK:- IBAction Methods
    @IBAction func S2_TappedBTN(_ sender: UIButton) {
        if self.VaultType.uppercased() == "Crypto".uppercased() {
            if self.S2_SelectedPayIndex == 0 {
                let step3 = FD_Step3VC.init(nibName: "FD_Step3VC", bundle: nil)
                step3.FromCome = self.FromCome
                step3.S1_CoinsVault = self.S1_CoinsVault
                self.navigationController?.pushViewController(step3, animated: false)
            }
            else if self.S2_SelectedPayIndex == 1 {
                let step4 = FD_Step4VC.init(nibName: "FD_Step4VC", bundle: nil)
                step4.FromCome = self.FromCome
                step4.S1_CoinsVault = self.S1_CoinsVault
                step4.SectionCoin = self.SectionCoin
                step4.BetsVaultLoad = self.BetsVaultLoad
                step4.S1_VaultType = self.VaultType
                self.navigationController?.pushViewController(step4, animated: false)
            }
            else {
                
            }
        }
        else {
            if self.S2_SelectedPayIndex == 0 {
                let step4 = FD_Step4VC.init(nibName: "FD_Step4VC", bundle: nil)
                step4.FromCome = self.FromCome
                step4.S1_CoinsVault = self.S1_CoinsVault
                step4.SectionCoin = self.SectionCoin
                step4.BetsVaultLoad = self.BetsVaultLoad
                step4.S1_VaultType = self.VaultType
                self.navigationController?.pushViewController(step4, animated: false)
            }
            else {
                
            }
        }
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

//MARK:- Tableview Delegate
extension FD_Step2VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.VaultType.uppercased() == "Crypto".uppercased() {
            return 3
        }
        else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.S2_TableviewCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.S2_SelectedPayIndex = indexPath.row
        var coin: String = ""
        if self.VaultType.uppercased() == "Crypto".uppercased() {
            if indexPath.row == 0 {
                coin = self.S1_CoinsVault.coin
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin + " Address", for: .normal)
            }
            else if indexPath.row == 1 {
                coin = "Vault"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
            else {
                coin = "Snapay"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
        }
        else {
            if indexPath.row == 0 {
                coin = "Vault"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
            else {
                coin = "Snapay"
                self.S2_ProcessBTN.setTitle("Proceed To Fund With " + coin, for: .normal)
            }
        }
        self.S2_ProcessBTN.isUserInteractionEnabled = true
        self.S2_ProcessBTN.alpha = 1
    }
    
}
