//
//  FD_Step4VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit


class FD_Step4VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 4 CoinSelectionView
    @IBOutlet weak var S4_CoinSectionview: UIView!
    @IBOutlet weak var S4_SegmentView: UIView!
    @IBOutlet var S4_SegmentBTN: [UIButton]!
    @IBOutlet weak var S4_TBL: UITableView!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var S4_ProcessBTN: UIButton!
    
    // TODO:- Step 4 Variables
    var CS4_SelectedSegment: Int = 0
    var FromCome: String!
    var S1_CoinsVault: BetsVaultVault!
    var S4_CoinsVault: CoinVaultCoin!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    var S4_VaultType: String!
    var S1_VaultType: String!
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupCoinSelection_4()
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }
    
    func SetupCoinSelection_4(index: Int = 0) {
        self.DF_MainView_Height.constant = (Screen_height / 2) + (Screen_height / 4)
        
        self.DF_MainTitleLBL.text = "Select Liquid Vault"
        
        if index == 0 {
            self.CS4_SelectedSegment = 0
            self.S4_SegmentBTN[0].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S4_SegmentBTN[1].titleLabel?.font = Font().MontserratLightFont(font: 17)
        }
        else {
            self.CS4_SelectedSegment = 1
            self.S4_SegmentBTN[1].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S4_SegmentBTN[0].titleLabel?.font = Font().MontserratLightFont(font: 17)
        }
        
        self.CS4_SelectedSegment = index
        
        self.S4_SegmentView.clipsToBounds = true
        self.S4_SegmentView.backgroundColor = UIColor.colorWithHexString(hexStr: "E4E9F2")
        self.S4_SegmentView.layer.borderWidth = 1.0
        self.S4_SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "E4E9F2").cgColor
        self.S4_SegmentView.layer.cornerRadius = 15
        
        self.S4_SegmentBTN[0].clipsToBounds = true
        self.S4_SegmentBTN[1].clipsToBounds = true
        
        self.S4_CoinSectionview.backgroundColor = .white
        self.S4_TBL.backgroundColor = .white
        
        self.S4_ProcessBTN.setTitle("Select ___ Liquid Vault", for: .normal)
        self.S4_ProcessBTN.tintColor = .clear
        self.S4_ProcessBTN.backgroundColor = .clear
        self.S4_ProcessBTN.setTitleColor(.white, for: .normal)
        self.S4_ProcessBTN.isUserInteractionEnabled = false
        self.S4_ProcessBTN.alpha = 0.5
        
        self.S4_TBL.register(UINib.init(nibName: "VaultSelectionCell", bundle: nil), forCellReuseIdentifier: "VaultSelectionCell")
        
        self.S4_TBL.delegate = self
        self.S4_TBL.dataSource = self
        self.S4_TBL.reloadData()
    }
    
    //    TODO:- S4 Tableview Config Methods
    func ConfigCoinSymbol(symbol: String) -> String {
        let array = symbol.components(separatedBy: ",")
        if array.count == 0 {
            return symbol
        }
        else {
            return (array.first?.removeWhiteSpace())!
        }
    }
    
    func Config_S4Cell(cell: VaultSelectionCell, indexPath: IndexPath) {
        if self.CS4_SelectedSegment == 0 {
            let dataobj = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "Crypto".uppercased()
            }.first
            let data = dataobj?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data!.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data!.coinName
            cell.CoinDetail.text = String.init(format: "%f", (data?.coinValue)!).CoinPriceThumbRules(Coin: data!.coinSymbol.uppercased())
            
            if self.S4_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S4_CoinsVault.coinName == data!.coinName {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        else {
            let dataobj = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "fiat".uppercased()
            }.first
            let data = dataobj?.sectionArray[indexPath.row]
            cell.IMGCoin.downloadedFrom(link: data?.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = data?.coinSymbol.uppercased()
            cell.CoinDetail.text = String.init(format: "%f", (data?.coinValue)!).CoinPriceThumbRules(Coin: "USD")
            
            if self.S4_CoinsVault == nil {
                cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
            }
            else {
                if self.S4_CoinsVault.coinSymbol == data!.coinSymbol {
                    cell.CoinName.font = UIFont.boldSystemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.boldSystemFont(ofSize: 15)
                }
                else {
                    cell.CoinName.font = UIFont.systemFont(ofSize: 15)
                    cell.CoinDetail.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func S4_TappedBTN(_ sender: Any) {
        if self.S4_CoinsVault != nil {
            let step5 = FD_Step5VC.init(nibName: "FD_Step5VC", bundle: nil)
            step5.FromCome = self.FromCome
            step5.S1_CoinsVault = self.S1_CoinsVault
            step5.S4_CoinsVault = self.S4_CoinsVault
            step5.SectionCoin = self.SectionCoin
            step5.BetsVaultLoad = self.BetsVaultLoad
            step5.S4_VaultType = self.S4_VaultType
            step5.S1_VaultType = self.S1_VaultType
            step5.DP_Process_Tag = 100
            step5.Value1 = 0.0
            step5.Value2 = 0.0
            self.navigationController?.pushViewController(step5, animated: false)
        }
    }
    
    @IBAction func S4_SegmentBTN(_ sender: UIButton) {
        if sender == self.S4_SegmentBTN[0] {
            self.CS4_SelectedSegment = 0
            self.S4_SegmentBTN[0].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S4_SegmentBTN[1].titleLabel?.font = Font().MontserratLightFont(font: 17)
            self.S4_VaultType = "Crypto"
            self.S4_CoinsVault = nil
        }
        else {
            self.CS4_SelectedSegment = 1
            self.S4_SegmentBTN[1].titleLabel?.font = Font().MontserratBoldFont(font: 17)
            self.S4_SegmentBTN[0].titleLabel?.font = Font().MontserratLightFont(font: 17)
            self.S4_VaultType = "Forex"
            self.S4_CoinsVault = nil
        }
        if self.S4_CoinsVault == nil {
            self.S4_ProcessBTN.setTitle("Select ___ Liquid Vault", for: .normal)
            self.S4_ProcessBTN.isUserInteractionEnabled = false
            self.S4_ProcessBTN.alpha = 0.5
        }
        else {
            var title = "Select ___ Liquid Vault"
            self.S4_ProcessBTN.setTitle(title.replaceLocalized(fromvalue: ["___"], tovalue: [self.S4_CoinsVault.coinName]), for: .normal)
            self.S4_ProcessBTN.isUserInteractionEnabled = true
            self.S4_ProcessBTN.alpha = 1
        }
        self.S4_TBL.reloadData()
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
}

//MARK:- Tableview Delegate
extension FD_Step4VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.CS4_SelectedSegment == 0 {
            let data = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "Crypto".uppercased()
            }.first
            return (data?.sectionArray.count)!
        }
        else {
            let data = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "fiat".uppercased()
            }.first
            return (data?.sectionArray.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VaultSelectionCell = tableView.dequeueReusableCell(withIdentifier: "VaultSelectionCell") as! VaultSelectionCell
        self.Config_S4Cell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.CS4_SelectedSegment == 0 {
            let data = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "Crypto".uppercased()
            }.first
            self.S4_CoinsVault = data?.sectionArray[indexPath.row]
        }
        else {
            let data = self.SectionCoin.filter { (sections) -> Bool in
                sections.sectionkey.uppercased() == "fiat".uppercased()
            }.first
            self.S4_CoinsVault = data?.sectionArray[indexPath.row]
        }
        var title = "Select ___ Liquid Vault"
        self.S4_ProcessBTN.setTitle(title.replaceLocalized(fromvalue: ["___"], tovalue: [self.S4_CoinsVault.coinName]), for: .normal)
        self.S4_ProcessBTN.isUserInteractionEnabled = true
        self.S4_ProcessBTN.alpha = 1
        
        self.S4_TBL.reloadData()
    }
    
}
