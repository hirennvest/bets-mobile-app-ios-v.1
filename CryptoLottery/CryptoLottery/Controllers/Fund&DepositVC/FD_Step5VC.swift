//
//  FD_Step5VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FD_Step5VC: BaseVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: CustomView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: CustomLBL!
    @IBOutlet weak var DF_LogoIMG: UIImageView!
    @IBOutlet weak var DF_MainTitleLBL: UILabel!
    @IBOutlet weak var DF_MainDismissBTN: UIButton!
    
    // TODO:- Step 5 DepositViews
    @IBOutlet weak var S5_DepositView: UIView!
    @IBOutlet weak var S5_TBL: UITableView!
    @IBOutlet weak var S5_ProcessBTN: UIButton!
    @IBOutlet var AllBottomview: UIView!
    
    var FromCome: String!
    var S1_CoinsVault: BetsVaultVault!
    var S4_CoinsVault: CoinVaultCoin!
    var SectionCoin = [CoinSectionArray]()
    var BetsVaultLoad: BetsVaultPayload!
    var S4_VaultType: String!
    var S1_VaultType: String!
    
    var DP_Process_Tag: Int = 100 // tag 100 = processed, tag 200 = Deposit , tag 300 = Deposti Api call
    var Value1: Double = 0.0
    var Value2: Double = 0.0
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = .white
        self.DF_MainView.BGColor = .white
        self.DF_LogoIMG.tintColor = AppDarkColor
        self.DF_MainTitleLBL.theme_textColor = GlobalPicker.textColor
        
        self.AllBottomview.backgroundColor = AppDarkColor
        self.SetupDPWD_5()
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- User Define Methods
    func cornerColor(color: UIColor = AppDarkColor) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupDPWD_5() {
        if self.S1_CoinsVault.coin.uppercased() == self.S4_CoinsVault.coinSymbol.uppercased() {
            self.DF_MainView_Height.constant = 130 + (1 * 120) + bottom_padding
        }
        else {
            self.DF_MainView_Height.constant = 130 + (2 * 100) + 30 + bottom_padding
        }
        
        self.S5_DepositView.backgroundColor = .white
        self.S5_TBL.backgroundColor = .white
        
        self.S5_ProcessBTN.tintColor = .clear
        self.S5_ProcessBTN.backgroundColor = .clear
        self.S5_ProcessBTN.setTitleColor(.white, for: .normal)
        
        self.S5_TBL.register(UINib.init(nibName: "VaultCoinCell", bundle: nil), forCellReuseIdentifier: "VaultCoinCell")
        self.S5_TBL.isScrollEnabled = false
        self.S5_TBL.delegate = self
        self.S5_TBL.dataSource = self
        
        if self.DP_Process_Tag == 100 {
            self.DF_MainTitleLBL.text = "Confirm Your Vault Selection"
            var BTNtitle = "Confirm Transfer From Liquid FIRST Vault To Bets SECOND Vault"
            self.S5_ProcessBTN.setTitle(BTNtitle.replaceLocalized(fromvalue: ["FIRST", "SECOND"], tovalue: [self.S1_CoinsVault.coin.uppercased(), self.S4_CoinsVault.coinSymbol.uppercased()]), for: .normal)
            self.S5_ProcessBTN.titleLabel?.textAlignment = .center
            self.S5_ProcessBTN.sizeToFit()
            self.DP_Process_Tag = 100
            self.S5_TBL.reloadData()
        }
        else if self.DP_Process_Tag == 200 {
            if self.FromCome.uppercased() == "FUND".uppercased() {
                self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
                self.S5_ProcessBTN.setTitle("Deposit -----", for: .normal)
            }
            else {
                self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
                self.S5_ProcessBTN.setTitle("Widthdraw -----", for: .normal)
            }
            self.DP_Process_Tag = 200
            self.S5_ProcessBTN.alpha = 0.5
            self.S5_ProcessBTN.isUserInteractionEnabled = false
            self.S5_TBL.reloadData()
        }
        else {
            self.DP_Process_Tag = 300
            if self.FromCome.uppercased() == "FUND".uppercased() {
                self.DF_MainTitleLBL.text = "Enter Amount For Deposit"
                let title = String.init(format: "Deposit ") + String.init(format: "%f", self.Value1).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)!
                self.S5_ProcessBTN.setTitle(title, for: .normal)
            }
            else {
                self.DF_MainTitleLBL.text = "Enter Amount For Widthdraw"
                let title = String.init(format: "Withdraw ") + String.init(format: "%f", self.Value2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)!
                self.S5_ProcessBTN.setTitle(title, for: .normal)
            }
            self.S5_TBL.reloadData()
        }
    }
    
    //    TODO:- S5 Tableview Config Methods
    func UpdateBetValut(cell: VaultCoinCell, indexpath: IndexPath) {
        if self.S1_CoinsVault.coin.uppercased() == self.S4_CoinsVault.coinSymbol.uppercased() {
            var title = "Your Liquid COIN Vault Will Be Debited"
            cell.CoinSTitle.text = title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coin])
            cell.IMGCoin.downloadedFrom(link: self.S1_CoinsVault.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
            cell.CoinName.text = self.S1_CoinsVault.coin
            if self.DP_Process_Tag == 100 {
                cell.Seprator.isHidden = true
                cell.CoinDetail.text = String.init(format: "%f", self.S1_CoinsVault.liveBalance.nativeField).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            }
            else if self.DP_Process_Tag == 200 {
                cell.Seprator.isHidden = false
                cell.CoinDetail.text = String.init(format: "%f", Value1).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            }
            else {
                cell.Seprator.isHidden = false
                cell.CoinDetail.text = String.init(format: "%f", Value1).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
            }
        }
        else {
            if indexpath.section == 0 {
                var title = "Bets COIN Vault Balance"
                cell.CoinSTitle.text = title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S4_CoinsVault.coinSymbol])
                cell.IMGCoin.downloadedFrom(link: self.S1_CoinsVault.imageUrl, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
                cell.CoinName.text = self.S1_CoinsVault.coin
                if self.DP_Process_Tag == 100 {
                    cell.Seprator.isHidden = true
                    cell.CoinDetail.text = String.init(format: "%f", self.S1_CoinsVault.liveBalance.nativeField).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
                }
                else if self.DP_Process_Tag == 200 {
                    cell.Seprator.isHidden = false
                    cell.CoinDetail.text = String.init(format: "%f", Value1).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
                }
                else {
                    cell.Seprator.isHidden = false
                    cell.CoinDetail.text = String.init(format: "%f", Value1).CoinPriceThumbRules(Coin: self.S1_CoinsVault.coin)
                }
            }
            else {
                var title = "Liquid COIN Vault Balance"
                cell.CoinSTitle.text = title.replaceLocalized(fromvalue: ["COIN"], tovalue: [self.S1_CoinsVault.coin])
                cell.IMGCoin.downloadedFrom(link: self.S4_CoinsVault.coinImage, contentMode: .scaleAspectFit, radious: (cell.IMGCoin.frame.height / 2))
                cell.CoinName.text = self.S4_CoinsVault.coinName
                if self.DP_Process_Tag == 100 {
                    cell.Seprator.isHidden = true
                    cell.CoinDetail.text = String.init(format: "%f", self.S4_CoinsVault.coinValue).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
                }
                else if self.DP_Process_Tag == 200 {
                    cell.Seprator.isHidden = false
                    cell.CoinDetail.text = String.init(format: "%f", Value2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
                }
                else {
                    cell.Seprator.isHidden = false
                    cell.CoinDetail.text = String.init(format: "%f", Value2).CoinPriceThumbRules(Coin: self.S4_CoinsVault.coinSymbol)
                }
            }
        }
    }
    
    func SetupTBlHeader() -> UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: Screen_height))
        view.theme_backgroundColor = GlobalPicker.backgroundColor
        return view
    }
    
    // MARK:- IBAction Methods
    @IBAction func S5_TappedBTN(_ sender: Any) {
        if self.DP_Process_Tag == 100 {
            self.DP_Process_Tag = 200
            self.SetupDPWD_5()
        }
        else if self.DP_Process_Tag == 200 {
            self.DP_Process_Tag = 300
            self.S5_ProcessBTN.alpha = 1
            self.S5_ProcessBTN.isUserInteractionEnabled = false
            self.S5_TBL.reloadData()
        }
        else {
            let step7 = FD_Step7VC.init(nibName: "FD_Step7VC", bundle: nil)
            step7.FromCome = self.FromCome
            step7.S1_CoinsVault = self.S1_CoinsVault
            step7.S4_CoinsVault = self.S4_CoinsVault
            step7.Topupamount1 = self.Value1
            step7.Topupamount2 = self.Value2
            step7.SectionCoin = self.SectionCoin
            step7.BetsVaultLoad = self.BetsVaultLoad
            step7.S4_VaultType = self.S4_VaultType
            step7.S1_VaultType = self.S1_VaultType
            self.navigationController?.pushViewController(step7, animated: false)
        }
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    

}

//MARK:- Tableview Delegate
extension FD_Step5VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.S1_CoinsVault.coin.uppercased() == self.S4_CoinsVault.coinSymbol.uppercased() {
            return 1
        }
        else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VaultCoinCell = tableView.dequeueReusableCell(withIdentifier: "VaultCoinCell") as! VaultCoinCell
        self.UpdateBetValut(cell: cell, indexpath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return section == 0 ? nil : self.SetupTBlHeader()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.DP_Process_Tag == 100 {
            
        }
        else {
            let step6 = FD_Step6VC.init(nibName: "FD_Step6VC", bundle: nil)
            step6.FromCome = self.FromCome
            step6.S1_CoinsVault = self.S1_CoinsVault
            step6.S4_CoinsVault = self.S4_CoinsVault
            step6.Topupamount1 = 0.00
            step6.Topupamount2 = 0.00
            step6.SectionCoin = self.SectionCoin
            step6.BetsVaultLoad = self.BetsVaultLoad
            step6.S4_VaultType = self.S4_VaultType
            step6.S1_VaultType = self.S1_VaultType
            self.navigationController?.pushViewController(step6, animated: false)
        }
    }
    
}
