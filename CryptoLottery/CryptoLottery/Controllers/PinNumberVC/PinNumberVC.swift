//
//  PinNumberVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PinNumberVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var Mainview: CustomView!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var PinVIew: UIStackView!
    @IBOutlet var PinValueView: [CustomView]!
    @IBOutlet var PinValueLBL: [UILabel]!
    @IBOutlet weak var DefaultPinLBL: UILabel!
    
    @IBOutlet weak var PinDigitView: UIView!
    @IBOutlet weak var FirstStack_Leading: NSLayoutConstraint!
    @IBOutlet var DigitsBTN: [UIButton]!
    
    //    MARK:- Variable Define
    
    let CodeCount: Int = 6
    let PinSTR: String = "●"
    var PinArray: NSMutableArray!
    var FinalOTP: String!
    
    public var NumberDismissCallBack: ((String) -> Void)?
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.backgroundColor = AppDarkColor
        self.setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    MARK:- User Define Methods
    
    func setupUI() {
        
        self.PinArray = NSMutableArray.init(capacity: self.CodeCount)
        self.Mainview.backgroundColor = .white
        
        self.DefaultPinLBL.text = "000-000"
        self.PinVIew.isHidden = true
        self.DefaultPinLBL.isHidden = false
        self.DefaultPinLBL.theme_textColor = GlobalPicker.textColor
        
        let string1: String = "Test your Luck"
        let string2: String = "\n Pick \(self.CodeCount) Numbers" as String
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratBoldFont(font: 25.0), AppDarkColor), (string2, Font().MontserratRegularFont(font: 15.0), AppDarkColor)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
        self.TitleLBL.attributedText = myMutableString
        
        self.PinDigitView.backgroundColor = AppDarkColor
        
        for n in 0...self.PinValueView.count - 1 {
            let view: CustomView = self.PinValueView[n]
            view.BGColor = AppDarkColor
            view.BorderColor = AppDarkColor
            view.BorderWidth = 1
            view.isCircle = true
            view.clickBound = true
            view.maskBound = true
            
            let lbl = self.PinValueLBL[n]
            lbl.textColor = .white
            lbl.textAlignment = .center
            lbl.text = ""
        }
        
        for item in self.DigitsBTN {
            if item.tag == 100 {
                let inset = (item.frame.height * 40) / 100
                item.imageEdgeInsets = UIEdgeInsets.init(top: inset, left: inset, bottom: inset, right: inset)
            }
        }
        
    }
    
    func EnterPin(digit: String) {
        self.DefaultPinLBL.isHidden = true
        self.PinVIew.isHidden = false
        if self.PinArray.count <= self.CodeCount - 2 {
            self.PinArray.add(digit)
        }
        else {
            if self.PinArray.count <= self.CodeCount - 1 {
                self.PinArray.add(digit)
            }
        }
        self.reloadPin()
    }
    
    func reloadPin() {
        for (index, item) in self.PinArray.enumerated() {
            if index <= self.CodeCount - 2 {
                self.PinValueLBL[index].text = item as? String
//                self.PinValueLBL[index].text = self.PinSTR
            }
            if index == self.CodeCount - 1 {
                self.PinValueLBL[index].text = item as? String
//                self.PinValueLBL[index].text = self.PinSTR
                self.FinalOTP = self.PinArray.componentsJoined(by: "")
                print("Entered Pin is: \(String(describing: self.FinalOTP))")
                self.dismiss(animated: true) {
                    self.NumberDismissCallBack!(self.FinalOTP)
                }
            }
        }
    }
    
    func backspace() {
        for index in 0...self.CodeCount - 1 {
            if index < self.PinArray.count {
                self.PinValueLBL[index].text = self.PinArray.object(at: index) as? String
                self.PinValueLBL[index].text = self.PinSTR
            }
            else {
                self.PinValueLBL[index].text = ""
                self.PinValueLBL[index].text = ""
            }
        }
    }
    
    //    MARK:- Api Calling
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedDismiss(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func TappedDigitsBTN(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 2:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 3:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 4:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 5:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 6:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 7:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 8:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 9:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 100:
            self.PinArray.removeLastObject()
            self.backspace()
            break
        case 0:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
        case 200:
            break
        default:
            break
        }
    }
    
}
