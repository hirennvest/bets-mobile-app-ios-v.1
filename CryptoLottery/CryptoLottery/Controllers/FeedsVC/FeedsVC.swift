//
//  FeedsVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-28.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications
import SwipeCellKit


let NodataMSG = "Sorry There Seems To Be No LOTTERY COIN Lotteries"
let FetchMSG = "We are fetching LOTTERY COIN Lotteries"
let Buy_Tickets_Count = "Tickets" // Buy Ticket value and it's array with ticket numbers <array>
let Buy_Vault = "Vault" // staticpaymentCoin <obj> from <Currenydata>
let Buy_Price = "Price" // Buy Price double value <TicketPrice>
let Buy_Currency = "Currency" // Currency String  <CurrencyLBL>
let Selected_Coin = "Coins" // CoinListPayload <CoinData>
let CoinsList = "Bets_CoinsList" // CoinListPayload
let Vault_Balancelist = "Vault_Balancelist"
let SelectedLottery = "SelectedLottery" // LotteryActiveLottery <activeLotteries> Or LotteryClosedLottery <closedLotteries>

let Pooled_keyvalue = "Pooled_LOTTERY_Jackpot_COIN"
let Pooled_keyPrize = "Pooled_LOTTERY_Prize_COIN"
let Closed_keyvalue = "Closed_LOTTERY_Jackpot_COIN"
let Closed_keyPrize = "Closed_LOTTERY_Prize_COIN"

class FeedsVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var NavBGView: UIView!
    @IBOutlet weak var NavIMG: UIImageView!
    
    @IBOutlet weak var Carousel: iCarousel!
    
    @IBOutlet weak var FeedTBL: UITableView!
    
    @IBOutlet weak var SelectedFeedType: UIView!
    @IBOutlet weak var SelctedFeedImg: UIImageView!
    @IBOutlet weak var SelectedFeedTitle: UILabel!
    @IBOutlet weak var SelectedFeedBTN: UIButton!
    
    @IBOutlet weak var FeedTypePopup: UIView!
    @IBOutlet weak var FeedTitleLBL: UILabel!
    @IBOutlet weak var OpenFeedImg: UIImageView!
    @IBOutlet weak var OpenFeedTitle: UILabel!
    @IBOutlet weak var OpenFeedBTN: UIButton!
    
    @IBOutlet weak var PooledFeedImg: UIImageView!
    @IBOutlet weak var PooledFeedTitle: UILabel!
    @IBOutlet weak var PooledFeedBTN: UIButton!
    
    @IBOutlet weak var ClosedFeedImg: UIImageView!
    @IBOutlet weak var ClosedFeedTitle: UILabel!
    @IBOutlet weak var ClosedFeedBTN: UIButton!
    
    @IBOutlet weak var nodatafoundLBL: UILabel!
    
    //    MARK:- Variable Define
    
    var lottery_arry : LotteryPayload!
    
    var Carousel_Coins : [CoinListPayload]!
    var Filtered_Coins : [CoinListPayload]!
    var SelectedBottomCell: Int = 0
    var CMC_Prices: CMCPrices!
    
    
    var SelectedLotteryType: Int = 0
    
    lazy var nodatafound: NodatafoundView = {
        () -> NodatafoundView in
        let bounds = self.view.frame
        let nodata = NodatafoundView.init(frame: bounds)
        nodata.SetupView(title: "No data available!", Notes: "There are no Data available yet!", image: "", enable: true)
        self.view.addSubview(nodata)
        return nodata
    }()
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommanApiCall().CMCprices { (CMC) in
            self.CMC_Prices = CMC
        }
        self.FeedTypePopup.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .ReloadFeed, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCoinList()
        self.NavBGView.backgroundColor = UIColor.white
        self.Carousel.clipsToBounds = true
        self.Carousel.layer.borderWidth = 1
        self.Carousel.layer.borderColor = AppDarkColor.withAlphaComponent(0.1).cgColor
        self.SetupUI()
        self.GetFundVault()
    }
    
    //    MARK:- User Define Methods
    
    @objc func onDidReceiveData(_ notification:Notification) {
        self.FeedTBL.reloadData()
        self.FeedTypePopup.isHidden = true
    }
    
    func SetupUI() {
        
        self.nodatafoundLBL.numberOfLines = 0
        self.nodatafoundLBL.lineBreakMode = .byWordWrapping
        self.nodatafoundLBL.font = Font().MontserratBoldFont(font: 20.0)
        self.nodatafoundLBL.textColor = AppDarkColor
        self.nodatafoundLBL.textAlignment = .center
        
        self.nodatafound.isHidden = false
        
        self.FeedTitleLBL.text = "Select Feed Type"
        self.FeedTitleLBL.font = Font().MontserratRegularFont(font: 25)
        
        self.FeedTypePopup.isHidden = true
        self.SelectedFeedTitle.text = "Open"
        self.SelectedFeedTitle.font = Font().MontserratRegularFont(font: 13)
        self.SelectedFeedTitle.sizeToFit()
        self.SelctedFeedImg.image = UIImage.init(named: "Padlock")
        
        self.FeedTBL.register(UINib.init(nibName: "CoinCarouselCell", bundle: nil), forCellReuseIdentifier: "CoinCarouselCell")
        self.FeedTBL.register(UINib.init(nibName: "LotteryListCell", bundle: nil), forCellReuseIdentifier: "LotteryListCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        
        self.FeedTBL.tableFooterView = footer
        
        self.SelectedLotteryType = 0
        
        self.Carousel.type = .coverFlow
    
    }
    
    func ReloadCarouselandTBL() {
        self.Carousel.delegate = self
        self.Carousel.dataSource = self
        self.Carousel.reloadData()
        self.Carousel.scrollToItem(at: self.SelectedBottomCell, animated: true)
        
        self.nodatafound.isHidden = true
        self.FeedTBL.isHidden = false
        self.FeedTBL.delegate = self
        self.FeedTBL.dataSource = self
        self.FeedTBL.reloadData()
        ApiLoader.shared.stopHUD()
    }
    
    func CheckOpenCoinLottery() {
        let group = DispatchGroup()
        if self.Filtered_Coins == nil {
            self.Filtered_Coins = [CoinListPayload]()
        }
        else {
            self.Filtered_Coins.removeAll()
        }
        
        for item in self.Carousel_Coins {
            group.enter()
            self.getLotteryList(selectedcoin: item.coin) { (LotteryLoad) in
                if LotteryLoad.activeLotteries.count > 0 {
                    self.Filtered_Coins.append(item)
                }
                group.leave()
            }
        }
        group.notify(queue: DispatchQueue.main) {
            if self.Filtered_Coins.count != 0 {
                self.getLotteryList(selectedcoin: (self.Filtered_Coins.first?.coin ?? "")!) { (LotteryLoad) in
                    self.lottery_arry = LotteryLoad
                    self.ReloadCarouselandTBL()
                }
            }
            else {
                ApiLoader.shared.stopHUD()
                self.nodatafound.isHidden = false
                self.FeedTBL.isHidden = true
            }
        }
    }
    
    //    MARK:- Api Calling
    func GetFundVault() {
        background {
            let param = FundVaultParamDict.init(arcade_userid: HeaderSetter().ArcadeUserid)
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            NetworkingRequests.shared.requestPOST(API_BetsVaultBalance, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let betsvault: BetsVaultRootClass = BetsVaultRootClass.init(fromDictionary: responseObject as NSDictionary)
                        UserInfoData.shared.SaveObjectdata(data: betsvault.payload as AnyObject, forkey: Vault_Balancelist)
                    }
                    else {
                        
                    }
                }
            }) { (Message, Code) in
                
            }
        }
    }
    
    func getCoinList() {
        background {
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            ApiLoader.shared.showHUD()
            NetworkingRequests.shared.requestPOST(API_CoinList, Parameters: [:], Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    let coinlist: CoinListRootClass = CoinListRootClass.init(fromDictionary: responseObject)
                    if status && coinlist.status {
                        self.getLotteryList(selectedcoin: (coinlist.payload.first?.coin)!) { (LotteryLoad) in
                            self.lottery_arry = LotteryLoad
                            if self.Carousel_Coins == nil {
                                self.Carousel_Coins = [CoinListPayload]()
                            }
                            else {
                                self.Carousel_Coins.removeAll()
                            }
                            
                            UserInfoData.shared.SaveObjectdata(data: coinlist as AnyObject, forkey: CoinsList)
                            
                            var array = coinlist.payload
                            array?.removeAll(where: { (coin) -> Bool in
                                coin.coin.uppercased() == "gxt".uppercased()
                            })
                            
                            self.Carousel_Coins = UserUtility().CoinArrangeOrder(coins: array!)
                            
                            if self.SelectedLotteryType == 0 {
                                self.CheckOpenCoinLottery()
                            }
                            else {
                                self.ReloadCarouselandTBL()
                            }
                        }
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.nodatafound.isHidden = false
                            self.nodatafound.didActionBlock = {
                                self.getCoinList()
                            }
                            self.FeedTBL.isHidden = true
                            ApiLoader.shared.stopHUD()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.nodatafound.isHidden = false
                        self.nodatafound.didActionBlock = {
                            self.getCoinList()
                        }
                        self.FeedTBL.isHidden = true
                    })
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    func getLotteryList(selectedcoin: String, ReloadData: ((_ data: LotteryPayload)->Void)?) {
        background {
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            let param = LotteryCoinParam.init(coin: selectedcoin)
            
            NetworkingRequests.shared.requestPOST(API_LotteryList, Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let root = LotteryListRootClass.init(fromDictionary: responseObject)
                        if root.status {
                            ReloadData!(root.payload)
                        }
                        else {
                            let Message = responseObject["payload"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                                self.nodatafound.isHidden = false
                                self.nodatafound.didActionBlock = {
                                    self.getCoinList()
                                }
                                self.FeedTBL.isHidden = true
                            })
                        }
                    }
                    else {
                        let Message = responseObject["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.nodatafound.isHidden = false
                            self.nodatafound.didActionBlock = {
                                self.getCoinList()
                            }
                            self.FeedTBL.isHidden = true
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.nodatafound.isHidden = false
                        self.nodatafound.didActionBlock = {
                            self.getCoinList()
                        }
                        self.FeedTBL.isHidden = true
                    })
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedFeedTypeTapped(_ sender: UIButton) {
        if sender == self.SelectedFeedBTN {
            self.FeedTypePopup.isHidden = false
        }
        else if sender == self.OpenFeedBTN {
            self.SelectedLotteryType = 0
            sender.titleLabel!.font = Font().MontserratRegularFont(font: 17)
            self.FeedTypePopup.isHidden = true
            self.SelectedFeedTitle.text = "Open"
            self.SelectedFeedTitle.sizeToFit()
            self.CheckOpenCoinLottery()
            self.SelctedFeedImg.image = UIImage.init(named: "Padlock")
        }
        else if sender == self.PooledFeedBTN {
            self.SelectedLotteryType = 1
            sender.titleLabel!.font = Font().MontserratRegularFont(font: 17)
            self.FeedTypePopup.isHidden = true
            self.SelectedFeedTitle.text = "Pooled"
            self.SelectedFeedTitle.sizeToFit()
            self.SelctedFeedImg.image = UIImage.init(named: "PooledIcon")
            self.Carousel.reloadData()
            self.FeedTBL.reloadData()
        }
        else if sender == self.ClosedFeedBTN {
            self.SelectedLotteryType = 2
            sender.titleLabel!.font = Font().MontserratRegularFont(font: 17)
            self.FeedTypePopup.isHidden = true
            self.SelectedFeedTitle.text = "Closed"
            self.SelectedFeedTitle.sizeToFit()
            self.SelctedFeedImg.image = UIImage.init(named: "ClosedIcon")
            self.Carousel.reloadData()
            self.FeedTBL.reloadData()
        }
    }
    
}

//MARK:- Tableview Delegate and datasource
extension FeedsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var nodatamessage = NodataMSG
        let coindata = self.Carousel_Coins[self.SelectedBottomCell]
        if self.SelectedLotteryType == 0 {
            if self.lottery_arry.activeLotteries.count == 0 {
                var str = "Unfortunately There Are No Open Lotteries For COIN\n\n"
                let string1 = str.replaceLocalized(fromvalue: ["COIN"], tovalue: [coindata.name])
                let string2 = "But Fret Not, There Are Open CryptoLotteries In These Currencies"

                let mainstring: String = string1 + string2
                let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratBoldFont(font: 20.0), AppDarkColor), (string2, Font().MontserratBoldFont(font: 15.0), AppDarkColor)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
                self.nodatafoundLBL.attributedText = myMutableString
                return 0
            }
            self.nodatafoundLBL.text = ""
            return self.lottery_arry.activeLotteries.count
        }
        else if self.SelectedLotteryType == 1 {
            if self.lottery_arry.freezedLotteries.count == 0 {
                let msg = nodatamessage.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: ["Pooled", coindata.name])
                self.nodatafoundLBL.text = msg
                return 0
            }
            self.nodatafoundLBL.text = ""
            return self.lottery_arry.freezedLotteries.count
        }
        else {
            if self.lottery_arry.closedLotteries.count == 0 {
                let msg = nodatamessage.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: ["Closed", coindata.name])
                self.nodatafoundLBL.text = msg
                return 0
            }
            self.nodatafoundLBL.text = ""
            return self.lottery_arry.closedLotteries.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LotteryListCell = tableView.dequeueReusableCell(withIdentifier: "LotteryListCell") as! LotteryListCell
//        let cella = tableView.dequeueReusableCellWithIdentifier("LotteryListCell") as! LotteryListCell
        
        if self.SelectedLotteryType == 0 {
            self.ActiveLotterycell(indexPath: indexPath, cell: cell)
        }
        else if self.SelectedLotteryType == 1 {
            self.FrozenLotteryCell(indexPath: indexPath, cell: cell)
        }
        else {
            self.ClosedLotteryCell(indexPath: indexPath, cell: cell)
        }
        
        cell.stopTimer()
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
            self.BuyTicketvc(index: indexPath.row)
        }
        else if (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
            
        }
        else if (self.SelectedLotteryType == 2 && self.lottery_arry.closedLotteries.count != 0) {
            
        }
    }
    
    func BuyTicketvc(index: Int) {
        DispatchQueue.main.async {
            let ticket = BuyTicketVC.init(nibName: "BuyTicketVC", bundle: nil)
            ticket.modalPresentationStyle = .overCurrentContext
            ticket.modalTransitionStyle = .coverVertical
            ticket.CoinData = self.Carousel_Coins[self.SelectedBottomCell]
            if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
                ticket.activeLotteries = self.lottery_arry.activeLotteries[index]
            }
            else if (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
                ticket.freezedLotteries = self.lottery_arry.freezedLotteries[index]
            }
            else if (self.SelectedLotteryType == 2 && self.lottery_arry.closedLotteries.count != 0) {
                ticket.closedLotteries = self.lottery_arry.closedLotteries[index]
            }
            ticket.CMC_Prices = self.CMC_Prices
            JustDismiss = {
                
            }
            ticket.TicketDismissWithDictCallBack = { (data) in
                let ticketlist = TicketsListVC.init(nibName: "TicketsListVC", bundle: nil)
                ticketlist.SelectedTicket_Dict = data
                self.navigationController?.pushViewController(ticketlist, animated: true)
            }
            self.present(ticket, animated: true) {
                
            }
        }
    }
    
}

extension FeedsVC: SwipeTableViewCellDelegate, SwipeActionTransitioning {
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        
    }
    
    func configure(action: SwipeAction, with descriptor: ActionDescriptor) {
        action.title = descriptor.title(forDisplayMode: .imageOnly)
        action.image = descriptor.image(forStyle: .backgroundColor, displayMode: .imageOnly)
        action.backgroundColor = descriptor.color(forStyle: .backgroundColor)
        action.transitionDelegate = self
    }
    
    func didTransition(with context: SwipeActionTransitioningContext) -> Void {
        if context.newPercentVisible >= 2.5 {
            if #available(iOS 10.0, *) {
                Vibration.light.vibrate()
            }
            else {
                Vibration.oldSchool.vibrate()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .left {
            let Save = SwipeAction(style: .default, title: nil) { action, indexPath in
                if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
                    self.BuyTicketvc(index: indexPath.row)
                }
            }
            Save.hidesWhenSelected = true
            configure(action: Save, with: .Price)
            return [Save]
        } else {
            let timer = SwipeAction(style: .default, title: nil) { action, indexPath in
                
            }
            timer.hidesWhenSelected = true
            configure(action: timer, with: .Timers)
            if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) || (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
                return [timer]
            }
            else {
                return nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = orientation == .right ? .none : SwipeExpansionStyle(target: .edgeInset(0), additionalTriggers: [.overscroll(100)], completionAnimation: .fill(.automatic(.reset, timing: .with)))
        options.transitionStyle = .border
        options.backgroundColor = .clear
        options.buttonSpacing = 0
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        let cell = tableView.cellForRow(at: indexPath) as! LotteryListCell
        if orientation == .left {
            if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
                background {
                    let activeLotteries = self.lottery_arry.activeLotteries[indexPath.row]
                    CommanApiCall().ForexConversion(Buy: activeLotteries.coin, From: "USD") { (keyvalue1, keyvalue2) in
                        main {
                            let price = String.init(format: "%f", (activeLotteries.ticketPrice * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                            cell.showPrice(price: price!)
                        }
                    }
                }
            }
            else if (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
                background {
                    let freezedLotteries = self.lottery_arry.freezedLotteries[indexPath.row]
                    
                    var lottery_prize = Pooled_keyPrize
                    let keyprize: String = lottery_prize.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [freezedLotteries.lottery, freezedLotteries.coin])
                    
                    let value: String = UserInfoData.shared.GetOtherInfo(key: keyprize)!
                    if value.count == 0 || value.isEmpty {
                        CommanApiCall().ForexConversion(Buy: freezedLotteries.coin, From: "USD") { (keyvalue1, keyvalue2) in
                            main {
                                let price = String.init(format: "%f", (freezedLotteries.ticketPrice * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                                UserInfoData.shared.SaveOtherInfo(info: price!, key: keyprize)
                                cell.showPrice(price: price!)
                            }
                        }
                    }
                    else {
                        main {
                            cell.showPrice(price: value)
                        }
                    }
                }
            }
            else if (self.SelectedLotteryType == 2 && self.lottery_arry.closedLotteries.count != 0) {
                background {
                    let closedLotteries = self.lottery_arry.closedLotteries[indexPath.row]
                    
                    var lottery_prize = Closed_keyPrize
                    let keyprize: String = lottery_prize.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [closedLotteries.lottery, closedLotteries.coin])
                    
                    let value: String = UserInfoData.shared.GetOtherInfo(key: keyprize)!
                    if value.count == 0 || value.isEmpty {
                        CommanApiCall().ForexConversion(Buy: closedLotteries.coin, From: "USD") { (keyvalue1, keyvalue2) in
                            main {
                                let price = String.init(format: "%f", (closedLotteries.ticketPrice * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                                UserInfoData.shared.SaveOtherInfo(info: price!, key: keyprize)
                                cell.showPrice(price: price!)
                            }
                        }
                    }
                    else {
                        main {
                            cell.showPrice(price: value)
                        }
                    }
                }
            }
        } else {
            if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
                DispatchQueue.main.async {
                    let activeLotteries = self.lottery_arry.activeLotteries[indexPath.row]
                    cell.UpdateTimer(stamp: Double(activeLotteries.freezeTimestamp!))
                }
            }
            else if (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
                DispatchQueue.main.async {
                    let freezedLotteries = self.lottery_arry.freezedLotteries[indexPath.row]
                    cell.UpdateTimer(stamp: Double(freezedLotteries.endTimestamp!))
                }
            }
            else if (self.SelectedLotteryType == 2 && self.lottery_arry.closedLotteries.count != 0) {
                DispatchQueue.main.async {
                    //                    let closedLotteries = self.lottery_arry.closedLotteries[indexPath.row]
                    //                    cell.UpdateTimer(stamp: Double(closedLotteries.freezeTimestamp!))
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        let cell = tableView.cellForRow(at: indexPath!) as! LotteryListCell
        if orientation == .left {
            DispatchQueue.main.async {
                cell.stopTimer()
            }
        } else {
            if (self.SelectedLotteryType == 0 && self.lottery_arry.activeLotteries.count != 0) {
                DispatchQueue.main.async {
                    cell.stopTimer()
                }
            }
            else if (self.SelectedLotteryType == 1 && self.lottery_arry.freezedLotteries.count != 0) {
                DispatchQueue.main.async {
                    cell.stopTimer()
                }
            }
            else if (self.SelectedLotteryType == 2 && self.lottery_arry.closedLotteries.count != 0) {
                DispatchQueue.main.async {
                    cell.stopTimer()
                }
            }
        }
    }
    
}

//MARK:- Lotterylist Cell Config methods
extension FeedsVC {
    
    func ActiveLotterycell(indexPath: IndexPath, cell: LotteryListCell) {
        
        cell.SubView.isHidden = false
        cell.JackpotStack.isHidden = false
        cell.IntervalStack.isHidden = true
        cell.stopTimer()
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressRecognizer.minimumPressDuration = 0.8
        longPressRecognizer.allowableMovement = 15
        cell.addGestureRecognizer(longPressRecognizer)
        
        let lottery = self.lottery_arry.activeLotteries[indexPath.row]
        
        cell.JackpotLBL.showGradientSkeleton()
        cell.AmountLBL.showGradientSkeleton()
        
        let ticketsSold = lottery.ticketsSold ?? 0.0
        let ticketPrice = lottery.ticketPrice ?? 0.0
        let totalJackpotRate = lottery.totalJackpotRate ?? 0.0
        
        CommanApiCall().ForexConversion(Buy: lottery.coin, From: "USD") { (keyvalue1, keyvalue2) in
            cell.JackpotLBL.hideSkeleton()
            cell.AmountLBL.hideSkeleton()
            let jackpotvalue = ticketPrice * ticketsSold * (totalJackpotRate / 100) * keyvalue2
            let jackpot = String.init(format: "%f", jackpotvalue).CoinPriceThumbRules(Coin: "USD")
            cell.JackpotLBL.text = "Jackpot"
            cell.JackpotLBL.font = Font().MontserratBoldFont(font: 15)
            cell.AmountLBL.font = Font().MontserratBoldFont(font: 30)
            cell.AmountLBL.text = String.init(format: "%@", jackpot!)
            cell.AmountLBL.numberOfLines = 1
            cell.AmountLBL.minimumScaleFactor = 8 / cell.AmountLBL.font.pointSize
            cell.AmountLBL.adjustsFontSizeToFitWidth = true
        }
        
        cell.UPactionHandler {
            
        }
        
        cell.DOWNactionHandler {
            
        }
        
    }
    
    func FrozenLotteryCell(indexPath: IndexPath, cell: LotteryListCell) {
        
        cell.SubView.isHidden = false
        cell.JackpotStack.isHidden = false
        cell.IntervalStack.isHidden = true
        cell.stopTimer()
        
        let lottery = self.lottery_arry.freezedLotteries[indexPath.row]
        
        let ticketsSold = lottery.ticketsSold ?? 0.0
        let ticketPrice = lottery.ticketPrice ?? 0.0
        let totalJackpotRate = lottery.totalJackpotRate ?? 0.0
        
        var lottery_Key = Pooled_keyvalue
        let keyvalue: String = lottery_Key.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [lottery.lottery, lottery.coin])
        
        var lottery_prize = Pooled_keyPrize
        let key_value2: String = lottery_prize.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [lottery.lottery, lottery.coin])

        cell.JackpotLBL.font = Font().MontserratBoldFont(font: 15)
        cell.AmountLBL.font = Font().MontserratBoldFont(font: 30)
        
        let value: String = UserInfoData.shared.GetOtherInfo(key: keyvalue)!
        if value.count == 0 || value.isEmpty {
            cell.JackpotLBL.showGradientSkeleton()
            cell.AmountLBL.showGradientSkeleton()
            
            CommanApiCall().ForexConversion(Buy: lottery.coin, From: "USD") { (keyvalue1, keyvalue2) in
                cell.JackpotLBL.hideSkeleton()
                cell.AmountLBL.hideSkeleton()
                let jackpotvalue = ticketPrice * ticketsSold * (totalJackpotRate / 100) * keyvalue2
                let jackpot = String.init(format: "%f", jackpotvalue).CoinPriceThumbRules(Coin: "USD")
                UserInfoData.shared.SaveOtherInfo(info: jackpot!, key: keyvalue)
                cell.JackpotLBL.text = "Jackpot"
                cell.AmountLBL.text = String.init(format: "%@", jackpot!)
                cell.AmountLBL.numberOfLines = 1
                cell.AmountLBL.minimumScaleFactor = 8 / cell.AmountLBL.font.pointSize
                cell.AmountLBL.adjustsFontSizeToFitWidth = true
                
                let price = String.init(format: "%f", (lottery.ticketPrice * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                UserInfoData.shared.SaveOtherInfo(info: price!, key: key_value2)
                
            }
        }
        else {
            cell.JackpotLBL.text = "Jackpot"
            cell.AmountLBL.text = String.init(format: "%@", value)
            cell.AmountLBL.numberOfLines = 1
            cell.AmountLBL.minimumScaleFactor = 8 / cell.AmountLBL.font.pointSize
            cell.AmountLBL.adjustsFontSizeToFitWidth = true
        }
        
        cell.UPactionHandler {
            
        }
        
        cell.DOWNactionHandler {
            
        }
    }
    
    func ClosedLotteryCell(indexPath: IndexPath, cell: LotteryListCell) {
        
        cell.SubView.isHidden = false
        cell.JackpotStack.isHidden = false
        cell.IntervalStack.isHidden = true
        cell.stopTimer()
        
        let lottery = self.lottery_arry.closedLotteries[indexPath.row]
        
        let ticketsSold = lottery.ticketsSold ?? 0.0
        let ticketPrice = lottery.ticketPrice ?? 0.0
        let interestRate = lottery.interestRate ?? 0.0
        
        var lottery_Key = Closed_keyvalue
        let keyvalue: String = lottery_Key.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [lottery.lottery, lottery.coin])
        
        var lottery_prize = Closed_keyPrize
        let key_value2: String = lottery_prize.replaceLocalized(fromvalue: ["LOTTERY", "COIN"], tovalue: [lottery.lottery, lottery.coin])
        
        cell.JackpotLBL.font = Font().MontserratBoldFont(font: 15)
        cell.AmountLBL.font = Font().MontserratBoldFont(font: 30)
        
        let value: String = UserInfoData.shared.GetOtherInfo(key: keyvalue)!
        if value.count == 0 || value.isEmpty {
            CommanApiCall().ForexConversion(Buy: lottery.coin, From: "USD") { (keyvalue1, keyvalue2) in
                
                cell.JackpotLBL.showGradientSkeleton()
                cell.AmountLBL.showGradientSkeleton()
                
                cell.JackpotLBL.hideSkeleton()
                cell.AmountLBL.hideSkeleton()
                let jackpotvalue = ticketPrice * ticketsSold * (interestRate / 100) * keyvalue2
                let jackpot = String.init(format: "%f", jackpotvalue).CoinPriceThumbRules(Coin: "USD")
                
                UserInfoData.shared.SaveOtherInfo(info: jackpot!, key: keyvalue)
                
                cell.JackpotLBL.text = "Jackpot"
                cell.AmountLBL.text = String.init(format: "%@", jackpot!)
                cell.AmountLBL.numberOfLines = 1
                cell.AmountLBL.minimumScaleFactor = 8 / cell.AmountLBL.font.pointSize
                cell.AmountLBL.adjustsFontSizeToFitWidth = true
                
                let price = String.init(format: "%f", (lottery.ticketPrice * keyvalue2)).CoinPriceThumbRules(Coin: "USD")
                UserInfoData.shared.SaveOtherInfo(info: price!, key: key_value2)
            }
        }
        else {
            cell.JackpotLBL.text = "Jackpot"
            cell.AmountLBL.text = String.init(format: "%@", value)
            cell.AmountLBL.numberOfLines = 1
            cell.AmountLBL.minimumScaleFactor = 8 / cell.AmountLBL.font.pointSize
            cell.AmountLBL.adjustsFontSizeToFitWidth = true
        }
        
        cell.UPactionHandler {
            
        }
        
        cell.DOWNactionHandler {
            
        }
    }
    
}

//MARK:- Carousel Delegates
extension FeedsVC: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        if self.SelectedLotteryType == 0 {
             return self.Filtered_Coins.count
        }
        return self.Carousel_Coins.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: CarouselCoinView
        
        if let view = view {
            itemView = view as! CarouselCoinView
        } else {
            let coindata: CoinListPayload!
            if self.SelectedLotteryType == 0 {
                coindata = self.Filtered_Coins[index]
            }
            else {
                coindata = self.Carousel_Coins[index]
            }
            itemView = CarouselCoinView(frame: CGRect(x: 5, y: 5, width: carousel.frame.height - 5, height: carousel.frame.height - 5))
            itemView.setupView(imageURL: coindata.imageUrl, imgTin: (self.SelectedBottomCell == index ? UIColor.white : AppDarkColor), bgcolor: (self.SelectedBottomCell == index ? AppDarkColor : UIColor.white))
            itemView.HighlightView(imgTin: self.SelectedBottomCell == index ? UIColor.white : AppDarkColor, bgcolor: self.SelectedBottomCell == index ? AppDarkColor : UIColor.white)
        }
        
        return itemView
    }
    
    private func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.SelectedBottomCell = carousel.currentItemIndex
        let coindata: CoinListPayload!
        if self.SelectedLotteryType == 0 {
             coindata = self.Filtered_Coins[self.SelectedBottomCell]
        }
        else {
            coindata = self.Carousel_Coins[self.SelectedBottomCell]
        }
        self.getLotteryList(selectedcoin: coindata.coin) { (LotteryLoad) in
            self.lottery_arry = LotteryLoad
            self.FeedTBL.reloadData()
        }
    }
    
    func carouselItemIndexDidChange(_ carousel: iCarousel, withpreviousIndex previous: Int, withcurrentIndex Current: Int) {
        let coindata: CoinListPayload!
        if self.SelectedLotteryType == 0 {
             coindata = self.Filtered_Coins[self.SelectedBottomCell]
        }
        else {
            coindata = self.Carousel_Coins[self.SelectedBottomCell]
        }
        self.getLotteryList(selectedcoin: coindata.coin) { (LotteryLoad) in
            self.lottery_arry = LotteryLoad
            self.FeedTBL.reloadData()
        }
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCurlDown, animations: { () -> Void in
            var currentview: CarouselCoinView!
            currentview = carousel.itemView(at: Current) as? CarouselCoinView
            currentview.HighlightView(imgTin: .white, bgcolor: AppDarkColor)
            
            var previview: CarouselCoinView!
            previview = carousel.itemView(at: previous) as? CarouselCoinView
            if previview != nil {
                previview.HighlightView(imgTin: AppDarkColor, bgcolor: .white)
            }
        }, completion: { (finished:Bool) -> Void in

        })
    }
    
}
