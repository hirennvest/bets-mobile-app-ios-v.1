//
//  LuckLoaderVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import Lottie
import CRNotifications

class LuckLoaderVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet weak var NavBGView: UIView!
    @IBOutlet weak var NavIMG: UIImageView!
    
    @IBOutlet weak var Mainview: CustomView!
    
    @IBOutlet weak var LoaderView: UIView!
    
    @IBOutlet weak var LPtitleLBL: UILabel!
    @IBOutlet weak var LPBGView: CustomView!
    @IBOutlet weak var LPView: CustomView!
    @IBOutlet weak var LPvalueLBL: UILabel!
    
    //    MARK:- Variable Define
    
    var SelectedTicket_Dict: NSMutableDictionary!
    var ticket_array: [String]!
    var TicketPrice: Double!
    var price: Double!
    var CoinData: CoinListPayload!
    var activeLotteries: LotteryActiveLottery!
    var selectedBal: staticpaymentCoin!
    
    var TicketCount: String = ""
    func TicketDismissCallBack(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    var IsLottie: Bool = true
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ticket_array = self.SelectedTicket_Dict.value(forKey: Buy_Tickets_Count) as? [String]
        self.CoinData = self.SelectedTicket_Dict.value(forKey: Selected_Coin) as? CoinListPayload
        self.activeLotteries = self.SelectedTicket_Dict.value(forKey: SelectedLottery) as? LotteryActiveLottery
        self.TicketPrice = self.SelectedTicket_Dict.value(forKey: Buy_Price) as? Double
        self.price = self.SelectedTicket_Dict.value(forKey: Buy_Price) as? Double
        self.selectedBal = self.SelectedTicket_Dict.value(forKey: Buy_Vault) as? staticpaymentCoin
        
        self.SetupLoader()
    }
    
    //    MARK:- User Define Methods
    
    func setupLottieAnimation() {
        
        let loadingAnimation = Animation.named("SpinnerLoader1")
        let lottieView = AnimationView(animation: loadingAnimation)
        // 2. SECOND STEP (Adding and setup):
        self.LPView.addSubview(lottieView)
        lottieView.contentMode = .scaleAspectFit
        lottieView.clipsToBounds = true
        lottieView.loopMode = .loop
        self.LPView.backgroundColor = .white
        lottieView.play(toFrame: .infinity)
        //        lottieView.play { (status) in
        //            self.dismiss(animated: true) {
        //                NotificationCenter.default.post(name: .ReloadFeed, object: nil)
        //                NotificationCenter.default.post(name: .FeedTimer, object: nil)
        //                self.TicketDismissCallBack()
        //            }
        //        }
        // 3. THIRD STEP (LAYOUT PREFERENCES):
        lottieView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            lottieView.leftAnchor.constraint(equalTo: self.LPView.leftAnchor),
            lottieView.rightAnchor.constraint(equalTo: self.LPView.rightAnchor),
            lottieView.topAnchor.constraint(equalTo: self.LPView.topAnchor),
            lottieView.bottomAnchor.constraint(equalTo: self.LPView.bottomAnchor)
        ])
        
    }
    
    func SetupLoader() {
        
        self.PurchaseTickets()
        self.NavBGView.backgroundColor = AppDarkColor
        
        self.LoaderView.isHidden = false
        
        self.Mainview.backgroundColor = .white
        
        self.TicketCount = String.init(format: "%d", self.ticket_array.count)
        
        self.LPtitleLBL.text = "Loading..."
        self.LPtitleLBL.theme_textColor = GlobalPicker.textColor
        
        let string1: String = "Securing"
        let string2: String = String.init(format: "\n\n%@ %@", TicketCount, "Tickets")
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font().MontserratBoldFont(font: 25.0), AppDarkColor), (string2, Font().MontserratRegularFont(font: 15.0), AppDarkColor)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
        
        self.LPvalueLBL.attributedText = myMutableString
        
        self.LPBGView.BGColor = AppDarkColor.withAlphaComponent(0.3)
        self.LPView.BGColor = .white
        self.LPvalueLBL.theme_textColor = GlobalPicker.textColor
        
        if self.IsLottie {
            self.setupLottieAnimation()
        }
        else {
            let pers = 1
            
            let progress = RPCircularProgress.init(frame: CGRect(x: 0, y: 0, width: self.LPView.frame.width, height: self.LPView.frame.height))
            progress.trackTintColor = UIColor.init(red: 74 / 255, green: 144 / 255, blue: 226 / 255, alpha: 0.3)
            progress.progressTintColor = AppDarkColor
            progress.thicknessRatio = 0.2
            
            self.LPView.addSubview(progress)
            progress.updateProgress(CGFloat(pers), initialDelay: 0.4, duration: 1, completion: {
                progress.removeFromSuperview()
                
            })
        }
        
    }
    
    //    MARK:- Api Calling
    
    func PurchaseTickets() {
        background {
            let lotteryid = self.activeLotteries.id as String
            let coin = self.CoinData.coin as String
            
            let param = PurchaseTicketsParamDict.init(email: HeaderSetter().email, tickets: self.ticket_array!, lotteryid: lotteryid, sourcevault: coin, arcade_userid: HeaderSetter().ArcadeUserid)
            debugPrint(param.description)
            let header = CommanHeader.init(email: HeaderSetter().email, token: HeaderSetter().token)
            NetworkingRequests.shared.requestPOST(API_Purchase_Tickets, Parameters: param.description, Headers: header.description, onSuccess: { (responseobjcet, status) in
                main {
                    guard let res_status = responseobjcet["status"] else {
                        return
                    }
                    if status && res_status as! Bool {
                        NotificationCenter.default.post(name: .ReloadFeed, object: nil)
                        NotificationCenter.default.post(name: .FeedTimer, object: nil)
                        
                        let pay = PaymentSuccessVC.init(nibName: "PaymentSuccessVC", bundle: nil)
                        pay.SelectedTicket_Dict = self.SelectedTicket_Dict
                        self.navigationController?.pushViewController(pay, animated: true)
                    }
                    else {
                        let Message = responseobjcet["payload"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }) { (Message, responsecode) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
    
    //    MARK:- IBAction Methods
    
}
