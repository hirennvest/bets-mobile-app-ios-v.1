//
//  SpinLuckVC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SpinLuckVC: BaseVC {
    
    //    MARK:- IBOutlet
    
    @IBOutlet var NoView: [UIView]!
    @IBOutlet var animatedView: [NumberScrollAnimatedView]!
    @IBOutlet var LBL: [UILabel]!
    
    @IBOutlet weak var BetStack: UIStackView!
    @IBOutlet var BetImg: [UIImageView]!
    @IBOutlet weak var BetStacklbl: UILabel!
    @IBOutlet weak var switchlbl: UILabel!
    @IBOutlet weak var Forall: UIButton!
    
    @IBOutlet weak var spinView: CustomView!
    @IBOutlet weak var spinBTN: CustomBTN!
    
    //    MARK:- Variable Define
    
    var FinalNUmber:String = ""
    public var NumberDismissCallBack: ((String) -> Void)?
    var Duration: Double = 2.0
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.view.backgroundColor = AppDarkColor
        
        self.switchlbl.text = "One Ticket"
        self.Forall.setImage(UIImage.init(named: "SwitchOFF"), for: .normal)
        self.Forall.tag = 0
        
        self.spinView.BGColor = UIColor.white.withAlphaComponent(0.5)
        self.spinBTN.BTN_BGColor = AppDarkColor
        
        for (index, view) in NoView.enumerated() {
            view.backgroundColor = .clear
            view.clipsToBounds = true
            view.layer.cornerRadius = 10
//            LBL[index].text = String.init(format: "%d", Int.random(in: 0..<9))
            LBL[index].text = String.init(format: "0")
            LBL[index].textColor = AppDarkColor
            LBL[index].font = UIFont.boldSystemFont(ofSize: 64)
        }
        
        self.BetStacklbl.text = "I'm Feeling Lucky"
        self.BetStacklbl.textColor = .white
        
        for item in BetImg {
            item.tintColor = item.tag == 100 ? UIColor.white : AppDarkColor
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //    MARK:- User Define Methods
    
    func DismissSpinner() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3 , execute: {
            if self.FinalNUmber.count != 0 {
                self.dismiss(animated: true) {
                    if self.Forall.tag == 100 {
                        self.NumberDismissCallBack!("")
                    }
                    else {
                        self.NumberDismissCallBack!(self.FinalNUmber)
                    }
                }
            }
        })
    }
    
    //    MARK:- Api Calling
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedDismiss(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func onPushButton(_ sender: UIButton) {
        self.FinalNUmber = ""
        for (index, view) in animatedView.enumerated() {
            LBL[index].isHidden = true
            view.text = String.init(format: "%d", Int.random(in: 0..<9))
            view.textColor = AppDarkColor
            view.font = UIFont.boldSystemFont(ofSize: 64)
            view.animationDuration = 2
            view.scrollingDirectionRule = { (_, columnIndex) in return (columnIndex) == 0 ? NumberScrollAnimationDirection.down : NumberScrollAnimationDirection.up }
            view.startAnimation()
            self.FinalNUmber = String.init(format: "%@%@", self.FinalNUmber, view.text)
        }
        self.DismissSpinner()
    }
    
    @IBAction func TappedForall(_ sender: UIButton) {
        if self.Forall.tag == 100 {
            self.switchlbl.text = "One Ticket"
            self.Forall.setImage(UIImage.init(named: "SwitchOFF"), for: .normal)
            self.Forall.tag = 0
        }
        else {
            self.switchlbl.text = "All Tickets"
            self.Forall.setImage(UIImage.init(named: "SwitchON"), for: .normal)
            self.Forall.tag = 100
        }
    }
    
}
