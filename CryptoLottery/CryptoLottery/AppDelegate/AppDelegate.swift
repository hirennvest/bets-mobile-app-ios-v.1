//
//  AppDelegate.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-04-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CRNotifications
//import Firebase
import CRNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let window: UIWindow = {
        let w = UIWindow()
        w.backgroundColor = .white
        w.makeKeyAndVisible()
        return w
    }()
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        #if Dev_DEBUG || Dev_RELEASE
        print("Dev")
        #else
        print("Prod")
        #endif
//        FirebaseApp.configure()
        self.GetinitialView()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
}

extension AppDelegate {
    
    @objc public func GetinitialView() {
        let userinfo = UserInfoData.shared.GetUserInfodata()
        if userinfo == nil || userinfo!.idToken.count == 0 {
            self.SetLoginVC()
        }
        else {
//            if (userinfo == nil || userinfo!.idToken.count != 0) && !HeaderSetter().ArcadeUserid.isEmpty {
                self.SetDashboardVC(index: 0)
//            }
//            else {
//                self.GXregisterVC()
//            }
        }
    }
    
    @objc public func SetDashboardVC(index: Int = 0) {
        let vc: TabBarVC = loadViewController("Main", "TabBarVC") as! TabBarVC
        vc.isSelectedIndex = index
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
    @objc public func SetLoginVC() {
        let vc = LoginSplashVC.init(nibName: "LoginSplashVC", bundle: nil)
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
//    @objc public func GXregisterVC() {
//        let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
//        let navigationController = UINavigationController(rootViewController: vc)
//        UIApplication.shared.windows.first?.rootViewController = navigationController
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
//        navigationController.setNavigationBarHidden(true, animated: false)
//        self.window.backgroundColor = UIColor.white
//    }
    
}
